# sophie

Ein Programm zur Verwaltung von Rezepten

Sophie ist in einem sehr frühem Stadium und nicht benutzbar.

Aktuell ruht die Entwicklung.

## Starten

* Lade den Quellcode herunter: `git clone https://gitlab.com/dknof/sophie.git`
* Wechsel in das src-Verzeichnis: `cd src`
* Kompiliere sophie: `make`
* Starte sophie: `./sophie`

### Voraussetzungen

Du benötigst make und einen C++-Compiler mit C++14-Unterstützung
Die benutzten Bibliotheken sind:

* [gtkmm3](https://www.gtkmm.org)
* [sqlite3](https://www.sqlite.org)

Debian:

    apt install g++ make
    apt install libgtkmm-3.0-dev libsqlite3-dev

## Website

Der Quelltext befindet sich unter <https://gitlab.com/dknof/sophie>.

## Author

* Dr. Diether Knof <dknof+sophie@posteo.de>

## Mitwirken

Sophie ist ein freies Projekt. Bei Interesse kontaktiere den [author](mailto:dknof+sophie@posteo.de).

## Lizenz

Sophie ist Freie Software: Sie können es unter den Bedingungen
der GNU General Public License, wie von der Free Software Foundation,
Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
veröffentlichten Version, weiter verteilen und/oder modifizieren.

## Status des Projekts

Dieses Projekt ruht auf unbestimmte Zeit (Stand: 10. Oktober 2019).

Ich habe sophie gestartet um mich mit Datenbanken zu beschäftigen.
Aktuell arbeite ich lieber an anderen Projekten und habe keinen festen Plan, an sophie weiterzuentwickeln.
