Kategorie
=========

Jedes link:Rezept.html[Rezept] wird genau einer Kategorie zugeordnet.
Kategorieen lassen sich hierarchisch anorden.

== Beispiele
* Kuchen
* Nachtisch
* Getränk/Cocktail


== Datenbank
* link:../Datenbank/Kategorie.html[Kategorie]

// vim: filetype=asciidoc
