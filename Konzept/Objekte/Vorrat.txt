Vorrat
======

Eine Sammlung von Vorräten und optionaler Menge.
Wird verwendet, um bei einer Einkaufsliste bestimmte Zutaten rauszulassen.

== Beispiele
* 10 Eier
* 1,5 kg Mehl
* Salz


== Datenbank
* link:../Datenbank/Vorrat.html[Vorrat]

// vim: filetype=asciidoc
