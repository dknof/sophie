Rezept
======

Ein Rezept besteht aus ein paar Metadaten, link:Bild.html[Bildern], den link:Zutat.html[Zutaten] und den Beschreibungen.
Mehrere Rezepte können zu einem link:Menü.html[Menü] zusammengeführt werden.

Ein Rezept kann aus mehreren Teilen bestehen (Beispiel: eine Bisquitrolle besteht aus dem Teig und der Füllung). Außerdem kann es von einem Rezept oder einem Teil mehrere Varianten geben (Beispiel: die Füllung der Bisquitrolle kann aus Himbeeren oder Erdbeern enthalten.).

== Bestandteile
* Name (muss eindeutig sein)
* Autor
* Quelle
* link:Bild.html[Bilder]
* Liste link:Eigenschaft.html[Eigenschaften] (nur Typ, keine Menge wie Kalorien)
* Menge (Endprodukt)
* Zubereitungsdauer
* Arbeitsdauer
* link:Kategorie.html[Kategorie] (wie Kuchen, Nachtisch)
* Liste link:Zutat.html[Zutaten] mit Menge
* Beschreibung


== Aktionen
* Suchen
* Zum Menü hinzufügen


== Sonstiges
* Jedes Rezept kann höchstens einer Kategorie zugeordnet werden.
Sonstige Unterscheidung muss über die Eigenschaften erfolgen.
* Eine Bewertung der Rezepte ist nicht vorgesehen. Ist das Rezept nicht gut, dann sollte es einfach gelöscht werden.
* Eventuell auch noch Werkzeug (wie Wok, Mixer) mit aufnehmen. Könnte aber auch über die Eigenschaften angegeben werden.


== Datenbank
* link:../Datenbank/Rezept.html[Rezept]

// vim: filetype=asciidoc
