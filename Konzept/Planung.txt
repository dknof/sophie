Planung
=======

== Was soll noch umgesetzt werden

=== Version 0.3
* gtkmm: Details

=== Version 0.4
* gtkmm: aktuelle Einstellungen bearbeiten

=== Version 0.6
* Geschäfte

=== Version 0.7
* Rezepte

=== Version 0.8
* pdf-Export

=== Version 0.9
* Ex- und Import von einzelnen Rezepten

=== Version 1.0
* Anwenderhandbuch

=== nach Version 1.0
* Filter
* Unterstützung von Modulen
* Unterstützung von anderen Dateiformaten

// vim: filetype=asciidoc
