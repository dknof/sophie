Fallbeispiele
=============

* Druck mir das Rezept Mandelhörnchen aus
* Wie viele Zutaten brauche ich, um für 6 Personen Waffeln zu backen?
* Ich habe noch 200~ml Sahne und 150~g Butter. Welchen Kuchen ohne Schokolade kann ich damit backen?
  - Für den ich die restlichen Zutaten bei Aldi finde?
* Ich möchte Schwarzwälder Kirschtorte backen. Ich habe noch 1~Glas Kirschen. Schreibe mir eine Einkaufsliste für die restlichen Zutaten.
  - Für Aldi / REWE: In der Reihenfolge zum durchgehen, mit Kosten
* Was kann ich jetzt zum Mittagessen für morgen kochen, zum Mitnehmen, ohne Schweinefleisch?
* Drei verschiedene Kuchen für einen Kindergeburtstag (4 Erwachsene, 6 Kinder).

// vim: filetype=asciidoc
