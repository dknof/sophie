Zutat
=====

Eine Zutat ist Bestandteil eines link:Rezept.html[Rezeptes] über die Tabelle link:Rezeptzutaten.html[Rezeptzutaten].
Außerdem sind link:Geschäft.html[Geschäfte]  Listen von Zutaten, siehe link:Geschäftzutaten.html[Geschäftzutaten].

== Elemente
* ZutatId
* Name Einzahl (String)
* Name Mehrzahl (String/Null)
* Bild (BildId/Null)
* Symbol (BildId/Null)

== Beispiele
* Mehl
* Eier
* Milch
* Fleisch
* Salz

== Ideen
* Zutaten gruppieren. Damit ist es einfacher, eine Reihenfolge für „Geschäft“ anzugeben.
* Zutaten mit Eigenschaften versehen, die sich auf das Rezept vererben.
  Beispiele: nicht vegetarisch, enthält Laktose.
  Vorsicht bei „süß“, da kommt es auf die Menge an.
* Substitute (Zuckerersatzstoff)

== Objekt
* link:../Objekte/Zutat.html[Zutat]

// vim: filetype=asciidoc
