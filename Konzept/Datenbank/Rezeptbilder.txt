Rezeptbilder
============

Sortierte Verknüpfung zwischen link:Rezept.html[Rezept] und link:Bild.html[Bildern].

== Elemente
* RezeptId
* Nummer (integer)
* BildId

== Regeln
* Für jedes RezeptId sind alle Nummer unterschiedlich.
* Für jedes RezeptId sind alle BildId unterschiedlich.

// vim: filetype=asciidoc
