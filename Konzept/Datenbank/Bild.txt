Bild
====

Ein Bild oder Symbol.
Kann bei link:Rezept.html[Rezepten] und link:Zutat.html[Zutaten] verwendet werden.

== Elemente
* BildId
* Name (String/Null)
* Daten (binär)

== Objekt
* link:../Objekte/Bild.html[Bild]

// vim: filetype=asciidoc
