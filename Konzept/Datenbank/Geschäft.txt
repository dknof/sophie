Geschäft
========

Ein Geschäft ist eine sortiert Liste aus Zutaten mit Preisen, siehe link:Geschäftzutaten.html[Geschäftzutaten].

== Elemente
* GeschäftId
* Name (String)

== Objekt
* link:../Objekte/Geschäft.html[Geschäft]

// vim: filetype=asciidoc
