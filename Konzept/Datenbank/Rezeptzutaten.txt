Rezeptzutaten
=============

Sortierte Verknüpfung zwischen link:Rezept.html[Rezept] und link:Zutat.html[Zutaten] mit Menge.

== Elemente
* RezeptId
* Nummer (integer)
* ZutatId
* Menge (double)
* EinheitId

== Regeln
* Für jedes RezeptId sind alle Nummer unterschiedlich.
* Zu einer RezeptId kann eine ZutatId mehrfach auftreten.

// vim: filetype=asciidoc
