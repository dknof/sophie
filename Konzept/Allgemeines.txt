Allgemeines
===========

== Was soll das Programm können
* Rezepte verwalten
* Rezepte suchen/zusammenstellen
* Einkaufslisten aus Rezepten erstellen


=== Sonstiges
* Synchronisation der Daten zwischen PC, Laptop, Tablet
* Unterstützung von Modulen


== Alternative Programme
* gourmet
* krecipes


== Sonstiges
* Auf deutsch
* Betriebssystem: Linux
* Versionsverwaltung: git
* html-Seiten: mit asciidoc
* Programmiersprache: C\++ (aktuell: C++14)

// vim: filetype=asciidoc
