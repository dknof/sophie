Vorrat
======

== Aktionen
* Ausdrucken


== Ideen
* Mindest- und Sollwerte
  - Beim Erstellen des link:Einkaufsliste.html[Einkaufsliste] auf den Sollwert auffüllen.
    - Wenn sowieso die Zutat gekauft wird
    - Wenn der Mindestwert unterschritten ist


// vim: filetype=asciidoc
