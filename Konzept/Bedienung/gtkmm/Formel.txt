Formel
======

Eine Formel zum Umrechnen von Einheiten

== Direkte Information
* Bild: Beide Einheiten, gegebenenfalls Zutat
* Menge 1
* Zutat
* Menge 2

== Verweise
* link:../../Datenbank/Zutat.html[Datenbank]

=== Oberkategorieen
* link:Einheitenumrechner.html[Einheitenumrechner]

// vim: filetype=asciidoc
