Rezepte
=========

Liste der link:Rezept.html[Rezepte]

== Tabelle
* Kategorie (mit Unterkategorie)
* Bild
* Name

== Aktionen
* neu: erstellt eine neue Rezept
* löschen: löscht eine Rezept
* drucken: erstellt ein pdf-Dokument mit der Rezeptliste

== Details
* link:Rezept.html[Rezept]

// vim: filetype=asciidoc
