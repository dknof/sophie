Einheit
=======

Eine Einheit

== Direkte Information
* Bild
: Lässt sich per drag-and-drop ändern
* Name
* Kürzel

== Indirekte Informationen
* Einheitenumrechnungen
** mit Eingabe (Menge, Zutat, Einheit 2)

== Aktionen
* Drucken
: Druckt die Einheit

== Verweise
* link:Einheitenumrechner.html[Einheitenumrechner]
* link:../../Datenbank/Einheit.html[Datenbank]

=== Oberkategorieen
* link:Einheiten.html[Einheiten]

// vim: filetype=asciidoc
