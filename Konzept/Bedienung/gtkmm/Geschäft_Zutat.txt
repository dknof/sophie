Geschäft -- Zutat
=================

Eine Zutat aus dem Geschäft

Wie link:Zutat.html[Zutat]

Änderungen:
== Direkte Informationen
* Preis

== Indirekte Informationen
* Preisvergleich mit anderen Geschäften
* Einheitenumrechnungen
** Menge des Geschäft als Voreinstellung
* Liste der Rezepte mit der Zutat
** Nur Rezepte, für die der Geschäft reicht

== Aktionen
* Drucken
: Druckt die Zutat

== Verweise
* link:Einheitenumrechner.html[Einheitenumrechner]
* link:../../Datenbank/Zutat.html[Datenbank]

=== Oberkategorieen
* link:Geschäft.html[Geschäft]

// vim: filetype=asciidoc
