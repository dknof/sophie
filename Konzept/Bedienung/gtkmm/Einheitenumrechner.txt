Einheitenumrechner
==================

Liste der link:Formel.html[Formeln]

== Tabelle
* Menge 1
: lässt sich direkt ändern
* Zutat
* Menge 2
: lässt sich direkt ändern

== Aktionen
* neu: erstellt eine neue Formel
* löschen: löscht eine Formel
* drucken: erstellt ein pdf-Dokument mit den Formeln

== Details
* link:Formel.html[Formel]

// vim: filetype=asciidoc
