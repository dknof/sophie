Zutat
=====

Eine Zutat

== Direkte Information
* Bild
: Lässt sich per drag-and-drop ändern
* Name
* Mehrzahl

== Indirekte Informationen
* Einheitenumrechnungen
** mit Eingabe (Menge, Einheit 1, Einheit 2)
* Liste der Rezepte mit der Zutat
** Liste nach Kategorieen
** enthält die Menge der Zutat
** einklappbar
** Doppelklick öffnet das entsprechende Rezept
* Menge im Vorrat
** lässt sich verändern
* Menge in der Einkaufsliste
** lässt sich verändern
* Preise in den Geschäften
** Liste: Preis + Menge + Geschäft, Sortierung nach Preis

== Aktionen
* Drucken
: Druckt die Zutat

== Verweise
* link:Einheitenumrechner.html[Einheitenumrechner]
* link:../../Datenbank/Zutat.html[Datenbank]

=== Oberkategorieen
* link:Zutaten.html[Zutaten]

// vim: filetype=asciidoc
