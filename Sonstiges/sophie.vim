" Vim syntax für die Rezeptverwaltung sophie
" Maintainer:	Dr. Diether Knof <dknof@gmx.de>

:syntax clear
:syntax sync fromstart
:set nospell

syn match Comment  "^[ \t]*#.*$"

syn match sophie_Kategorie "^/\?Rezepte$"
syn match sophie_Kategorie "^/\?Vorrat$"
syn match sophie_Kategorie "^/\?Einkaufsliste$"
syn match sophie_Kategorie "^/\?Zutaten$"
syn match sophie_Kategorie "^/\?Einheiten$"
syn match sophie_Kategorie "^/\?Einheitenumrechner$"
syn match sophie_Kategorie "^/\?Geschäfte$"


syn match sophie_Eigenschaft " Bild:"
syn match sophie_Eigenschaft " Kategorie:"
syn match sophie_Befehl "^+"
syn match sophie_Befehl "^-"
syn match sophie_Befehl "^Liste$"
syn match sophie_Befehl "^Version$"

syn match sophie_beenden "^beenden$"


"hi Comment	cterm=bold	ctermfg=darkgray    guifg=darkgray

hi sophie_Kategorie             cterm=none ctermfg=darkgreen  guifg=darkgreen
hi sophie_Befehl                cterm=none ctermfg=blue       guifg=blue
hi sophie_beenden               ctermbg=red                   guibg=red
hi sophie_Eigenschaft           ctermfg=blue                  guifg=blue
