#pragma once

#include <array>

#include "class/readconfig/readconfig.h"

/** Einstellungen
 **/
class Einstellung {
  public:
    enum TypBool {
      ERSTER,
      BOOL_ERSTER = ERSTER,

      // automatisches Speichern
      EINSTELLUNGEN_SPEICHERN = BOOL_ERSTER,
      // Prompt anzeigen
      PROMPT,
      // Befehle ausgeben
      BEFEHLE_AUSGEBEN,

      BOOL_LETZTER = BEFEHLE_AUSGEBEN
    }; // enum TypBool
    static unsigned constexpr BOOL_ANZAHL = (1 + BOOL_LETZTER - BOOL_ERSTER);

    enum TypString {
      STRING_ERSTER = BOOL_LETZTER + 1,
      DATEIPFAD = STRING_ERSTER,

      STRING_LETZTER = DATEIPFAD
    }; // enum TypString
    static unsigned constexpr STRING_ANZAHL = (1 + STRING_LETZTER - STRING_ERSTER);

    enum TypStringKonstant {
      STRING_KONSTANT_ERSTER = STRING_LETZTER + 1,

      VERZEICHNIS_PRIVAT = STRING_KONSTANT_ERSTER,
      EINSTELLUNGEN_DATEI,

      STRING_KONSTANT_LETZTER = EINSTELLUNGEN_DATEI
    }; // enum TypeStringConst
    static unsigned constexpr STRING_KONSTANT_ANZAHL = (1 + STRING_KONSTANT_LETZTER - STRING_KONSTANT_ERSTER);

  public:
    // Konstruktor
    Einstellung();
    // Destruktor
    ~Einstellung();

    // Gibt den Namen der Einstellung zurück
    static char const* name(TypBool typ);
    static char const* name(TypString typ);
    static char const* name(TypStringKonstant typ);

    // Gibt die Einstellung zurück
    bool operator()(TypBool typ) const;
    string operator()(TypString typ) const;
    string operator()(TypStringKonstant typ) const;

    // Gibt den Typ mit dem Namen zurück
    int typ(string const& name) const;

    // Setzt die Einstellung
    bool setze(string const& typ, string const& wert);
    bool setze(Config config);
    void setze(TypBool typ, bool wert);
    void setze(TypBool typ, string const& wert);
    void setze(TypString typ, string const& wert);

    // Lade die Einstellungen
    void lese(istream& istr);
    void lade();
    void lade(string const& datei);

    // Speicher die Einstellung
    void schreibe(ostream& ostr) const;
    bool speicher() const;
    bool speicher(string const& datei) const;

  private:
    // Setze auf die Ursprungswerte zurück
    void zuruecksetzen();

  private:
    // Einstellungen vom Typ bool
    std::array<bool, (BOOL_LETZTER - BOOL_ERSTER + 1)> bool_;
    // Einstellungen vom Typ string
    std::array<string, (STRING_LETZTER - STRING_ERSTER + 1)> string_;
}; // class Einstellung

// Ausgabe der Einstellungen
ostream& operator<<(ostream& ostr, Einstellung const& einstellung);
// Ausgabe der Einstellungen
istream& operator>>(istream& istr, Einstellung& einstellung);

// Vergleich zweier Einstellungen
bool operator==(Einstellung const& einstellung_a,
                Einstellung const& einstellung_b);
bool operator!=(Einstellung const& einstellung_a,
                Einstellung const& einstellung_b);

extern Einstellung einstellung;
