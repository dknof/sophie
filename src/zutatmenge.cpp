#include "constants.h"

#include "zutatmenge.h"

/** Konstruktor
 **/
ZutatMenge::ZutatMenge(Zutat const zutat) :
  zutat(zutat)
{ }

/** Konstruktor
 **/
ZutatMenge::ZutatMenge(Zutat const zutat,
                       Menge const menge) :
  zutat(zutat), menge(menge)
{ }

/** Konstruktor
 **/
ZutatMenge::ZutatMenge(Zutat const zutat,
                       Einheit const einheit,
                       Anzahl const anzahl) :
  zutat(zutat), menge(anzahl, einheit)
{ }

/** Konstruktor
 **/
ZutatMenge::ZutatMenge(string const zutat,
                       string const einheit,
                       string const anzahl) :
  zutat(zutat), menge(std::stod(anzahl), Einheit(einheit))
{ }

/** Konstruktor
 **/
ZutatMenge::ZutatMenge(vector<string> const zutat_menge)
{
  if (zutat_menge.size() == 1) {
    this->zutat = Zutat(zutat_menge[0]);
  } else if (zutat_menge.size() == 3) {
    this->zutat = Zutat(zutat_menge[0]);
    this->menge = Menge(std::stod(zutat_menge[2]), Einheit(zutat_menge[1]));
  } else {
    throw "ZutatMenge(vector<string>): Der Vektor hat eine falsche Größe: " + to_string(zutat_menge.size());
  }
} // ZutatMenge::ZutatMenge(vector<string> zutat_menge)

/** ZutatMenge ausgeben
 **/
ostream&
operator<<(ostream& ostr, ZutatMenge const& zutat_menge)
{
  if (zutat_menge.menge)
    ostr << zutat_menge.menge << ' ';
  ostr << zutat_menge.zutat.name();
  return ostr;
} // ostream& operator<<(ostream& ostr, ZutatMenge zutat_menge)
