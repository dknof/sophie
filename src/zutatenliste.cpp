#include "constants.h"

#include "zutatenliste.h"

/** Konstruktor
 **/
ZutatenListe::ZutatenListe()
{ }

/** -> Rückgabe
 **
 ** @param    zutat
 **
 ** @return   Ob die Zutat enthalten ist
 **/
bool
ZutatenListe::enthaelt(Zutat const& zutat) const
{
  for (auto const& e : *this)
    if (e.zutat == zutat)
      return true;
  return false;
} // bool ZutatenListe::enthaelt(Zutat zutat) const

/** -> Rückgabe
 **
 ** @param    zutat
 **
 ** @return   Menge der Zutat
 **/
Menge
ZutatenListe::menge(Zutat const& zutat) const
{
  for (auto const& e : *this)
    if (e.zutat == zutat)
      return e.menge;
  return Menge();
} // Menge ZutatenListe::menge(Zutat zutat) const

/** Fügt der Liste eine Zutat hinzu
 **
 ** @param    zutat    Zutat mit Menge
 **
 ** @todo    alles
 **/
void
ZutatenListe::hinzufuege(ZutatMenge const& zutat)
{
  this->push_back(zutat);
  return ;
 } // void ZutatenListe::hinzufuege(ZutatMenge zutat)

/** Fügt der Liste eine Zutat hinzu
 **
 ** @param    zutat    Zutat
 **/
void
ZutatenListe::hinzufuege(Zutat const& zutat)
{
  this->hinzufuege(ZutatMenge(zutat));
  return ;
} // void ZutatenListe::hinzufuege(Zutat zutat)

/** Fügt der Liste eine Zutat mit Menge hinzu
 **
 ** @param    zutat    Zutat
 ** @param    menge    Menge
 **/
void
ZutatenListe::hinzufuege(Zutat const& zutat, Menge const& menge)
{
  this->hinzufuege(ZutatMenge(zutat, menge));
  return ;
} // void ZutatenListe::hinzufuege(Zutat zutat, Menge menge)

/** Entfernt die Zutat mit Menge aus der Liste^
 **
 ** @param    zutat    Zutat mit Menge
 **
 ** @return   ob die Zutat entfernt wurde
 **
 ** @todo    alles
 **/
bool
ZutatenListe::entferne(ZutatMenge const& zutat)
{
  return false;
} // bool ZutatenListe::entferne(ZutatMenge zutat)

/** Entfernt die Zutat aus der Liste
 **
 ** @param    zutat    Zutat
 **
 ** @return   ob die Zutat entfernt wurde
 **/
bool
ZutatenListe::entferne(Zutat const& zutat)
{
  bool ergebnis = false;
  for (auto z = this->begin(); z != this->end(); ) {
    if (z->zutat == zutat) {
      z = this->erase(z);
      ergebnis = true;
    } else {
      ++z;
    }
  } // for (auto z : this)
  return ergebnis;
} // bool ZutatenListe::entferne(Zutat zutat)

/** Entfernt die Zutat mit Menge aus der Liste
 **
 ** @param    zutat    Zutat
 ** @param    menge    Menge
 **
 ** @return   ob die Zutat entfernt wurde
 **/
bool
ZutatenListe::entferne(Zutat const& zutat, Menge const& menge)
{
  return this->entferne(ZutatMenge(zutat, menge));
} // bool ZutatenListe::entferne(Zutat zutat, Menge menge)

/** ZutatenListe ausgeben
 **
 ** @param   ostr            Ausgabestrom
 ** @param   zutaten_liste   ZutatenListe
 **
 ** @return  Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, ZutatenListe const& zutaten_liste)
{
  for (auto& e : zutaten_liste)
    ostr << e << '\n';
  return ostr;
} // ostream& operator<<(ostream& ostr, ZutatenListe zutaten_liste)
