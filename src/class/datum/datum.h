#pragma once

#include <iosfwd>
#include <string>

/** Eine Datumsklasse, bestehend aus Jahr, Monat, Tag
 ** Abstände/Differenzen sind nicht möglich.
 **/
class Datum {
  public:
    // Der Monat
    enum Monat { JANUAR = 1, FEBRUAR, MAERZ, APRIL, MAI, JUNI, JULI, AUGUST, SEPTEMBER, OKTOBER, NOVEMBER, DEZEMBER };

    // Konvertierung eines Strings in einen Monat
    static Monat stomonat(std::string monatsname);

  public:
    // Konstruktor (Kompilierdatum)
    Datum();
    // Konstruktor (Jahr, Monat, Tag)
    Datum(int jahr, Monat monat, int tag);
    // Konstruktor (Strom)
    Datum(std::istream& istr);
    // Konstruktor (String)
    Datum(std::string const& s);

    // Konvertierung in einen String, 2006-10-11
    operator std::string() const;

    // Jahr
    int jahr = 1900;
    // Monat
    Monat monat = JANUAR;
    // Tag
    int tag = 1;
}; // class Datum

// Vergleich
bool operator==(Datum const& d1, Datum const& d2);
bool operator!=(Datum const& d1, Datum const& d2);
bool operator<( Datum const& d1, Datum const& d2);
bool operator<=(Datum const& d1, Datum const& d2);
bool operator>( Datum const& d1, Datum const& d2);
bool operator>=(Datum const& d1, Datum const& d2);

// Datum ausgeben
std::ostream& operator<<(std::ostream& ostr, Datum const& datum);
