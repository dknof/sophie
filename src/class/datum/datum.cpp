#include "datum.h"

#include <fstream>
#include <sstream>
#include <cstdlib>

/** Standardkonstruktor: Kompilierdatum
 **/
Datum::Datum() :
  // Format: monat tag jahr (ex: Dec  8 2003)
  jahr{std::stoi(__DATE__ + 7)},
  monat{Datum::stomonat(std::string(__DATE__, 3))},
  tag{std::stoi(__DATE__ + 4)}
{ }

/** Konstruktor
 **
 ** @param     jahr    Jahr
 ** @param     monat   Monat
 ** @param     tag     Tag
 **/
Datum::Datum(int const jahr, Monat const monat, int const tag) :
  jahr{jahr},
  monat{monat},
  tag{tag}
{ }

/** Konstruktor: Eingabestrom, Format: 2014-01-01
 **
 ** @param     istr   Eingabestrom
 **/
Datum::Datum(std::istream& istr)
{
  int jahr = 0;
  Monat monat = JANUAR;
  int tag = 0;

  if (::isdigit(istr.peek())) {
    // Fomat 1: 2014-01-02
    istr >> jahr;
    if (istr.get() != '-')
      return ;
    if (istr.peek() == '0')
      istr.get();
    if (istr.get() != '-')
      return ;
    if (istr.peek() == '0')
      istr.get();
    istr >> tag;

  } else {
    // Fomat 2:  2 Jan 2014
    char s[4];
    s[3] = 0;
    istr.get(s, 4);
    monat = Datum::stomonat(s);
    if (istr.get() != ' ')
      return ;
    istr >> tag;
    if (istr.fail())
      return ;
    istr >> jahr;
  }

  if (istr.fail())
    return ;

  this->jahr = jahr;
  this->monat = static_cast<Monat>(monat);
  this->tag = tag;
} // Datum::Datum(std::istream& istr)

/** Konstruktor: String
 **
 ** @param     s   Text, aus dem das Datum gelesen wird
 **/
Datum::Datum(std::string const& s)
{
  std::istringstream istr(s.c_str());
  *this = Datum(istr);
} // Datum::Datum(std::string s)

/** Konvertierung in einen String
 **
 ** @return    String-Darstellung
 **/
Datum::operator std::string() const
{
  std::ostringstream ostr;
  ostr << *this;
  return ostr.str();
} // Datum::operator std::string() const

/** Schreibe ein Datum in einen Ausgabestrom, Format 2014-01-01
 **
 ** @param     ostr    Ausgabestrom
 ** @param     datum   Datum
 **
 ** @return    der Ausgabestrom
 **/
std::ostream&
operator<<(std::ostream& ostr, Datum const& datum)
{
  ostr << datum.jahr << '-'
    << (datum.monat < 10 ? "0" : "") << datum.monat << '-'
    << (datum.tag < 10 ? "0" : "") << datum.tag;

  return ostr;
} // std::ostream& operator<<(std::ostream& ostr, Datum datum)

/** Vergleichsoperator
 **
 ** @param     d1   erstes Datum
 ** @param     d2   zweites Datum
 **
 ** @return    ob d1 und d2 gleich sind
 **/
bool
operator==(Datum const& d1, Datum const& d2)
{
  return (   (d1.tag == d2.tag)
          && (d1.monat == d2.monat)
          && (d1.jahr == d2.jahr));
} // bool operator==(Datum d1, Datum d2)

bool
operator!=(Datum const& d1, Datum const& d2)
{ return !(d1 == d2); }

/** Vergleichsoperator
 **
 ** @param     d1   erstes Datum
 ** @param     d2   zweites Datum
 **
 ** @return    ob d1 vor d2 liegt (älter ist)
 **/
bool
operator<(Datum const& d1, Datum const& d2)
{
  if (d1.jahr < d2.jahr)
    return true;
  if (d1.jahr > d2.jahr)
    return false;

  // ab jetzt gilt 'd1.jahr == d2.jahr'
  if (d1.monat < d2.monat)
    return true;
  if (d1.monat > d2.monat)
    return false;

  // ab jetzt gilt 'd1.monat == d2.monat'
  if (d1.tag < d2.tag)
    return true;
  if (d1.tag > d2.tag)
    return false;

  return false;
} // bool operator<(Datum const& d1, Datum const& d2)

bool
operator>(Datum const& d1, Datum const& d2)
{ return (d2 < d1); }

bool
operator<=(Datum const& d1, Datum const& d2)
{ return !(d2 < d1); }

bool
operator>=(Datum const& d1, Datum const& d2)
{ return !(d1 < d2); }


/**
 ** -> result
 **
 ** @param     monatname   name of the monat (english, short form, ex: Dec)
 **
 ** @return    number of the monat 'monatname',
 **	           0 on failure
 ** 
 ** @author    Diether Knof
 **
 ** @version   2003-12-09
 **/
Datum::Monat
Datum::stomonat(std::string const monatname)
{
  if (monatname == "Jan")
    return JANUAR;
  if (monatname == "Feb")
    return FEBRUAR;
  if (monatname == "Mar")
    return MAERZ;
  if (monatname == "Apr")
    return APRIL;
  if (monatname == "May")
    return MAI;
  if (monatname == "Jun")
    return JUNI;
  if (monatname == "Jul")
    return JULI;
  if (monatname == "Aug")
    return AUGUST;
  if (monatname == "Sep")
    return SEPTEMBER;
  if (monatname == "Oct")
    return OKTOBER;
  if (monatname == "Nov")
    return NOVEMBER;
  if (monatname == "Dec")
    return DEZEMBER;

  // Fehler
  return JANUAR;
} // Datum::Monat Datum::monattou(std::string monatname)
