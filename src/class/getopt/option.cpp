//////////////////////////////////////////////////////////////////////////////
//
//   Copyright (C) 2002  by Diether Knof
//
//   This program is free software; you can redistribute it and/or 
//   modify it under the terms of the GNU General Public License as 
//   published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details. 
//   You can find this license in the file 'gpl.txt'.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA  02111-1307  USA
//
//  Contact:
//    Diether Knof dknof@gmx.de
//
//////////////////////////////////////////////////////////////////////////////

#include "getopt.h"

#include <iostream>
#include <cstdlib>
#include <climits>

using namespace GetOpt;

/*
 * -> result
 *
 * @return   the error code
 */
Option::Error
Option::error() const
{
  return this->error_;
} // Option::Error Option::error() const

/*
 * -> result
 *
 * @return   wether the parsing is failed
 */
bool
Option::fail() const
{
  return (   (this->error() != OK)
	  && (this->error() != NO_OPTION));
} // bool Option::fail() const

/*
 * -> result
 *
 * @return   whether another option could be parsed
 *           (with or without an error)
 */
Option::operator bool() const
{
  return (this->error() != NO_OPTION);
} // Option::operator bool() const


/*
 * -> result
 *
 * @return   the name of the option
 */
std::string const&
Option::name() const
{
  return this->name_;
} // std::string Option::name() const

/*
 * -> return
 *
 * @return   the type of the option
 */
Syntax::Type
Option::type() const
{
  return this->type_;
} // Syntax::Type Option::type() const

/*
 * -> result
 *
 * @return   the bool value
 */
bool
Option::value(TypeBool) const
{
  return this->value_.b;
} // bool Option::value(TypeBool) const

/*
 * -> result
 *
 * @return   the int value
 */
int
Option::value(TypeInt) const
{
  return this->value_.i;
} // int Option::value(TypeInt) const

/*
 * -> result
 *
 * @return   the unsigned value
 */
unsigned
Option::value(TypeUnsigned) const
{
  return this->value_.u;
} // unsigned Option::value(TypeUnsigned) const

/*
 * -> result
 *
 * @return   the double value
 */
double
Option::value(TypeDouble) const
{
  return this->value_.d;
} // double Option::value(TypeDouble) const

/*
 * -> result
 *
 * @return   the char value
 */
char
Option::value(TypeChar) const
{
  return this->value_.b;
} // char Option::value(TypeChar) const

/*
 * -> result
 *
 * @return   the string value
 */
std::string const&
Option::value(Typestring) const
{
  return this->value_string_;
} // std::string Option::value(Typestring) const

/*
 * -> result
 *
 * @return   the string value
 */
std::string const&
Option::value_string() const
{
  return this->value_string_;
} // std::string const& Option::value_string() const

/*
 * Description:	sets the value of the option
 *
 * @param    argv   the argument with the valule
 *
 * @return   the errorcode
 */
Option::Error
Option::set_value(char const* argv)
{
  this->error_ = OK;

  switch(this->type()) {
  case Syntax::BOOL:
    // cannot be
    this->error_ = UNKNOWN_ERROR;
    break;
  case Syntax::INT:
    {
      char* endptr;
      this->value_.i = strtol(argv, &endptr, 0);
      if ((endptr == argv)
	  || (*endptr != '\0'))
	this->error_ = FALSE_ARGUMENT;
      break;
    }
  case Syntax::UNSIGNED:
    {
      char* endptr;
      this->value_.u = strtoul(argv, &endptr, 0);
      if ((endptr == argv)
	  || (*endptr != '\0')
	  || (this->value_.u == UINT_MAX)
	  || (argv[0] == '-'))
	this->error_ = FALSE_ARGUMENT;
      break;
    }
  case Syntax::DOUBLE:
    {
      char* endptr;
      this->value_.d = strtod(argv, &endptr);
      if ((endptr == argv)
	  || (*endptr != '\0'))
	this->error_ = FALSE_ARGUMENT;
      break;
    }
  case Syntax::CHAR:
    this->value_.c = *argv;
    if (argv[1] != '\0')
      this->error_ = FALSE_ARGUMENT;
    break;
  case Syntax::BSTRING:
    this->value_string_ = argv;
    this->error_ = OK;
    break;
  case Syntax::END:
    // cannot be
    this->error_ = UNKNOWN_ERROR;
    break;
  }; // switch(this->type())

  return this->error();
} // Option::Error Option::set_value(char const* argv)

/*
 * writes the option error into the output stream
 *
 * @param    ostr    output stream
 * @param    error   the error
 *
 * @return   the output stream
 */
std::ostream&
operator<<(std::ostream& ostr, Option::Error const error)
{
  switch(error) {
  case Option::OK:
    ostr << "ok";
    break;
  case Option::NO_OPTION:
    ostr << "no option";
    break;
  case Option::UNKNOWN_OPTION:
    ostr << "unknown option";
    break;
  case Option::FALSE_ARGUMENT:
    ostr << "false argument";
    break;
  case Option::NO_ARGUMENT:
    ostr << "no argument";
    break;
  case Option::UNKNOWN_ERROR:
    ostr << "unknown error";
    break;
  } // switch(error)

  return ostr;
} // std::ostream& operator<<(std::ostream& ostr, Option::Error error)

