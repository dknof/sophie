//////////////////////////////////////////////////////////////////////////////
//
//   Copyright (C) 2002  by Diether Knof
//
//   This program is free software; you can redistribute it and/or 
//   modify it under the terms of the GNU General Public License as 
//   published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details. 
//   You can find this license in the file 'gpl.txt'.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA  02111-1307  USA
//
//  Contact:
//    Diether Knof dknof@gmx.de
//
//////////////////////////////////////////////////////////////////////////////

#include "getopt.h"

#include <string.h>

#include <stdarg.h>

using namespace GetOpt;

// remove the argument 'n'
void remove_argument(int& argc, char* argv[], unsigned n = 1);
// remove 'i' argument characters at position  '[n1][n2]'
// default: 'program -asdf' -> 'program -sdf'
void remove_argument_chars(int& argc, char* argv[], unsigned i = 1,
			   unsigned n1 = 1,  unsigned n2 = 1);

/*
 * gets the next option and removes it from the arguments
 *
 * @param    argc         the number of arguments
 * @param    argv         the arguments
 * @param    name         the (long) name of the option
 * @param    short_name   the short name of the option
 * @param    type         the type of the option
 * @param    ...          tripel 'name, short_name, type', ended by 'type == Syntax::END'
 *
 * @return   the next option
 */
Option
GetOpt::getopt(int& argc, char* argv[],
	       char const* const name, char short_name, Syntax::Type type,
	       ...)
{
  std::vector<Syntax> syntax;

  // put the arguments in a syntax list

  Syntax s;
  // reads the first syntax
  s.name = name;
  s.short_name = short_name;
  s.type = type;

  va_list ap;

  va_start(ap, type);

  // read all other syntax
  while (s.type != Syntax::END) {
    // adds the option-setting
    syntax.push_back(s);

    // reads the next syntax
    s.name = std::string{va_arg(ap, char const*)}; // (memory leak?)
    s.short_name = char(va_arg(ap, int));
    s.type = Syntax::Type(va_arg(ap, int));
  } // while(true)

  va_end(ap);

  return getopt(argc, argv, syntax);
} // Option getopt(int& argc, char* argv[], char const* const name, char short_name, Syntax::Type type, ...)

/*
 * gets the next option and removes it from the arguments
 *
 * @param    argc     the number of arguments
 * @param    argv     the arguments
 * @param    syntax   the syntax of the arguments
 *
 * @return   the next option
 */
Option
GetOpt::getopt(int& argc, char* argv[], std::vector<Syntax> const& syntax)
{
  Option option;

  if (argc == 0) {
    // error -- there must be one argument
    option.error_ = Option::NO_ARGUMENT;
    return option;
  } else if (   (argc == 1)
             || (   (argc == 2)
                 && (strncmp(argv[1], "--", 3) == 0)
                )
            ) {
    // no remaining argument (or only '--')
    option.error_ = Option::NO_OPTION;
    return option;
  } else { // if (argc > 1)
    if (   (argv[1][0] != '-')
        || (argv[1][1] == '\0')) {
      // a sole string
      option.error_ = Option::OK;
      option.name_ = "";
      option.type_ = Syntax::BSTRING;
      option.value_string_ = argv[1];

      remove_argument(argc, argv);

      return option;
    } // if (argv[1][0] != '-')
    else if (strncmp(argv[1], "--", 3) == 0) {
      // a sole string on the second position
      option.error_ = Option::OK;
      option.name_ = "";
      option.type_ = Syntax::BSTRING;
      option.value_string_ = argv[2];

      remove_argument(argc, argv, 2);

      return option;
    } // if (strncmp(argv[1], "--", 3) == 0)
    else if (strncmp(argv[1], "--", 2) == 0) {
      // long option
      option.value_string_ = argv[1];
      option.error_ = Option::UNKNOWN_OPTION;
      for (auto& s : syntax) {
        // search the option
        if (strncmp(argv[1] + 2, s.name.c_str(), s.name.length()) == 0) {

          option.type_ = s.type;
          option.name_ = s.name;

          if (option.type() == Syntax::BOOL) {
            if (argv[1][2 + s.name.size()] == '\0') {
              option.error_ = Option::OK;
              option.value_.b = true;
              remove_argument(argc, argv);

              return option;
            } // if (argument = '--name'

          } else { // if !(option.type() == Syntax::BOOL)

            if (argv[1][2 + s.name.size()] == '\0') {
              // only the name of the option is in this argument,
              // the value (if any) is in the next
              remove_argument(argc, argv);

              // test, whether there is a remaining argument
              if (argc == 1) {
                option.error_ = Option::NO_ARGUMENT;

                return option;

              } else { // if !(argc == 1)
                // the value of the option is now the argument number 1
                (option.value_string_ += " ") += argv[1];
                option.set_value(argv[1]);
                if (option.error() == Option::OK)
                  remove_argument(argc, argv);

                return option;

              } // if !(argc == 1)
            } // if (value in next argument)
            else if (argv[1][2 + s.name.size()] == '=') {

              option.set_value(argv[1] + 2 + s.name.length() + 1);
              if (option.error() == Option::OK)
                remove_argument(argc, argv);

              return option;

            } // if (argument = '--name=value')

          } // if !(option.type() == Syntax::BOOL)

        } // if (strncmp(argv[1] + 2, syntax, ) == 0)
      } // for (n < syntax.size())

    } else { // if !(long option)

      // long option
      option.value_string_ = argv[1];
      option.error_ = Option::UNKNOWN_OPTION;

      // short option
      for (auto const& s : syntax) {
        // search the option
        if (argv[1][1] == s.short_name) {

          option.type_ = s.type;
          option.name_ = s.name;

          if (option.type() == Syntax::BOOL) {
            // BOOL
            option.error_ = Option::OK;
            option.value_.b = true;
            remove_argument_chars(argc, argv);
            if (argv[1][1] == '\0') // argument is empty
              remove_argument(argc, argv);

            return option;

          } else { // if !(option.type() == Syntax::BOOL)

            if (argv[1][2] == '\0') {
              // the value is in the next argument
              remove_argument(argc, argv);
              (option.value_string_ += " ") += argv[1];
              option.set_value(argv[1]);
              if (option.error() == Option::OK)
                remove_argument(argc, argv);

              return option;

            } else { // if !(argv[1][2] == '\0')

              // the value is in this argument
              if (   (option.type() == Syntax::CHAR)
                  && !(argv[1][2] == '=')) {
                // CHAR
                option.error_ = Option::OK;
                option.value_.c = argv[1][2];
                remove_argument_chars(argc, argv, 2);
                if (argv[1][1] == '\0') // argument is empty
                  remove_argument(argc, argv);

                return option;

              } else { // if !(option.type() == Syntax::BOOL)
                // an '=' is ignored
                option.set_value(argv[1] + 2 +
                                 (argv[1][2] == '=' ? 1 : 0));
                if (option.error() == Option::OK)
                  remove_argument(argc, argv);

                return option;

              } // if !(option.type() == Syntax::BOOL)

            } // if (value in this argument)

          } // if !(option.type() == Syntax::BOOL)
        } // if (argv[1][1] == s.short_name)
      } // for (s : syntax)

    } // if (short option)

  } // if !(argc > 1)

  return option;
} // Option getopt(int& argc, char* argv[], std::vector<Syntax> const& syntax)

/*
 * remove the 'n'th argument from argc and argv
 *
 * @param    argc   the number of arguments
 * @param    argv   the arguments
 * @param    n      the argument to be removed
 */
void
remove_argument(int& argc, char* argv[], unsigned const n)
{
  if (n >= static_cast<unsigned>(argc))
    return; // nothing to do (error)

  for (auto i = n; i < static_cast<unsigned>(argc) - 1; ++i)
    argv[i] = argv[i + 1];

  argc -= 1;

  return;
} // void remove_argument(int& argc, char* argv[], unsigned n = 1)

/*
 * remove 'i' argument characters at position  '[n1][n2]'
 * default: 'program -asdf' -> 'program -sdf'
 *
 * @param    argc  the number of arguments
 * @param    argv  the arguments
 * @param    i     the number of characters to remove
 * @param    n1    the argument number of the character(s)
 * @param    n2    the position in the argument of the character(s)
 */
void
remove_argument_chars(int& argc, char* argv[], unsigned const i,
                      unsigned const n1,  unsigned const n2)
{
  if (n1 >= static_cast<unsigned>(argc))
    return; // nothing to do (error)

  if (n2 + i >= strlen(argv[n1])) {
    // the last argument
    argv[n1][n2] = '\0';
    return;
  }

  strcpy(argv[n1] + n2, argv[n1] + n2 + i);

  return;
} // void remove_argument_chars(int& argc, char* argv[], unsigned i = 1, unsigned n1 = 1, unsigned n2 = 1)
