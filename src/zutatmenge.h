#pragma once

#include "zutat.h"
#include "menge.h"

/** Zutat mit Menge (Einheit und Anzahl), optional ohne Menge
 ** Beispiel:
 ** 1 kg Zucker
 ** Salz
 **/
struct ZutatMenge {
  public:
    ZutatMenge() = default;
    explicit ZutatMenge(Zutat zutat);
    ZutatMenge(Zutat zutat, Menge menge);
    ZutatMenge(Zutat zutat, Einheit einheit, Anzahl anzahl);
    ZutatMenge(string zutat, string einheit, string anzahl);
    ZutatMenge(vector<string> zutatmenge);
  public:
    Zutat zutat;
    Menge menge;
}; // struct ZutatMenge

ostream& operator<<(ostream& ostr, ZutatMenge const& zutat_menge);
