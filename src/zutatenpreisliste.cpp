#include "constants.h"

#include "zutatenpreisliste.h"

/** Konstruktor
 **/
ZutatenPreisListe::ZutatenPreisListe()
{ }

/** -> Rückgabe
 **
 ** @param    zutat
 **
 ** @return   Ob die Zutat enthalten ist
 **/
bool
ZutatenPreisListe::enthaelt(Zutat const& zutat) const
{
  for (auto const& e : *this)
    if (e.zutat == zutat)
      return true;
  return false;
} // bool ZutatenPreisListe::enthaelt(Zutat zutat) const

/** -> Rückgabe
 **
 ** @param    zutat
 **
 ** @return   Menge der Zutat
 **/
Menge
ZutatenPreisListe::menge(Zutat const& zutat) const
{
  for (auto const& e : *this)
    if (e.zutat == zutat)
      return e.menge;
  return Menge();
} // Menge ZutatenPreisListe::menge(Zutat zutat) const

/** -> Rückgabe
 **
 ** @param    zutat
 **
 ** @return   Menge der Zutat
 **/
Preis
ZutatenPreisListe::preis(Zutat const& zutat) const
{
  for (auto const& e : *this)
    if (e.zutat == zutat)
      return e.preis;
  return 0;
} // Preis ZutatenPreisListe::preis(Zutat zutat) const

/** Fügt der Liste eine Zutat hinzu
 **
 ** @param    zutat    Zutat mit Menge
 **/
void
ZutatenPreisListe::hinzufuege(ZutatMengePreis const& zutat)
{
  this->push_back(zutat);
  return ;
 } // void ZutatenPreisListe::hinzufuege(ZutatMenge zutat)

/** Entfernt die Zutat aus der PreisListe
 **
 ** @param    zutat    Zutat
 **
 ** @return   ob die Zutat entfernt wurde
 **/
bool
ZutatenPreisListe::entferne(Zutat const& zutat)
{
  bool ergebnis = false;
  for (auto z = this->begin(); z != this->end(); ) {
    if (z->zutat == zutat) {
      z = this->erase(z);
      ergebnis = true;
    } else {
      ++z;
    }
  } // for (auto z : this)
  return ergebnis;
} // bool ZutatenPreisListe::entferne(Zutat zutat)

/** ZutatenPreisListe ausgeben
 **
 ** @param   ostr            Ausgabestrom
 ** @param   zutaten_liste   ZutatenPreisListe
 **
 ** @return  Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, ZutatenPreisListe const& zutaten_liste)
{
  for (auto& e : zutaten_liste)
    ostr << e << '\n';
  return ostr;
} // ostream& operator<<(ostream& ostr, ZutatenPreisListe zutaten_liste)
