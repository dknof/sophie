#include "constants.h"

#include "zutatmengepreis.h"

/** Konstruktor
 **/
ZutatMengePreis::ZutatMengePreis(Zutat const zutat) :
  ZutatMenge(zutat)
{ }

/** Konstruktor
 **/
ZutatMengePreis::ZutatMengePreis(Zutat const zutat,
                                 Menge const menge) :
  ZutatMenge(zutat, menge)
{ }

/** Konstruktor
 **/
ZutatMengePreis::ZutatMengePreis(Zutat const zutat,
                                 Menge const menge,
                                 Preis const preis) :
  ZutatMenge(zutat, menge),
  preis(preis)
{ }

/** Konstruktor
 **/
ZutatMengePreis::ZutatMengePreis(vector<string> const zutat_menge_preis)
{
  if (zutat_menge_preis.size() == 1) {
    this->zutat = Zutat(zutat_menge_preis[0]);
  } else if (   (zutat_menge_preis.size() == 3)
             || (zutat_menge_preis.size() == 4)) {
    this->zutat = Zutat(zutat_menge_preis[0]);
    if (   (zutat_menge_preis[1] != "NULL")
        && (zutat_menge_preis[1] != "") ) {
      this->menge = Menge(std::stod(zutat_menge_preis[2]), Einheit(zutat_menge_preis[1]));
      if (   (zutat_menge_preis.size() == 4)
          && (zutat_menge_preis[3] != "NULL")
          && (zutat_menge_preis[3] != "")) {
        try {
          this->preis = stod(zutat_menge_preis[3]);
        } catch(...) {
        }
      }
    }
  } else {
    cerr << "ZutatMengePreis(vector<string>): Der Vektor hat eine falsche Größe: " + to_string(zutat_menge_preis.size()) << '\n';
    throw "ZutatMengePreis(vector<string>): Der Vektor hat eine falsche Größe: " + to_string(zutat_menge_preis.size());
    // Fehler
  }
} // ZutatMenge::ZutatMenge(vector<string> zutat_menge)

/** ZutatMengePreis ausgeben
 **/
ostream&
operator<<(ostream& ostr, ZutatMengePreis const& zutat_menge_preis)
{
  ostr << zutat_menge_preis.zutat;
  if (zutat_menge_preis.menge)
    ostr << ' ' << zutat_menge_preis.menge;
  if (zutat_menge_preis.preis >= 0)
    ostr << " = " << zutat_menge_preis.preis;
  return ostr;
} // ostream& operator<<(ostream& ostr, ZutatMengePreis zutat_menge_preis)
