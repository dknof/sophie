#pragma once

#include "zutatmenge.h"

/** Zutat mit Menge (Einheit und Anzahl) und Preis, optional ohne Preis / ohne Preis und Menge
 ** Beispiel:
 ** Zucker
 ** Zucker  1 kg
 ** Zucker  1 kg = 1.99
 **/
struct ZutatMengePreis : public ZutatMenge {
  public:
    ZutatMengePreis() = default;
    explicit ZutatMengePreis(Zutat zutat);
    ZutatMengePreis(Zutat zutat, Menge menge);
    ZutatMengePreis(Zutat zutat, Menge menge, Preis preis);
    ZutatMengePreis(vector<string> zutatmengepreis);
  public:
    Preis preis = -1;
}; // struct ZutatMengePreis : public ZutatMenge

ostream& operator<<(ostream& ostr, ZutatMengePreis const& zutat_menge_preis);
