#include "constants.h"

#include "class/getopt/getopt.h"

#include "einstellung.h"

#ifdef USE_UI_GTKMM
#include "ui/gtkmm/gtkmm.h"
#endif
#include "ui/text/text.h"

#include "datenbank/datenbank.h"

// Für 'mkdir'
#include <sys/stat.h>

// Globale Variablen

// Lizenz
#include "gpl.string"
// Version
Version version(__DATE__);

// Datenbank
unique_ptr<Datenbank::Datenbank> db;

// Benutzerschnittstelle
unique_ptr<UI> ui;

/** Hauptfunktion
 **
 ** @param    argc   Anzahl der Argumente (wird ignoriert)
 ** @param    argv   Argumente (wird ignoriert)
 **
 ** @return   0/Fehler
 **/
int
main(int argc, char* argv[])
{
  string datei = "";

  { // Kommandozeilenargumente durchgehen
    GetOpt::Option option;
    while ((option =
            GetOpt::getopt(argc, argv,
                           {{"hilfe",   'h',  GetOpt::Syntax::BOOL},
                           {"help",     '?',  GetOpt::Syntax::BOOL},
                           {"version",  'v',  GetOpt::Syntax::BOOL},
                           {"debug",    '\0', GetOpt::Syntax::BOOL},
                           {"license",  'L',  GetOpt::Syntax::BOOL},
                           {"lizenz",   '\0', GetOpt::Syntax::BOOL},
                           {"defines",  '\0', GetOpt::Syntax::BOOL},
                           {"ui",       'u',  GetOpt::Syntax::BSTRING},
                           }) // { }
           )) {
      if (option.fail()) {
        // Fehlbedienung
        cerr << argv[0] << "\n"
          << "Falsche Bedienung\n"
          << option.error() << " " << option.value_string() << "\n"
          << "Für Hilfe: '" << argv[0] << " --hilfe'"
          << endl;
        return EXIT_FAILURE;
      } // if (option.fail())

      if (option.name() == "hilfe") {
        // Hilfe ausgeben
#include "hilfe.string"

        cout << hilfe_string << endl;
        return EXIT_SUCCESS;
      } else if (option.name() == "version") {
        // Ausgabe der Version
        cout << "sophie \n"
          << "Kompiliert: " << __DATE__ << ", " << __TIME__ << '\n'
          << "Kompiler: "
#if   defined(__GNUG__)
          << "g++"
#elif defined(__clang__)
          << "clang"
#elif defined(__ICC)
          << "icc"
#elif defined(__MSC_VER)
          << "msc"
#endif
          << '\n'
#ifdef __VERSION__
          << "  version: " << __VERSION__ << '\n'
#endif
#ifdef __cplusplus
          << "  C++ version: " << __cplusplus << '\n'
#endif
          ;
        return EXIT_SUCCESS;
      } else if (   (option.name() == "license")
                 || (option.name() == "lizenz") ) {
        // output of the license (GPL)
#include "gpl.string"
        cout << "sophie -- Lizenz:\n\n"
          << GPL_string
          << endl;
        return EXIT_SUCCESS;
      } else if (option.name() == "defines") {
        cout << "sophie Module:\n";
#ifdef USE_UI_TEXT
        cout << "  USE_UI_TEXT  = " << USE_UI_TEXT << '\n';
#endif
#ifdef USE_UI_GTKMM
        cout << "  USE_UI_GTKMM = " << USE_UI_GTKMM << '\n';
#endif
        return EXIT_SUCCESS;
      } else if (option.name() == "debug") {
        // ToDo
      } else if (option.name() == "ui") {
        if (option.value_string() == "text")
          ::ui = make_unique<UI_Text>();
#ifdef USE_UI_GTKMM
        else if (option.value_string() == "gtkmm")
          ::ui = make_unique<UI_Gtkmm>();
#endif
        else {
          cerr << "ui '" << option.value_string() << "' nicht implementiert. Wähle „text” oder „gtkmm“.\n"
            << "Beende sophie.\n";
          return EXIT_FAILURE;
        } // if (option.value_string() == "...")

      } else if (option.name().empty()) {
        // Datei angeben
        if (datei.empty())
          datei = option.value_string();

      } else { // if (option.name() == ...)
        cerr << "Option '" << option.name() << "' unbekannt!" << endl;
        exit(EXIT_FAILURE);
      } // if (option.name() == ...)
    } // while (getopt())
  } // Kommandozeilenargumente durchgehen

  if (!::ui) {
    // Benutzerschnittstelle einrichten
#ifdef USE_UI_GTKMM
    ::ui = make_unique<UI_Gtkmm>();
#else
    ::ui = make_unique<UI_Text>();
#endif
  }

  // Einstellungen laden
  ::einstellung.lade();
  if (!datei.empty())
    ::einstellung.setze(Einstellung::DATEIPFAD,
                        (datei == "-" ? ""s : datei));

  // Datenbank einrichten
  ::db = make_unique<Datenbank::Datenbank>();
  ::db->lade();


  // Vorbereitung
  ::ui->initialisiere();

  // Die Hauptroutine
  ::ui->starte();

  // alles schließen
  if (::einstellung(Einstellung::EINSTELLUNGEN_SPEICHERN))
    ::einstellung.speicher();
  ::ui->schliesse();

  // Die Datenbank als Datenbankdatei speichern
  //::db->speicher_sqlite3("rezeptbuch.sqlite3");
  // Die Datenbank im sophie-Format speichern
  //::db->speicher();

  return EXIT_SUCCESS;
} // int main()
