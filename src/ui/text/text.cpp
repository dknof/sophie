#include "constants.h"

#include "text.h"
#include "eingabe.h"

#include "einstellungen.h"
#include "hauptmenue.h"

namespace UI_TEXT {

/** Konstruktor
 **/
UI_Text::UI_Text() :
  istr(&cin),
  ostr(&cout),
  hauptmenue_(make_unique<Hauptmenue>(*this))
{ }

/** Konstruktor
 **
 ** @param   istr   Eingabestrom
 ** @param   ostr   Ausgabestrom
 **/
UI_Text::UI_Text(istream& istr, ostream& ostr) :
  istr(&istr),
  ostr(&ostr),
  hauptmenue_(make_unique<Hauptmenue>(*this))
{ }

/** Destruktor
 **/
UI_Text::~UI_Text()
{ }

/** Hauptroutine
 ** Liest Zeilen ein und interpretiert sie
 **/
void
UI_Text::starte()
{
  Eingabe eingabe(*this);
  for (eingabe.lese_zeile();
       this->istr->good() && !this->istr->eof();
       eingabe.lese_zeile()) {

    // Zeile interpretieren und Befehl ausführen

    // Allgemeiner Befehl
    if (this->befehl_allgemein(eingabe))
      continue;
    // Spezialfall: Programm beenden
    if (   (eingabe.pruefe_befehl("Beenden"))
        || (eingabe.pruefe_befehl("quit"))
        || (eingabe.pruefe_befehl("q"))
        || (eingabe.pruefe_befehl("exit")))
      break;
    // Befehl zur Kontextnavigation
    if (this->befehl_kontext(eingabe))
      continue;
    // Befehl an Kontext übergeben
    if (this->kontext().befehl(eingabe)) {
      continue;
    }
    // Lizenz
    if (eingabe.pruefe_befehl("Lizenz", "L")) {
      *this->ostr << ::GPL_string;
      continue;
    }
    if (eingabe.pruefe_befehl("Version", "V")) {
      *this->ostr << "Version: " << ::version << '\n';
      continue;
    }
    // Einstellungen
    if (eingabe.pruefe_befehl("Einstellungen", "E")) {
      if (eingabe.text().empty()) {
        if (this->kontext().name() != "Einstellungen")
          this->zu_kontext(make_unique<Einstellungen>(*this));
        continue;
      }
      auto e = Eingabe(*this, eingabe.text());
      if (Einstellungen(*this).befehl(e))
        continue;
    }
    // direkter Sprung
    if (this->befehl_sprung(eingabe))
      continue;

    // unbekannter Befehl
    *this->ostr << "Befehl '" << eingabe.text() << "' unbekannt\n";
  } // while (this->istr->good() && !this->istr->eof())
} // void UI_Text::starte()

/** Fehlermeldung
 **
 ** @param   text   Text der Meldung
 **/
void
UI_Text::fehler(string const text)
{
  *this->ostr << text << endl;
} // void UI_Text::fehlen(string text)

/** -> Rückgabe
 **
 ** @return   der aktuelle Kontext
 **/
Kontext&
UI_Text::kontext()
{
  return (this->kontext_.empty()
          ? *this->hauptmenue_
          : *this->kontext_.back());
} // Kontext& UI_Text::kontext() const

/** -> Rückgabe
 **
 ** @return   der aktuelle Kontext
 **/
Kontext const&
UI_Text::kontext() const
{
  return (this->kontext_.empty()
          ? *this->hauptmenue_
          : *this->kontext_.back());
} // Kontext const& UI_Text::kontext() const

/** wechselt den Kontext
 **
 ** @param   kontext   der neue Kontext
 **/
void
UI_Text::zu_kontext(unique_ptr<Kontext> kontext)
{
  this->kontext_.push_back(std::move(kontext));
} // void UI_Text::zu_kontext(unique_ptr<Kontext> kontext)

/** wechselt zum vorigen Kontext
 **/
void
UI_Text::zu_vorigem_kontext()
{
  if (!this->kontext_.empty())
    this->kontext_.pop_back();
} // void UI_Text::zu_vorigem_kontext()

/** liest eine Zeile aus dem Eingabestrom ein
 **
 ** @return   Eingelesene Zeile
 **
 ** @todo     auf readline umstellen
 **/
string
UI_Text::lese_zeile()
{
  // Eingabe abwarten
  string zeile;
  std::getline(*this->istr, zeile);

  return zeile;
} // string UI_Text::lese_zeile()

/** -> Rückgabe
 **
 ** @return   der Prompt
 **/
string
UI_Text::prompt() const
{
  // Prompt anzeigen
  switch (this->kontext_.size()) {
  case 0:
    return "→/: ";
  case 1:
    return "→" + this->kontext().name() + ": ";
  case 2:
    return ("→"
            + this->kontext_.front()->name()
            + "/" + this->kontext().name()
            + ": ");
  case 3:
    return ("→"
            + this->kontext_.front()->name()
            + "/" + this->kontext_[1]->name()
            + "/" + this->kontext().name()
            + ": ");
  default: // mehr als 3
    return ("→"s
            + "..[" + std::to_string(this->kontext_.size() - 2) + "]../"
            + this->kontext_[this->kontext_.size() - 2]->name()
            + "/" + this->kontext().name()
            + ": ");
  } // switch (this->kontext_.size())
} // string UI_Text::prompt() const

/** Einen allgemeinen Befehl ausführen
 **
 ** @param    eingabe   Eingabe
 **
 ** @return   Ob ein Befehl erkannt und durchgeführt wurde
 **/
bool
UI_Text::befehl_allgemein(Eingabe& eingabe)
{
  // ignorieren
  if (eingabe.ist_leer()) {
    return true;
  } else if (eingabe.text()[0] == '#') {
    return true;
  } else if (eingabe.text()[0] == '\'') {
    *this->ostr << eingabe.text() << '\n';
    return true;
  } else if (   eingabe.pruefe_befehl("Hilfe", "H")
             || eingabe.pruefe_befehl("?")) {
    // Hilfe
    *this->ostr
      << "Befehle können abgekürzt werden, außerdem wird nicht auf Großschreibung geachtet.\n"
      << "Beispiel: 'h' für 'Hilfe'\n"
      << "Zeilen mit # am Anfang werden ignoriert.\n"
      << "Zeilen mit ' am Anfang werden ausgegeben.\n"
      << "\n"
      << "Allgemeine Befehle:\n"
      << "Hilfe:   Die Hilfe anzeigen\n"
      << "Lizenz:  Die Lizenz anzeigen\n"
      << "Version: Die Version anzeigen\n"
      << "Einstellungen: In die Einstellungen wechseln\n"
      << "Beenden: Sophie beenden\n"
      << "/:       Zurück zum Hauptmenü\n"
      << "..:      Zum übergeordneten Kontext zurückgehen\n"
      << "„Name“:  Direkt zum Objekt „Name“ wechseln (zum Beispiel „Zucker“)\n"
      << "\n"
      << "Befehle im Kontext " << this->kontext().name() << '\n'
      << this->kontext().hilfe();
    return true;
  }

  return false;
} // bool UI_Text::befehl_allgemein(Eingabe& eingabe)

/** Einen Befehl zur Kontextnavigation ausführen
 ** Wechsel nach vorne werden nicht hier sondern in den einzelnen Kontexten interpretiert
 **
 ** @param    eingabe   Eingabe
 **
 ** @return   Ob ein Befehl erkannt und durchgeführt wurde
 **/
bool
UI_Text::befehl_kontext(Eingabe& eingabe)
{
  // an den Anfang
  if (eingabe == "/") {
    this->kontext_.clear();
    return true;
  }
  // eine Ebene zurück
  if (eingabe == "..") {
    this->zu_vorigem_kontext();
    return true;
  }
  // In einen speziellen Kontext wechseln
  auto befehl = eingabe.pruefe_befehle({
                                       "/Rezepte",
                                       "/Zutaten",
                                       "/Zutatenkategorieen", "/ZK",
                                       "/Einheiten",
                                       "/Einheitenumrechner", "/EU",
                                       "/Einkaufsliste", "/EL",
                                       "/Kategorieen",
                                       "/Eigenschaften",
                                       "/Menüs",
                                       "/Geschäfte",
                                       "/Vorrat",
                                       });
  if (!befehl.empty()) {
    this->kontext_.clear();
    eingabe.loesche_erstes_zeichen();
    this->hauptmenue_->befehl(eingabe);
    return true;
  }

  return false;
} // bool UI_Text::befehl_kontext(Eingabe& eingabe)

/** Einen Sprung-Befehl ausführen
 ** Ein Sprung in eine Element, zum Beispiel Zutat „Zucker“ über die Eingabe des Namens.
 **
 ** @param    eingabe   Eingabe
 **
 ** @return   Ob ein Befehl erkannt und durchgeführt wurde
 **/
bool
UI_Text::befehl_sprung(Eingabe& eingabe)
{
  return false;
} // bool UI_Text::befehl_sprung(Eingabe& eingabe)

} // namespace UI_TEXT
