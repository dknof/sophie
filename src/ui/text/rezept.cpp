#include "constants.h"
#include "rezept.h"

#include "text.h"
#include "eingabe.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Rezept::Rezept(UI_Text& ui, ::Rezept rezept) :
  Kontext(ui, rezept.name()),
  rezept(rezept)
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Rezeptliste
 **/
string 
Rezept::hilfe() const
{
  return 
    "Zeige:     Rezept anzeigen\n"
    "Lösche:    Rezept löschen\n"
    "Eigenschaften ändern:\n"
    "  Name:REWE       Name auf REWE ändern\n"
    "  Bild:REWE.png   Bild ändern\n"
    "  Bild:-          Bild löschen\n"
    ;
} // string Rezept::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Rezept::befehl(Eingabe& eingabe)
{
  { // Listenbefehle
    auto const befehl = eingabe.pruefe_befehle({"Zeige",
                                               "Lösche"});

    if ((befehl == "Zeige") && eingabe.text().empty()) {
      this->zeige(eingabe);
      return true;
    } else if ((befehl == "Lösche") && eingabe.text().empty())  {
      this->loesche(eingabe);
      return true;
    } else if (eingabe.zeile_config().separator) {
      this->aendere(eingabe);
      return true;
    }
  } // Listenbefehle

  return false;
} // bool Rezept::befehl(Eingabe& eingabe)

/** Zeigt die Rezept an
 **/
void
Rezept::zeige(Eingabe& eingabe)
{
  *this->ui->ostr << "Name: " << this->rezept.name();
  *this->ui->ostr << "Bild: " << this->rezept.bild();
  *this->ui->ostr << "\n";
} // void zeige(Eingabe& eingabe)

/** Gibt die Rezept in einer Zeile aus
 **/
void
Rezept::schreibe()
{
  auto const name = this->rezept.name();
  auto const bild = this->rezept.bild();

  *this->ui->ostr << name;

  if (!bild.empty())
    *this->ui->ostr << " Bild:" << bild;
} // void Rezept::schreibe()

/** Löscht eine Rezept
 ** Eingabe: Name (optional)
 **
 ** @param    eingabe    Eingabe
 **/
void
Rezept::loesche(Eingabe& eingabe)
{
  this->rezept.loesche();
  this->rezept = ::Rezept();
  this->ui->zu_vorigem_kontext();
} // void loesche(Eingabe& eingabe)

/** Ändert einen Wert
 **
 ** @param    eingabe    Eingabe
 **/
void
Rezept::aendere(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    Config config = eingabe.naechstes_config();
    if (config.name == "Name") {
      string name = config.value;
      this->rezept.setze_name(name);
    } else if (config.name == "Bild") {
      this->rezept.setze_bild(config.value);
    } else {
      cerr << "Rezept::aendere(eingabe)\n"
        << "Befehl umbekannt: " << config << '\n';
    }
  } // while (!eingabe.ist_leer())
} // void aendere(Eingabe& eingabe)

} // namespace UI_TEXT
