#pragma once

#include "kontext.h"
#include "../../einheitenumrechner.h"

namespace UI_TEXT {

  /** Die Formeln zum Umrechnen von Einheitenumrechner
   **
   ** @todo   Um einen Filter erweitern, um nur bestimmte Einheitenumrechner aufzulisten
   **/
  class Einheitenumrechner : public Kontext {
    public:
      Einheitenumrechner(UI_Text& ui);

      string hilfe() const;

      // Einen Befehl ausführen
      bool befehl(Eingabe& eingabe);

    private:
      // Alle Einheitenumrechner auflisten
      void liste(Eingabe& eingabe);
      // Eine neue Einheit eingeben
      void neu(Eingabe& eingabe);
      // Eine Einheit löschen
      void loesche(Eingabe& eingabe);
      // Eine Umrechnung rechnen
      bool umrechnen(Eingabe& eingabe);
  }; // class Einheitenumrechner : public Kontext

} // namespace UI_TEXT
