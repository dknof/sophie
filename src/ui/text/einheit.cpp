#include "constants.h"
#include "einheit.h"

#include "text.h"
#include "eingabe.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Einheit::Einheit(UI_Text& ui, ::Einheit einheit) :
  Kontext(ui, einheit.name()),
  einheit(einheit)
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Einheitliste
 **/
string 
Einheit::hilfe() const
{
  return 
    "Zeige:     Einheit anzeigen\n"
    "Lösche:    Einheit löschen\n"
    "Eigenschaften ändern:\n"
    "  Name:Gramm        Name auf Gramm ändern\n"
    "  Name:Glas/Gläser  Name auf Glas, Mehrzahl Gläser, ändern\n"
    "  Bild:pfad         Pfad für das Bild auf pfad ändern\n"
    ;
} // string Einheit::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Einheit::befehl(Eingabe& eingabe)
{
  { // Listenbefehle
    auto const befehl = eingabe.pruefe_befehle({"Zeige",
                                               "Lösche"});

    if ((befehl == "Zeige") && eingabe.text().empty()) {
      this->zeige(eingabe);
      return true;
    } else if ((befehl == "Lösche") && eingabe.text().empty())  {
      this->loesche(eingabe);
      return true;
    } else if (eingabe.zeile_config().separator) {
      this->aendere(eingabe);
      return true;
    }
  } // Listenbefehle

  return false;
} // bool Einheit::befehl(Eingabe& eingabe)

/** Zeigt die Einheit an
 **/
void
Einheit::zeige(Eingabe& eingabe)
{
  *this->ui->ostr << "Name: " << this->einheit.name();
  if (!this->einheit.name_mehrzahl().empty())
    *this->ui->ostr << '/' << this->einheit.name_mehrzahl();
  if (!this->einheit.bild().empty())
    *this->ui->ostr << " Bild:" << this->einheit.bild();
  *this->ui->ostr << "\n";
} // void zeige(Eingabe& eingabe)

/** Gibt die Einheit in einer Zeile aus
 **/
void
Einheit::schreibe()
{
  auto const name = this->einheit.name();
  auto const mehrzahl = this->einheit.name_mehrzahl();
  auto const bild = this->einheit.bild();

  *this->ui->ostr << name;
  if (   !mehrzahl.empty()
      && (mehrzahl != name))
    *this->ui->ostr << '/' << mehrzahl;

  if (!bild.empty()) {
    if (bild.find(' ') == string::npos)
      *this->ui->ostr << " Bild:" << bild;
    else
      *this->ui->ostr << " Bild:\"" << bild << '"';
  } // if (!bild.empty())
} // void Einheit::schreibe()

/** Löscht eine Einheit
 ** Eingabe: Name (optional)
 **
 ** @param    eingabe    Eingabe
 **/
void
Einheit::loesche(Eingabe& eingabe)
{
  this->einheit.loesche();
  this->einheit = ::Einheit();
  this->ui->zu_vorigem_kontext();
} // void loesche(Eingabe& eingabe)

/** Ändert einen Wert
 **
 ** @param    eingabe    Eingabe
 **/
void
Einheit::aendere(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    Config config = eingabe.naechstes_config();
    if (config.name == "Name") {
      string name = config.value;
      // Gegebenenfalls Plural finden
      string name_mehrzahl;
      auto p = name.find('/');
      if (p != string::npos) {
        name_mehrzahl = string(name, p+1, string::npos);
        name.erase(p, string::npos);
      }
      this->einheit.setze_name(name);
      if (!name_mehrzahl.empty())
        this->einheit.setze_name_mehrzahl(name_mehrzahl);
    } else if (config.name == "Mehrzahl") {
      this->einheit.setze_name_mehrzahl(config.value);
    } else if (config.name == "Bild") {
      this->einheit.setze_bild(config.value);
    } else {
      cerr << "Einheit::aendere(eingabe)\n"
        << "Befehl umbekannt: " << config << '\n';
    }
  } // while (!eingabe.ist_leer())
} // void aendere(Eingabe& eingabe)

} // namespace UI_TEXT
