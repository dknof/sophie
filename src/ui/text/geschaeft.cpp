#include "constants.h"
#include "geschaeft.h"

#include "text.h"
#include "eingabe.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Geschaeft::Geschaeft(UI_Text& ui, ::Geschaeft geschaeft) :
  Kontext(ui, geschaeft.name()),
  geschaeft(geschaeft)
{ }

/** -> Rückgabe
 **/
string 
Geschaeft::hilfe() const
{
  return 
    "Zeige:     Geschaeft anzeigen\n"
    "Lösche:    Geschaeft löschen\n"
    "Liste:     Zutaten anzeigen\n"
    "Eigenschaften ändern:\n"
    "  Name:REWE       Name auf REWE ändern\n"
    "  Bild:REWE.png   Bild/Logo ändern\n"
    "  Logo:REWE.png   Bild/Logo ändern\n"
    "  Bild:-          Bild/Logo löschen\n"
    "Zutaten hinzufügen (sortierte Liste):\n"
    "  +Zucker         Zucker am Ende der Liste hinzufügen\n"
    "  +Zucker >       Zucker am Ende der Liste hinzufügen\n"
    "  +Zucker <       Zucker am Anfang der Liste hinzufügen\n"
    "  +Zucker < Mehl  Zucker vor Mehl hinzufügen\n"
    "  +Zucker > Mehl  Zucker hinter Mehl hinzufügen\n"
    "  +Zucker 1 kg    Zucker mit Verkaufsmenge hinzufügen\n"
    "  +Zucker 1 kg 0.99   Zucker mit Verkaufsmenge und Preis hinzufügen\n"
    "Zutaten umsortieren:\n"
    "  Zucker >       Zucker an das Ende der Liste verschieben\n"
    "  Zucker <       Zucker an den Anfang der Liste verschieben\n"
    "  Zucker < Mehl  Zucker vor Mehl verschieben\n"
    "  Zucker > Mehl  Zucker hinter Mehl verschieben\n"
    "  Zucker <> Mehl Zucker und Mehl vertauschen\n"
    "Zutaten entfernen:\n"
    "  -Zucker        Zucker aus der Liste entfernen\n"
    ;
} // string Geschaeft::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Geschaeft::befehl(Eingabe& eingabe)
{
  { // Umsortieren der Zutaten
    auto zeile = eingabe.text();
    ::Zutat zutat1;
    ::Zutat zutat2;
    if (eingabe.parse(zeile, zutat1, "<")) {
      this->geschaeft.verschiebe_an_anfang(zutat1);
      return true;
    } else if (eingabe.parse(zeile, zutat1, ">")) {
      this->geschaeft.verschiebe_an_ende(zutat1);
      return true;
    } else if (eingabe.parse(zeile, zutat1, "<", zutat2)) {
      this->geschaeft.verschiebe_vor(zutat1, zutat2);
      return true;
    } else if (eingabe.parse(zeile, zutat1, ">", zutat2)) {
      this->geschaeft.verschiebe_hinter(zutat1, zutat2);
      return true;
    } else if (eingabe.parse(zeile, zutat1, "<>", zutat2)) {
      this->geschaeft.vertausche(zutat1, zutat2);
      return true;
    }
  } // Umsortieren der Zutaten
  { // Listenbefehle
    auto const befehl
      = eingabe.pruefe_befehle({"Zeige",
                               "Lösche",
                               "Liste",
                               "hinzufüge", "+",
                               "entferne", "-",
                               });

    if ((befehl == "Zeige") && eingabe.text().empty()) {
      this->zeige(eingabe);
      return true;
    } else if ((befehl == "Lösche") && eingabe.text().empty())  {
      this->loesche(eingabe);
      return true;
    } else if ((befehl == "Liste") && eingabe.text().empty()) {
      this->zeige_zutaten(eingabe);
      return true;
    } else if ((befehl == "hinzufüge") || (befehl == "+")) {
      this->hinzufuege(eingabe);
      return true;
    } else if ((befehl == "entferne") || (befehl == "-")) {
      this->entferne(eingabe);
      return true;
    } else if (eingabe.zeile_config().separator) {
      this->aendere(eingabe);
      return true;
    } else {
    }
  } // Listenbefehle

  return false;
} // bool Geschaeft::befehl(Eingabe& eingabe)

/** Zeigt das Geschäft an
 **/
void
Geschaeft::zeige(Eingabe& eingabe)
{
  *this->ui->ostr << "Name: " << this->geschaeft.name();
  *this->ui->ostr << "Bild: " << this->geschaeft.bild();
  *this->ui->ostr << "Zutaten:\n" << this->geschaeft.zutaten();
  *this->ui->ostr << "\n";
} // void zeige(Eingabe& eingabe)

/** Zeigt die Zutaten an
 **/
void
Geschaeft::zeige_zutaten(Eingabe& eingabe)
{
  *this->ui->ostr << this->geschaeft.zutaten();
} // void zeige_zutaten(Eingabe& eingabe)

/** Gibt die Geschäft in einer Zeile aus
 **/
void
Geschaeft::schreibe()
{
  auto const name = this->geschaeft.name();
  auto const bild = this->geschaeft.bild();

  *this->ui->ostr << name;

  if (!bild.empty())
    *this->ui->ostr << " Bild:" << bild;

  // ToDo: Zutaten mit ausgeben
} // void Geschaeft::schreibe()

/** Löscht eine Geschäft
 ** Eingabe: Name (optional)
 **/
void
Geschaeft::loesche(Eingabe& eingabe)
{
  this->geschaeft.loesche();
  this->geschaeft = ::Geschaeft();
  this->ui->zu_vorigem_kontext();
} // void loesche(Eingabe& eingabe)

/** Gibt einen neuen Eintrag ein
 ** Eingabe (Beispiele)
 **   1 kg Zucker
 **   Salz
 **
 ** @param    eingabe    Eingabe
 **/
void
Geschaeft::hinzufuege(Eingabe& eingabe)
{
  if (eingabe.ist_leer())
    return ;

  auto text = eingabe.text();
  ::Zutat zutat;
  ::Zutat zutat_referenz;
  Menge menge;
  Preis preis;
  if (   eingabe.parse(text, zutat)
      || eingabe.parse(text, zutat, ">")) {
    this->geschaeft.hinzufuege(zutat);
  } else if (   eingabe.parse(text, zutat, menge)
             || eingabe.parse(text, zutat, menge, ">")) {
    this->geschaeft.hinzufuege(zutat, menge);
  } else if (   eingabe.parse(text, zutat, menge, "=", preis)
             || eingabe.parse(text, zutat, menge, "=", preis, ">")) {
    this->geschaeft.hinzufuege(zutat, menge, preis);
  } else if (eingabe.parse(text, zutat, "<")) {
    this->geschaeft.hinzufuege(zutat);
    this->geschaeft.verschiebe_an_anfang(zutat);
  } else if (eingabe.parse(text, zutat, menge, "<")) {
    this->geschaeft.hinzufuege(zutat, menge);
    this->geschaeft.verschiebe_an_anfang(zutat);
  } else if (eingabe.parse(text, zutat, menge, "=", preis, "<")) {
    this->geschaeft.hinzufuege(zutat, menge, preis);
    this->geschaeft.verschiebe_an_anfang(zutat);
  } else if (eingabe.parse(text, zutat, "<", zutat_referenz)) {
    this->geschaeft.hinzufuege(zutat);
    this->geschaeft.verschiebe_vor(zutat, zutat_referenz);
  } else if (eingabe.parse(text, zutat, menge, "<", zutat_referenz)) {
    this->geschaeft.hinzufuege(zutat, menge);
    this->geschaeft.verschiebe_vor(zutat, zutat_referenz);
  } else if (eingabe.parse(text, zutat, menge, "=", preis, "<", zutat_referenz)) {
    this->geschaeft.hinzufuege(zutat, menge, preis);
    this->geschaeft.verschiebe_vor(zutat, zutat_referenz);
  } else if (eingabe.parse(text, zutat, ">", zutat_referenz)) {
    this->geschaeft.hinzufuege(zutat);
    this->geschaeft.verschiebe_hinter(zutat, zutat_referenz);
  } else if (eingabe.parse(text, zutat, menge, ">", zutat_referenz)) {
    this->geschaeft.hinzufuege(zutat, menge);
    this->geschaeft.verschiebe_hinter(zutat, zutat_referenz);
  } else if (eingabe.parse(text, zutat, menge, "=", preis, ">", zutat_referenz)) {
    this->geschaeft.hinzufuege(zutat, menge, preis);
    this->geschaeft.verschiebe_hinter(zutat, zutat_referenz);
  } else {
    *this->ui->ostr << "Konnte „" << text << "“ nicht erkennen. Es muss eine Zutat oder Zutat und Menge oder Zutat und Menge = Preis sein.\n";
  }
} // void Geschaeft::hinzufuege(Eingabe& eingabe)

/** Entfernt die angegebene Zutat
 **
 ** @param    eingabe    Eingabe (Zutat)
 **/
void
Geschaeft::entferne(Eingabe& eingabe)
{
  if (eingabe.ist_leer())
    return ;

  auto text = eingabe.text();
  ::Zutat zutat;

  if (text == "*") {
    this->geschaeft.entferne_alles();
  } else if (eingabe.parse(text, zutat)) {
    if (!this->geschaeft.entferne(zutat))
      *this->ui->ostr << "Zutat '" << zutat.name() << "' ist nicht in dem Geschäft und konnte daher nicht entfernt werden.\n";
  } else {
    *this->ui->ostr << "Konnte „" << text << "“ nicht erkennen. Es muss eine Zutat sein.\n";
  }
} // void Geschaeft::entferne(Eingabe& eingabe)

/** Ändert einen Wert
 **/
void
Geschaeft::aendere(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    Config config = eingabe.naechstes_config();
    if (config.name == "Name") {
      string name = config.value;
      this->geschaeft.setze_name(name);
    } else if (   (config.name == "Bild")
               || (config.name == "Logo") ) {
      this->geschaeft.setze_bild(config.value);
    } else {
      cerr << "Geschaeft::aendere(eingabe)\n"
        << "Befehl umbekannt: " << config << '\n';
    }
  } // while (!eingabe.ist_leer())
} // void aendere(Eingabe& eingabe)

} // namespace UI_TEXT
