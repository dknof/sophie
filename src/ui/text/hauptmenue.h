#pragma once

#include "kontext.h"

namespace UI_TEXT {

/** Das Hauptmenü
 **/
class Hauptmenue : public Kontext {
  public:
    Hauptmenue(UI_Text& ui);
    ~Hauptmenue();

    // der Name des Kontexts
    string name() const override;
    // Die Hilfe zum Kontext
    string hilfe() const override;

    // Einen Befehl ausführen
    bool befehl(Eingabe& eingabe) override;
}; // class Hauptmenue : public Kontext

} // namespace UI_TEXT
