#pragma once

#include "../../class/readconfig/readconfig.h"
#include "../../zutat.h"
#include "../../einheit.h"
#include "../../geschaeft.h"
#include "../../rezept.h"

namespace UI_TEXT {
class UI_Text;
/** Ein Eingabe ist ein Befehl, gegebenenfalls mit Argumenten
 **/
class Eingabe {
  public:
    // Konstruktor
    Eingabe(UI_Text& ui);
    Eingabe(UI_Text& ui, string zeile);

    // liest die nächste Zeile
    void lese_zeile();
    // die gelesene Zeile
    string zeile() const;
    // Der gesamte Text als Konfigurationseintrag
    Config zeile_config() const;

    // ob der Eingabe leer ist
    bool ist_leer() const;
    // Resttext
    string text() const;
    // ob der Resttext aus einem Wort besteht
    bool ist_ein_wort() const;
    // gibt das erste Wort vom Resttext zurück
    string erstes_wort() const;
    // gibt den Namen vom Resttext zurück
    string name() const;

    // löscht das erste Zeichen
    void loesche_erstes_zeichen();

    // prüfe, ob die Zeile dem Eingabe entspricht
    bool operator==(string text) const;
    bool pruefe_befehl(string befehl);
    bool pruefe_befehl(string befehl, string kurzform);
    // sucht einen Befehl, dabei darf dieser auch abgekürzt eingegeben sein
    string pruefe_befehle(vector<string> befehle);

    // Holt den nächsten Wert aus den Argumenten
    string naechstes_argument();
    string naechster_name();
    Config naechstes_config();
    ::Zutat argument_zutat();
    ::Einheit argument_einheit();
    ::Anzahl argument_anzahl();
    ::Menge argument_menge();
    ::Geschaeft argument_geschaeft();
    ::Rezept argument_rezept();

    // Parsen von mehreren verschiedenen Argumenten (Rekursion)
    template<typename... Args>	
      bool parse(string& text, char const* fixtext, Args&... args);
    template<typename T, typename... Args>	
      bool parse(string& text, T& param, Args&... args);
    bool parse(string& text);
    // Hilfsmethoden
    bool parse_(string& text, ::Zutat& zutat);
    bool parse_(string& text, ::Einheit& einheit);
    bool parse_(string& text, ::Anzahl& anzahl);
    bool parse_(string& text, ::Menge& menge);
    bool parse_(string& text, char const* fixtext);

    // überspringe ein Zeichen
    void ignoriere(char zeichen);

  private:
    // überspringe die folgenden Leerzeichen
    void ignoriere_leerzeichen();
    static void entferne_leerzeichen(string& text);
    // gibt das erste Wort vom Text zurück
    static string erstes_wort(string const& text);

  private:
    // Die UI
    UI_Text* const ui = nullptr; 
    // der Eingabe
    string zeile_; 
    // Position in der Zeile
    size_t pos = 0;
}; // class Eingabe

/** Prüft, ob text mit fixtext beginnt. Wenn ja, wird weiter geparst, ansonesten nicht.
 ** Gibt zurück, ob die angegebenen Argumente gepasst haben.
 ** Wenn alle Argumente passen, werden die Parameter auf den entsprechenden Wert gesetzt.
 **/
template<typename... Args>
bool
Eingabe::parse(string& text, char const* fixtext, Args&... args)
{
  auto text2 = text;
  if (!this->parse_(text2, fixtext)) 
    return false;
  if (!this->parse(text2, args...))
    return false;
  return true;
} // template<typename... Args> bool Eingabe::parse(string& text, char const* fixtext, Args... args)

/** Prüft, ob text mit Typ T beginnt. Wenn ja, wird weiter geparst, ansonesten nicht.
 ** Gibt zurück, ob die angegebenen Argumente gepasst haben.
 ** Wenn alle Argumente passen, werden die Parameter auf den entsprechenden Wert gesetzt.
 **/
template<typename T, typename... Args>
bool Eingabe::parse(string& text, T& param, Args&... args)
{
  auto text2 = text;
  T p;
  if (!this->parse_(text2, p)) 
    return false;
  if (!this->parse(text2, args...))
    return false;
  param = move(p);
  return true;
} // template<typename T, typename... Args> bool Eingabe::parse(string& text, T& param, Args... args)

} // namespace UI_TEXT
