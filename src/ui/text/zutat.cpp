#include "constants.h"
#include "zutat.h"

#include "text.h"
#include "eingabe.h"
#include "../../zutatenkategorie.h"

namespace UI_TEXT {
/** Konstruktor
 **
 ** @param    ui       zugehörige Benutzerschnittstelle
 ** @param    zutat    Zutat
 **/
Zutat::Zutat(UI_Text& ui, ::Zutat zutat) :
  Kontext(ui, zutat.name()),
  zutat(zutat)
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Zutatliste
 **/
string 
Zutat::hilfe() const
{
  return 
    "Zeige:     Zutat anzeigen\n"
    "Lösche:    Zutat löschen\n"
    "Eigenschaften ändern:\n"
    "  Name:Zucker       Name auf Zucker ändern\n"
    "  Name:Ei/Eier      Name auf Ei, Mehrzahl Eier, ändern\n"
    "  Kategorie:Gewürz  Kategorie auf Gewürz setzen\n"
    "  Bild:Pfad         Pfad zum Bild angeben\n"
    ;
} // string Zutat::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Zutat::befehl(Eingabe& eingabe)
{
  { // Listenbefehle
    auto const befehl = eingabe.pruefe_befehle({"Zeige",
                                               "Lösche"});

    if ((befehl == "Zeige") && eingabe.text().empty()) {
      this->zeige(eingabe);
      return true;
    } else if ((befehl == "Lösche") && eingabe.text().empty())  {
      this->loesche(eingabe);
      return true;
    } else if (eingabe.zeile_config().separator) {
      this->aendere(eingabe);
      return true;
    }
  } // Listenbefehle

  return false;
} // bool Zutat::befehl(Eingabe& eingabe)

/** Zeigt die Zutat an
 **/
void
Zutat::zeige(Eingabe& eingabe)
{
  *this->ui->ostr << "Name: " << this->zutat.name();
  if (!this->zutat.name_mehrzahl().empty())
    *this->ui->ostr << '/' << this->zutat.name_mehrzahl();
  *this->ui->ostr << "\n";
  if (this->zutat.kategorie())
    *this->ui->ostr << "Kategorie: " << this->zutat.kategorie() << '\n';
  if (!this->zutat.bild().empty())
    *this->ui->ostr << "Bild: " << this->zutat.bild() << '\n';
} // void zeige(Eingabe& eingabe)

/** Gibt die Zutat in einer Zeile aus
 **/
void
Zutat::schreibe()
{
  auto const name = this->zutat.name();
  auto const mehrzahl = this->zutat.name_mehrzahl();
  auto const bild = this->zutat.bild();
  auto const kategorie = this->zutat.kategorie();

  *this->ui->ostr << name;
  if (   !mehrzahl.empty()
      && (mehrzahl != name))
    *this->ui->ostr << '/' << mehrzahl;

  if (kategorie)
    *this->ui->ostr << " Kategorie:" << kategorie;

  if (!bild.empty())
    *this->ui->ostr << " Bild:" << bild;
} // void Zutat::schreibe()

/** Löscht eine Zutat
 ** Eingabe: Name (optional)
 **
 ** @param    eingabe    Eingabe
 **/
void
Zutat::loesche(Eingabe& eingabe)
{
  this->zutat.loesche();
  this->zutat = ::Zutat();
  this->ui->zu_vorigem_kontext();
} // void loesche(Eingabe& eingabe)

/** Ändert einen Wert
 **
 ** @param    eingabe    Eingabe
 **/
void
Zutat::aendere(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    Config config = eingabe.naechstes_config();
    if (config.name == "Name") {
      string name = config.value;
      // Gegebenenfalls Plural finden
      string name_mehrzahl;
      auto p = name.find('/');
      if (p != string::npos) {
        name_mehrzahl = string(name, p+1, string::npos);
        name.erase(p, string::npos);
      }
      this->zutat.setze_name(name);
      if (!name_mehrzahl.empty())
        this->zutat.setze_name_mehrzahl(name_mehrzahl);
    } else if (config.name == "Mehrzahl") {
      this->zutat.setze_name_mehrzahl(config.value);
    } else if (config.name == "Kategorie") {
      if (!::ZutatenKategorie(config.value))
        *this->ui->ostr << "Lege Kategorie " << config.value << " an.\n";
      this->zutat.setze_kategorie(config.value);
    } else if (config.name == "Bild") {
      this->zutat.setze_bild(config.value);
    } else {
      cerr << "Zutat::aendere(eingabe)\n"
        << "Befehl umbekannt: " << config << '\n';
    }
  } // while (!eingabe.ist_leer())
} // void aendere(Eingabe& eingabe)

} // namespace UI_TEXT
