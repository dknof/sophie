#include "constants.h"
#include "einheitenumrechner.h"
#include "einheiten.h"
#include "zutaten.h"

#include "text.h"
#include "eingabe.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Einheitenumrechner::Einheitenumrechner(UI_Text& ui) :
  Kontext(ui, "Einheitenumrechner")
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Einheitenumrechnerliste
 **/
string 
Einheitenumrechner::hilfe() const
{
  return 
    "Liste:   Formeln auflisten\n"
    "+:       Neue Formel eingeben\n"
    "-:       Eine Formel löschen\n"
    "Beispiele:\n"
    "  + 1000 g = 1 kg\n"
    "  + 1 Esslöffel Zucker = 20 Gramm\n"
    "  - Esslöffel Zucker = Gramm\n"
    "  - Esslöffel Zucker = \n"
    "  - Esslöffel = \n"
    ;
} // string Einheitenumrechner::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Einheitenumrechner::befehl(Eingabe& eingabe)
{
  vector<string> liste = {"Liste", "L", "Zeige",
    "+",
    "-"};
  auto const befehl = eingabe.pruefe_befehle(liste);

  if (befehl.empty()) {
    // eine Formel zum berechnen
    // Beispiele
    // 1 kg =
    // 1 kg Mehl =
    // 1 kg Mehl = ml
    return this->umrechnen(eingabe);
  } else if ((befehl == "Liste") || (befehl == "L")
             || (befehl == "Zeige")) {
    this->liste(eingabe);
    return true;
  } else if (befehl == "+") {
    this->neu(eingabe);
    return true;
  } else if (befehl == "-") {
    this->loesche(eingabe);
    return true;
  } // Befehl für eine Einheit

  return false;
} // bool Einheitenumrechner::befehl(Eingabe& eingabe)

/** Listet alle Einheitenumrechner auf
 **
 ** @param    filter   (optionaler) Filter
 **/
void
Einheitenumrechner::liste(Eingabe& eingabe)
{
  auto const formeln = ::einheitenumrechner.liste();
  for (auto& f : formeln) {
    *this->ui->ostr << f.menge1;
    if (f.zutat)
      *this->ui->ostr << " " << f.zutat.name();
    *this->ui->ostr << " = " << f.menge2 << '\n';
  }
} // void Einheitenumrechner::liste(Eingabe& eingabe)

/** Gibt eine neue Formel ein
 ** Eingabe (Beispiele)
 ** 1 kg = 1000 g
 ** 1 l Mehl = 620 g
 **
 ** @param    eingabe    Eingabe
 **/
void
Einheitenumrechner::neu(Eingabe& eingabe)
{
  string zeile = eingabe.zeile();
  Menge menge1;
  Menge menge2;
  ::Zutat zutat;
  if (eingabe.parse(zeile, "+", menge1, "=", menge2)) {
    ::einheitenumrechner.neu(menge1, menge2);
  } else if (eingabe.parse(zeile, "+", menge1, zutat, "=", menge2)) {
    ::einheitenumrechner.neu(menge1, menge2, zutat);
  } else {
    *this->ui->ostr << "Keine Formel für die Umrechnung gefunden: „" << eingabe.text() << "“\n";
  }
} // void Einheitenumrechner::neu(Eingabe& eingabe)

/** Löscht eine Formel
 ** Eingabe (Beispiele)
 ** kg =
 ** 1 kg =
 ** kg = g
 ** 1 kg = g
 ** l Mehl =
 ** 1 l Mehl =
 ** l Mehl = g
 ** 1 l Mehl = g
 **
 ** @param    eingabe    Eingabe
 **/
void
Einheitenumrechner::loesche(Eingabe& eingabe)
{
  string zeile = eingabe.zeile();
  Menge menge1;
  Menge menge2;
  ::Zutat zutat;
  ::Einheit einheit1;
  ::Einheit einheit2;
  if (eingabe.parse(zeile, "-", einheit1, "=")) {
    ::einheitenumrechner.loesche(einheit1);
  } else if (eingabe.parse(zeile, "-", menge1, "=")) {
    ::einheitenumrechner.loesche(menge1.einheit);
  } else if (eingabe.parse(zeile, "-", einheit1, "=", einheit2)) {
    ::einheitenumrechner.loesche(einheit1, einheit2);
  } else if (eingabe.parse(zeile, "-", menge1, "=", einheit2)) {
    ::einheitenumrechner.loesche(menge1.einheit, einheit2);
  } else if (eingabe.parse(zeile, "-", einheit1, zutat, "=")) {
    ::einheitenumrechner.loesche(einheit1, zutat);
  } else if (eingabe.parse(zeile, "-", menge1, zutat, "=")) {
    ::einheitenumrechner.loesche(menge1.einheit, zutat);
  } else if (eingabe.parse(zeile, "-", einheit1, zutat, "=", einheit2)) {
    ::einheitenumrechner.loesche(einheit1, einheit2, zutat);
  } else if (eingabe.parse(zeile, "-", menge1, zutat, "=", einheit2)) {
    ::einheitenumrechner.loesche(menge1.einheit, einheit2, zutat);
  } else {
    *this->ui->ostr << "Keine Formel für die Umrechnung gefunden: „" << eingabe.text() << "“\n";
  }
} // void Einheitenumrechner::loesche(Eingabe& eingabe)

/** Rechnet eine Formel um
 ** Beispiele
 ** 1 l =
 ** 2 EL Zucker =
 ** 100 Gramm = kg
 ** 100 ml Zucker = l
 **
 ** @param    eingabe    Eingabe
 **
 ** @return   ob eine Formel erkannt wurde
 **/
bool
Einheitenumrechner::umrechnen(Eingabe& eingabe)
{
  string zeile = eingabe.zeile();
  Menge menge1;
  ::Zutat zutat;
  ::Einheit einheit2;
  if (eingabe.parse(zeile, menge1, "=")) {
    for (auto& e : ::Einheit::liste()) {
      if (e == menge1.einheit)
        continue;
      auto const menge2 = ::einheitenumrechner.umrechnen(menge1, e);
      if (!menge2)
        continue;
      *this->ui->ostr << menge1 << " = " << menge2 << '\n';
    } // for (auto e : ::Einheit::liste())

  } else if (eingabe.parse(zeile, menge1, zutat, "=")) {
    for (auto& e : ::Einheit::liste()) {
      if (e == menge1.einheit)
        continue;
      auto const menge2 = ::einheitenumrechner.umrechnen(menge1, e, zutat);
      if (!menge2)
        continue;
      *this->ui->ostr << menge1 << " " << zutat.name() << " = " << menge2 << '\n';
    } // for (auto e : ::Einheit::liste())

  } else if (eingabe.parse(zeile, menge1, "=", einheit2)) {
    auto const menge2 = ::einheitenumrechner.umrechnen(menge1, einheit2);
    if (menge2) {
      *this->ui->ostr << menge1 << " = " << menge2 << '\n';
    } else {
      *this->ui->ostr << "Keine Umrechnung gefunden\n";
    }

  } else if (eingabe.parse(zeile, menge1, zutat, "=", einheit2)) {
    auto const menge2 = ::einheitenumrechner.umrechnen(menge1, einheit2, zutat);
    if (menge2) {
      *this->ui->ostr << menge1 << " " << zutat.name() << " = " << menge2 << '\n';
    } else {
      *this->ui->ostr << "Keine Umrechnung gefunden\n";
    }

  } else {
    return false;
  }

  return true;
} // void Einheitenumrechner::umrechnen(Eingabe& eingabe)

} // namespace UI_TEXT
