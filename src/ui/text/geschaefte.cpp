#include "constants.h"
#include "geschaefte.h"
#include "geschaeft.h"

#include "text.h"
#include "eingabe.h"

#include "../../geschaeft.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Geschaefte::Geschaefte(UI_Text& ui) :
  Kontext(ui, "Geschäfte")
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Geschäfteliste
 **/
string 
Geschaefte::hilfe() const
{
  return 
    "Liste:  Geschäfte auflisten\n"
    "+:      Neues Geschäft eingeben\n"
    "-:      Geschäft löschen\n"
    "In ein Geschäft kann über den Namen gewechselt werden\n"
    ;
} // string Geschaefte::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Geschaefte::befehl(Eingabe& eingabe)
{
  auto liste = ::Geschaeft::liste_namen();
  liste.push_back("Liste");
  liste.push_back("L");
  liste.push_back("Zeige");
  liste.push_back("+");
  liste.push_back("-");
  liste.push_back("=");
  auto const befehl = eingabe.pruefe_befehle(liste);

  if (befehl.empty()) {
    return false;
  } else if ((befehl == "Liste") || (befehl == "L")
             || (befehl == "Zeige")) {
    this->liste(eingabe);
    return true;
  } else if (befehl == "+") {
    this->neu(eingabe);
    return true;
  } else if (befehl == "-") {
    this->loesche(eingabe);
    return true;
  } else if (befehl == "=") {
    this->aendere(eingabe);
    return true;
  } else { // Befehl für ein Geschäft
    if (eingabe.text().empty()) {
      // In ein Geschäft wechseln
      auto geschaeft = ::Geschaeft(befehl);
      if (geschaeft)
      this->ui->zu_kontext(make_unique<Geschaeft>(*this->ui, geschaeft));
      return true;
    } else {
      // Befehl an die Geschäft weiterreichen
      Eingabe eingabe2(*this->ui, eingabe.text());
      auto geschaeft = ::Geschaeft(befehl);
      if (geschaeft)
        return Geschaeft(*this->ui, geschaeft).befehl(eingabe2);
    } // Befehl für ein Geschäft
  }

  return false;
} // bool Geschaefte::befehl(Eingabe& eingabe)

/** Listet alle Geschäfte auf
 **
 ** @param    filter   (optionaler) Filter
 **/
void
Geschaefte::liste(Eingabe& eingabe)
{
  for (auto const& i : ::Geschaeft::liste())
    *this->ui->ostr << i << '\n';
} // void liste(Eingabe& eingabe)

/** Gibt ein neues Geschäft ein
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **/
void
Geschaefte::neu(Eingabe& eingabe)
{
  // Namen abfragen
  auto const name = eingabe.naechster_name();
  if (name.empty())
    return ;

  auto geschaeft = ::Geschaeft::neu(name);
  if (geschaeft)
    this->geschaeft(name).aendere(eingabe);
} // void neu(Eingabe& eingabe)

/** Löscht ein Geschäft
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **/
void
Geschaefte::loesche(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    auto g = eingabe.argument_geschaeft();
    if (!g)
      break;
    g.loesche();
  }
} // void Geschaefte::loesche(Eingabe& eingabe)

/** Ändert ein Geschäft
 ** Eingabe: Name, zu ändernde Parameter
 **
 ** @param    eingabe    Eingabe
 **
 ** @todo   alles
 **/
void
Geschaefte::aendere(Eingabe& eingabe)
{
  do { // while (!eingabe.ist_leer())
    // Namen abfragen
    auto name = eingabe.text();
    //this->db_zugriff->loesche(name);
  } while (!eingabe.ist_leer()); 
} // void aendere(Eingabe& eingabe)

/** Kontext eines einzelnen Geschäfts
 **
 ** @param    name   Name der Geschäft
 **
 ** @return   Kontext zur angegeben Zusage
 **/
Geschaeft 
Geschaefte::geschaeft(string const& name)
{
  return Geschaeft(*this->ui, ::Geschaeft(name));
} // Geschaeft Geschaefte::geschaeft(string name)

} // namespace UI_TEXT
