#pragma once

#include "kontext.h"

#include "../../vorrat.h"

namespace UI_TEXT {

/** Die Vorrat
 **/
class Vorrat : public Kontext {
  public:
    Vorrat(UI_Text& ui);

    // Die Hilfe zum Kontext
    string hilfe() const;

    // Einen Befehl ausführen
    bool befehl(Eingabe& eingabe);

  private:
    // Alle Einträge auflisten
    void liste(Eingabe& eingabe);
    // Einen neuen Eintrag hinzufügen
    void hinzufuege(Eingabe& eingabe);
    // Eine Einheit entfernen
    void entferne(Eingabe& eingabe);

    // Zugriff auf die Datenbank
    ::Vorrat& vorrat;
}; // class Vorrat : public Kontext

} // namespace UI_TEXT
