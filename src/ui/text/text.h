#pragma once

#include "../ui.h"

#include "kontext.h"

namespace UI_TEXT {
class Eingabe;

/** Eine Text-Benutzerschnittstelle
 **/
class UI_Text : public UI {
  public:
    // Konstruktor
    UI_Text();
    // Konstruktor
    UI_Text(istream& istr, ostream& ostr);
    // Destruktor
    ~UI_Text();

    // die Hauptroutine
    void starte();


    // Fehlermeldung
    void fehler(string text);

    // Wechsel im Kontext

    // der aktuelle Kontext
    Kontext& kontext();
    Kontext const& kontext() const;
    // wechselt zum Kontext
    void zu_kontext(unique_ptr<Kontext> kontext);
    // wechselt zum vorigen Kontext
    void zu_vorigem_kontext();

    // lese einen Wert
    string lese_zeile();
    // gibt den Prompt zurück
    string prompt() const;

  private:
    // die Lizenz
    string lizenz() const;

    // den Befehl ausführen
    // allgemeiner Befehl
    bool befehl_allgemein(Eingabe& eingabe);
    // Kontextbewegung
    bool befehl_kontext(Eingabe& eingabe);
    // direkter Sprung
    bool befehl_sprung(Eingabe& eingabe);

  public: // Variablen
    // der Eingabestrom
    istream* istr = nullptr;
    // der Ausgabestrom
    ostream* ostr = nullptr;

  private: // Variablen
    // Die Kontext (inklusive Historie)
    vector<unique_ptr<Kontext>> kontext_;
    // Der Kontext "Hauptmenü"
    unique_ptr<Kontext> hauptmenue_ = nullptr;
}; // class UI_Text

} // namespace UI_TEXT

using UI_TEXT::UI_Text;
