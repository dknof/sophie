#pragma once

#include "kontext.h"
#include "../../einheit.h"

namespace UI_TEXT {

/** Eine Einheit
 **/
class Einheit : public Kontext {
  friend class Einheiten;
  public:
    Einheit(UI_Text& ui, ::Einheit einheit);

    // Die Hilfe zum Kontext
    string hilfe() const;

    // Einen Befehl ausführen
    bool befehl(Eingabe& eingabe);

  private:
    // Anzeigen
    void zeige(Eingabe& eingabe);
    // in einer Zeile ausgeben
    void schreibe();
    // Diese Einheit löschen
    void loesche(Eingabe& eingabe);
    // Eigenschaft ändern
    void aendere(Eingabe& eingabe);

    // Einheit-Objekt
    ::Einheit einheit;
}; // class Einheit : public Kontext

} // namespace UI_TEXT
