#pragma once

#include "kontext.h"

namespace UI_TEXT {
  /** Die Einstellungenliste
   **
   ** @todo   Um einen Filter erweitern, um nur bestimmte Einstellungen aufzulisten
   **/
  class Einstellungen : public Kontext {
    public:
      Einstellungen(UI_Text& ui);

      string hilfe() const;

      // Einen Befehl ausführen
      bool befehl(Eingabe& eingabe);

      // die Einstellungen ausgeben
      void liste(Eingabe& eingabe) const;
  }; // class Einstellungen : public Kontext

} // namespace UI_TEXT
