#pragma once

#include "kontext.h"
#include "../../geschaeft.h"

namespace UI_TEXT {
class Geschaeft;

/** Die Geschäfteliste
 **/
class Geschaefte : public Kontext {
  public:
    Geschaefte(UI_Text& ui);

    string hilfe() const override;

    bool befehl(Eingabe& eingabe) override;

  private:
    void liste(Eingabe& eingabe);
    void neu(Eingabe& eingabe);
    void loesche(Eingabe& eingabe);
    void aendere(Eingabe& eingabe);

    Geschaeft geschaeft(string const& name);
}; // class Geschaefte : public Kontext

} // namespace UI_TEXT
