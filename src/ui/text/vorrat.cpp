#include "constants.h"
#include "vorrat.h"

#include "text.h"
#include "eingabe.h"

#include "../../einheit.h"
#include "../../zutat.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Vorrat::Vorrat(UI_Text& ui) :
  Kontext(ui, "Vorrat"),
  vorrat(::vorrat)
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Vorratliste
 **/
string 
Vorrat::hilfe() const
{
  return 
    "Liste:     Vorrat auflisten\n"
    "+:         Neuen Eintrag eingeben\n"
    "-:         Eine Zutat entfernen\n"
    "- *:       Alles entfernen\n"
    "Beispiele:\n"
    "  + 1 kg Zucker\n"
    "  + Salz\n"
    "  - 500 g Zucker\n"
    "  - Zucker\n"
    "  - *\n"
    ;
} // string Vorrat::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Vorrat::befehl(Eingabe& eingabe)
{
  auto const befehl
    = eingabe.pruefe_befehle({"Liste", "L", "Zeige",
                             "hinzufüge", "+",
                             "entferne", "-",
                             });

  if ((befehl == "Liste") || (befehl == "L")
      || (befehl == "Zeige")) {
    this->liste(eingabe);
    return true;
  } else if ((befehl == "hinzufüge") || (befehl == "+")) {
    this->hinzufuege(eingabe);
    return true;
  } else if ((befehl == "entferne") || (befehl == "-")) {
    this->entferne(eingabe);
    return true;
  } // Befehl für eine Einheit

  return false;
} // bool Vorrat::befehl(Eingabe& eingabe)

/** Listet alle Einträge auf
 **
 ** @param    eingabe   (optionaler) Filter
 **/
void
Vorrat::liste(Eingabe& eingabe)
{
  *this->ui->ostr << this->vorrat.liste();
  return ;
} // void Vorrat::liste(Eingabe& eingabe)

/** Gibt einen neuen Eintrag ein
 ** Eingabe (Beispiele)
 **   1 kg Zucker
 **   Salz
 **
 ** @param    eingabe    Eingabe
 **/
void
Vorrat::hinzufuege(Eingabe& eingabe)
{
  if (eingabe.ist_leer())
    return ;

  auto text = eingabe.text();
  Menge menge;
  ::Zutat zutat;
  if (eingabe.parse(text, zutat)) {
    this->vorrat.hinzufuege(zutat);
  } else if (eingabe.parse(text, menge, zutat)) {
    this->vorrat.hinzufuege(zutat, menge);
  } else {
    *this->ui->ostr << "Konnte „" << text << "“ nicht erkennen. Es muss eine Zutat oder Menge und Zutat sein.\n";
  }

  return ;
} // void Vorrat::hinzufuege(Eingabe& eingabe)

/** Entfernt die angegebene Zutat / die Menge der Zutat
 **
 ** @param    eingabe    Eingabe (Zutat mit Menge)
 **/
void
Vorrat::entferne(Eingabe& eingabe)
{
  if (eingabe.ist_leer())
    return ;

  auto text = eingabe.text();
  Menge menge;
  ::Zutat zutat;

  if (text == "*") {
    this->vorrat.entferne_alles();
  } else if (eingabe.parse(text, zutat)) {
    if (!this->vorrat.entferne(zutat))
      *this->ui->ostr << "Zutat '" << zutat.name() << "' ist nicht im Vorrat und konnte daher nicht entfernt werden.\n";
  } else if (eingabe.parse(text, menge, zutat)) {
    if (!this->vorrat.entferne(zutat, menge))
      *this->ui->ostr << "Zutat '" << zutat.name() << "' ist nicht im Vorrat oder konnte nicht die Einheit „" << menge.einheit << "“ umrechnen.\n";
  } else {
    *this->ui->ostr << "Konnte „" << text << "“ nicht erkennen. Es muss eine Zutat oder Menge und Zutat sein.\n";
  }
} // void Vorrat::entferne(Eingabe& eingabe)

} // namespace UI_TEXT
