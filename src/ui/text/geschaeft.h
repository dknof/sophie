#pragma once

#include "kontext.h"
#include "../../geschaeft.h"

namespace UI_TEXT {
class Geschaefte;

/** Eine Geschäft
 **/
class Geschaeft : public Kontext {
  friend class Geschaefte;
  public:
  Geschaeft(UI_Text& ui, ::Geschaeft geschaeft);

  string hilfe() const override;

  bool befehl(Eingabe& eingabe) override;

  private:
  void zeige(Eingabe& eingabe);
  void zeige_zutaten(Eingabe& eingabe);
  void schreibe();
  void loesche(Eingabe& eingabe);
  void aendere(Eingabe& eingabe);

  void hinzufuege(Eingabe& eingabe);
  void entferne(Eingabe& eingabe);

  ::Geschaeft geschaeft;
}; // class Geschaeft : public Kontext

} // namespace UI_TEXT
