#include "constants.h"
#include "hauptmenue.h"

#include "text.h"
#include "eingabe.h"

#include "rezepte.h"
#include "einkaufsliste.h"
#include "vorrat.h"
#include "zutaten.h"
#include "einheiten.h"
#include "einheitenumrechner.h"
#include "geschaefte.h"

namespace UI_TEXT {
/** Konstruktor
 **
 ** @param    ui   zugehörige Benutzerschnittstelle
 **/
Hauptmenue::Hauptmenue(UI_Text& ui) :
  Kontext(ui, "Hauptmenü")
{ }

/** Destruktor
 **/
Hauptmenue::~Hauptmenue()
{ }

/** -> Rückgabe
 **
 ** @result   "Hauptmenü"
 **/
string 
Hauptmenue::name() const
{
  return "Hauptmenü";
} // string Hauptmenue::name() const

/** -> Rückgabe
 **
 ** @result   Die Hilfe zum Hauptmenü
 **/
string 
Hauptmenue::hilfe() const
{
  return 
    "Über die Eingabe der folgenden Unterkategorieen kann in diese gewechselt werden.\n"
    "* Rezepte\n"
    "* Zutaten\n"
    "* Zutatenkategorieen\n"
    "* Einheiten\n"
    "* Einheitenumrechner\n"
    "* Einkaufsliste\n"
    "* Kategorieen\n"
    "* Eigenschaften\n"
    "* Menüs\n"
    "* Geschäfte\n"
    "* Vorrat\n"
    ;
} // string Hauptmenue::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   Eingabe
 **
 ** @result   ob der Befehl interpretiert wurde
 **/
bool 
Hauptmenue::befehl(Eingabe& eingabe)
{
  auto const befehl = eingabe.pruefe_befehle({"Rezepte",
                                             "Einkaufsliste", "EL",
                                             "Vorrat",
                                             "Zutaten", "Z",
                                             "Zutatenkategorieen", "ZK",
                                             "Einheiten", "E",
                                             "Einheitenumrechner", "EU",
                                             "Kategorieen",
                                             "Eigenschaften",
                                             "Menüs",
                                             "Geschäfte",
                                             });
  // In einen speziellen Kontext wechseln
  if (befehl == "Rezepte") {
    this->ui->zu_kontext(make_unique<Rezepte>(*this->ui));
    return true;
  } else if (   (befehl == "Einkaufsliste")
             || (befehl == "EL")) {
    this->ui->zu_kontext(make_unique<Einkaufsliste>(*this->ui));
    return true;
  } else if (befehl == "Vorrat") {
    this->ui->zu_kontext(make_unique<Vorrat>(*this->ui));
    return true;
  } else if (   (befehl == "Zutaten")
             || (befehl == "Z") ) {
    this->ui->zu_kontext(make_unique<Zutaten>(*this->ui));
    return true;
  } else if (   (befehl == "Zutatenkategorieen")
             || (befehl == "ZK") ) {
    //this->zu_kontext(Kontext::ZUTATEN);
    return true;
  } else if (befehl == "Einheiten") {
    this->ui->zu_kontext(make_unique<Einheiten>(*this->ui));
    return true;
  } else if (   (befehl == "Einheitenumrechner")
             || (befehl == "EU")) {
    this->ui->zu_kontext(make_unique<Einheitenumrechner>(*this->ui));
    return true;
  } else if (befehl == "Kategorieen") {
    //this->zu_kontext(Kontext::ZUTATEN);
    return true;
  } else if (befehl == "Eigenschaften") {
    //this->ui->zu_kontext(Kontext::EIGENSCHAFTEN);
    return true;
  } else if (befehl == "Menüs") {
    //this->ui->zu_kontext(Kontext::MENUES);
    return true;
  } else if (befehl == "Geschäfte") {
    this->ui->zu_kontext(make_unique<Geschaefte>(*this->ui));
    return true;
  }

  return false;
} // bool Hauptmenue::befehl(Eingabe& eingabe)

} // namespace UI_TEXT
