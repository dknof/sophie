#include "constants.h"
#include "kontext.h"
#include "text.h"

namespace UI_TEXT {
/** Konstruktor
 **
 ** @param    ui   Benutzerschnittstelle
 **/
Kontext::Kontext(UI_Text& ui, string const name) :
  ui(&ui),
  name_(name)
{ }

/** -> Rückgabe
 **
 ** @result   Name des Kontexts
 **/
string 
Kontext::name() const
{
  return this->name_;
} // string Kontext::name() const

} // namespace UI_TEXT
