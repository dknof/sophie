#include "constants.h"
#include "einstellungen.h"

#include "text.h"
#include "eingabe.h"

#include "../../einstellung.h"

namespace UI_TEXT {
/** Konstruktor
 **
 ** @param    ui            zugehörige Benutzerschnittstelle
 **/
Einstellungen::Einstellungen(UI_Text& ui) :
  Kontext(ui, "Einstellungen")
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Einstellungenliste
 **/
string 
Einstellungen::hilfe() const
{
  return 
    "Liste:     Einstellungen auflisten\n"
    "Neu:       Neue Einheit eingeben\n"
    "Editieren: Eine Einheit ändern\n"
    "Löschen:   Eine Einheit löschen\n"
    //"In eine Einheit kann über den Namen gewechselt werden\n"
    ;
} // string Einstellungen::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Einstellungen::befehl(Eingabe& eingabe)
{
  vector<string> schluessel;
  schluessel.reserve(Einstellung::STRING_LETZTER - Einstellung::BOOL_ERSTER + 2);
  for (int t = Einstellung::BOOL_ERSTER;
       t <= Einstellung::BOOL_LETZTER;
       ++t)
    schluessel.push_back(::einstellung.name(static_cast<Einstellung::TypBool>(t)));

  for (int t = Einstellung::STRING_ERSTER;
       t <= Einstellung::STRING_LETZTER;
       ++t)
    schluessel.push_back(::einstellung.name(static_cast<Einstellung::TypString>(t)));

  schluessel.push_back("Liste");
  schluessel.push_back("L");

  { // Listenbefehle
    auto const befehl = eingabe.pruefe_befehle(schluessel);

    if ((befehl == "Liste") || (befehl == "L")) {
      this->liste(eingabe);
      return true;
    } else {
      for (int t = Einstellung::BOOL_ERSTER;
           t <= Einstellung::BOOL_LETZTER;
           ++t) {
        if (befehl == ::einstellung.name(static_cast<Einstellung::TypBool>(t))) {
          ::einstellung.setze(befehl, eingabe.zeile_config().value);
          return true;
        }
      }
      for (int t = Einstellung::STRING_ERSTER;
           t <= Einstellung::STRING_LETZTER;
           ++t) {
        if (befehl == ::einstellung.name(static_cast<Einstellung::TypString>(t))) {
          ::einstellung.setze(befehl, eingabe.zeile_config().value);
          return true;
        }
      }
    }
  } // Listenbefehle

  return false;
} // bool Einstellungen::befehl(Eingabe& eingabe)

/** Listet alle Einstellungen auf
 **
 ** @param    filter   (optionaler) Filter
 **/
void
Einstellungen::liste(Eingabe& eingabe) const
{
  for (int t = Einstellung::BOOL_ERSTER;
       t <= Einstellung::BOOL_LETZTER;
       ++t)
    *this->ui->ostr << ::einstellung.name(static_cast<Einstellung::TypBool>(t)) << " = " << (::einstellung(static_cast<Einstellung::TypBool>(t)) ? "ja" : "nein") << '\n';

  for (int t = Einstellung::STRING_ERSTER;
       t <= Einstellung::STRING_LETZTER;
       ++t)
    *this->ui->ostr << ::einstellung.name(static_cast<Einstellung::TypString>(t)) << " = " << ::einstellung(static_cast<Einstellung::TypString>(t)) << '\n';
} // void Einstellungen::liste(Eingabe& eingabe) const

} // namespace UI_TEXT
