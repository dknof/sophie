#pragma once

namespace UI_TEXT {
class UI_Text;
class Eingabe;
/** Basisklasse für die speziellen Kontexte, in denen sich der Anwender in der UI befinden kann.
 ** Kontexte
 ** - Einstellungen
 ** - Hauptmenü
 ** - Rezepte
 ** -   Rezept
 ** -     Kategorie
 ** - Zutaten
 ** -   Zutat
 ** - Zutatkategorieen
 ** -   Zutatkategorie
 ** - Einheiten
 ** -   Einheit
 ** - Einheitenumrechner
 ** - Eigenschaften
 ** -   Eigenschaft
 ** - Menüs
 ** -   Menü
 ** - Geschäfte
 ** -   Geschäft
 ** - Vorrat
 **/
class Kontext {
  public:
    // Konstruktor
    Kontext(UI_Text& ui, string name);
    // Destruktor
    virtual ~Kontext() = default;

    // der Name des Kontexts
    virtual string name() const;
    // Die Hilfe zum Kontext
    virtual string hilfe() const = 0;

    // Einen Befehl ausführen
    virtual bool befehl(Eingabe& eingabe) = 0;

  protected:
    // Die UI
    UI_Text* const ui; 
    // Der Name des Kontexts
    string const name_;
}; // class Kontext
} // namespace UI_TEXT
