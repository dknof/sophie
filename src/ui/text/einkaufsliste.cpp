#include "constants.h"
#include "einkaufsliste.h"
#include "einheiten.h"
#include "zutaten.h"

#include "text.h"
#include "eingabe.h"

#include "../../einheit.h"
#include "../../zutat.h"

namespace UI_TEXT {
  /** Konstruktor
   **/
  Einkaufsliste::Einkaufsliste(UI_Text& ui) :
    Kontext(ui, "Einkaufsliste"),
    einkaufsliste(::einkaufsliste)
  { }

  /** -> Rückgabe
   **
   ** @result   Die Hilfe zur Einkaufslisteliste
   **/
  string 
    Einkaufsliste::hilfe() const
    {
      return 
        "Liste:     Einkaufsliste auflisten\n"
        "+:         Neuen Eintrag eingeben\n"
        "-:         Eine Zutat entfernen\n"
        "- *:       Alles entfernen\n"
        "Beispiele:\n"
        "  + 1 kg Zucker\n"
        "  + Salz\n"
        "  - 500 g Zucker\n"
        "  - Zucker\n"
        "  - *\n"
        ;
    } // string Einkaufsliste::hilfe() const

  /** Führt einen Befehl aus
   **
   ** @param    eingabe   die Eingabe (Befehl + Argumente)
   **
   ** @result   ob ein Befehl interpretiert wurde
   **/
  bool 
    Einkaufsliste::befehl(Eingabe& eingabe)
    {
      vector<string> liste = {"Liste", "L", "Zeige",
        "hinzufüge", "+",
        "entferne", "-",
      };
      auto const befehl = eingabe.pruefe_befehle(liste);

      if ((befehl == "Liste") || (befehl == "L")
          || (befehl == "Zeige")) {
        this->liste(eingabe);
        return true;
      } else if ((befehl == "hinzufüge") || (befehl == "+")) {
        this->hinzufuege(eingabe);
        return true;
      } else if ((befehl == "entferne") || (befehl == "-")) {
        this->entferne(eingabe);
        return true;
      } // Befehl für eine Einheit

      return false;
    } // bool Einkaufsliste::befehl(Eingabe& eingabe)

  /** Listet alle Einträge auf
   **
   ** @param    eingabe   (optionaler) Filter
   **/
  void
    Einkaufsliste::liste(Eingabe& eingabe)
    {
      {
        auto const einkauf = this->einkaufsliste.liste_nicht_im_vorrat();
        if (einkauf.empty())
          *this->ui->ostr << "Einkauf: --\n";
        else
          *this->ui->ostr << "Einkauf:\n" << einkauf;
      }

      {
        auto const vorrat = this->einkaufsliste.liste_im_vorrat();
        if (vorrat.empty())
          *this->ui->ostr << "Vorrat: --\n";
        else
          *this->ui->ostr << "Vorrat:\n" << vorrat;
      }
      return ;
    } // void Einkaufsliste::liste(Eingabe& eingabe)

  /** Gibt einen neuen Eintrag ein
   ** Eingabe (Beispiele)
   ** 1 kg Zucker
   ** Salz
   **
   ** @param    eingabe    Eingabe
   **/
  void
    Einkaufsliste::hinzufuege(Eingabe& eingabe)
    {

      if (eingabe.ist_leer())
        return ;

      if (isdigit(eingabe.text()[0])) {
        // Zutat mit Menge hinzufügen
        auto const menge = eingabe.argument_menge();
        if (!menge) {
          *this->ui->ostr << "Menge und Zutat „" << eingabe.text() << "“ nicht bekannt\n";
          return ;
        }
        auto const zutat = eingabe.argument_zutat();
        if (!zutat) {
          *this->ui->ostr << "Zutat „" << eingabe.text() << "“ nicht gefunden\n.";
          return ;
        }
        ::einkaufsliste.hinzufuege(zutat, menge);

      } else {
        // Die Zutat ohne Mengenangabe hinzufügen
        auto const zutat = eingabe.argument_zutat();
        if (!zutat) {
          *this->ui->ostr << "Zutat „" << eingabe.text() << "“ nicht gefunden\n.";
          return ;
        }
        this->einkaufsliste.hinzufuege(zutat);
      }

      return ;
    } // void Einkaufsliste::hinzufuege(Eingabe& eingabe)

  /** Entfernt die angegebene Zutat / die Menge der Zutat
   **
   ** @param    eingabe    Eingabe (Zutat mit Menge)
   **/
  void
    Einkaufsliste::entferne(Eingabe& eingabe)
    {
      if (eingabe.ist_leer())
        return ;

      if (eingabe.text() == "*") {
        // Alles entfernen
        this->einkaufsliste.entferne_alles();
        return ;
      }

      if (isdigit(eingabe.text()[0])) {
        // Menge von der Zutat entfernen
        auto const menge = eingabe.argument_menge();
        if (!menge) {
          *this->ui->ostr << "Menge und Zutat „" << eingabe.text() << "“ nicht bekannt\n";
          return ;
        }
        auto const zutat = eingabe.argument_zutat();
        if (!zutat) {
          *this->ui->ostr << "Zutat „" << eingabe.text() << "“ nicht gefunden\n.";
          return ;
        }
        if (!this->einkaufsliste.entferne(zutat, menge)) {
          *this->ui->ostr << "Zutat '" << zutat << "' ist nicht in der Einkaufsliste oder konnte nicht die Einheit „" << menge.einheit << "“ umrechnen.\n";
          return ;
        }
      } else {
        // Alle Einträge zu der Zutat entfernen
        auto const zutat = eingabe.argument_zutat();
        if (!zutat) {
          *this->ui->ostr << "Zutat „" << eingabe.text() << "“ nicht gefunden\n.";
          return ;
        }
        if (!this->einkaufsliste.entferne(zutat)) {
          *this->ui->ostr << "Zutat '" << zutat << "' ist nicht in der Einkaufsliste und konnte daher nicht entfernt werden.\n";
        }
      }
      return ;
    } // void Einkaufsliste::entferne(Eingabe& eingabe)

} // namespace UI_TEXT
