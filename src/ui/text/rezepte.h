#pragma once

#include "kontext.h"

namespace UI_TEXT {
class Rezept;

/** Die Rezeptliste
 **/
class Rezepte : public Kontext {
  public:
    Rezepte(UI_Text& ui);

    // Die Hilfe zum Kontext
    string hilfe() const override;

    // Einen Befehl ausführen
    bool befehl(Eingabe& eingabe) override;

  private:
    // Alle Rezepte auflisten
    void liste(Eingabe& eingabe);
    // Ein neues Rezept eingeben
    void neu(Eingabe& eingabe);
    // Ein Rezept löschen
    void loesche(Eingabe& eingabe);
    // Ein Rezept ändern
    void aendere(Eingabe& eingabe);

    // Zugriff auf ein Rezept
    Rezept rezept(string const& name);
}; // class Rezepte : public Kontext

} // namespace UI_TEXT
