#pragma once

#include "kontext.h"
#include "einheit.h"

namespace UI_TEXT {

  /** Die Einheitenliste
   **
   ** @todo   Um einen Filter erweitern, um nur bestimmte Einheiten aufzulisten
   **/
  class Einheiten : public Kontext {
    friend class Einheitenumrechner;
    public:
      explicit Einheiten(UI_Text& ui);
      // Destruktor
      ~Einheiten();

      // Die Hilfe zum Kontext
      string hilfe() const;

      // Einen Befehl ausführen
      bool befehl(Eingabe& eingabe);

      // der Zugriff zu einer Einheit
      Einheit einheit(string const& text);

    private:
      // Alle Einheiten auflisten
      void liste(Eingabe& eingabe);
      // Eine neue Einheit eingeben
      void neu(Eingabe& eingabe);
      // Eine Einheit löschen
      void loesche(Eingabe& eingabe);
      // Eine Einheit ändern
      void aendere(Eingabe& eingabe);
  }; // class Einheiten : public Kontext

} // namespace UI_TEXT
