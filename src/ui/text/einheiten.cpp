#include "constants.h"
#include "einheiten.h"
#include "einheit.h"

#include "text.h"
#include "eingabe.h"

#include "../../einheit.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Einheiten::Einheiten(UI_Text& ui) :
  Kontext(ui, "Einheiten")
{ }

/** Destruktor
 **/
Einheiten::~Einheiten()
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Einheitenliste
 **/
string 
Einheiten::hilfe() const
{
  return 
    "Liste: Einheiten auflisten\n"
    "+:     Neue Einheit eingeben\n"
    "-:     Eine Einheit löschen\n"
    "=:     Eine Einheit ändern\n"
    "In eine Einheit kann über den Namen gewechselt werden\n"
    ;
} // string Einheiten::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Einheiten::befehl(Eingabe& eingabe)
{
  auto liste = ::Einheit::liste_namen();
  liste.push_back("Liste");
  liste.push_back("L");
  liste.push_back("Zeige");
  liste.push_back("+");
  liste.push_back("-");
  liste.push_back("=");
  auto const befehl = eingabe.pruefe_befehle(liste);

  if (befehl.empty()) {
    return false;
  } else if ((befehl == "Liste") || (befehl == "L")
             || (befehl == "Zeige")) {
    this->liste(eingabe);
    return true;
  } else if (befehl == "+") {
    this->neu(eingabe);
    return true;
  } else if (befehl == "-") {
    this->loesche(eingabe);
    return true;
  } else if (befehl == "=") {
    this->aendere(eingabe);
    return true;
  } else // Befehl für eine Einheit
    if (eingabe.text().empty()) {
      // In eine Einheit wechseln
      auto einheit = ::Einheit(befehl);
      if (einheit)
        this->ui->zu_kontext(make_unique<Einheit>(*this->ui, einheit));
      return true;
    } else {
      // Befehl an die Einheit weiterreichen
      Eingabe eingabe2(*this->ui, eingabe.text());
      auto einheit = ::Einheit(befehl);
      if (einheit)
        return Einheit(*this->ui, einheit).befehl(eingabe2);
    } // Befehl für eine Einheit

  return false;
} // bool Einheiten::befehl(Eingabe& eingabe)

/** -> Rückgabe
 ** 
 ** @param    text   Name der Einheit
 **
 ** @return   Name der mit 'text' bezeichneten Einheit
 **/
Einheit
Einheiten::einheit(string const& text)
{
  return Einheit(*this->ui, ::Einheit(text));
} // Einheit Einheiten::einheit(string const& text)

/** Listet alle Einheiten auf
 **
 ** @param    filter   (optionaler) Filter
 **/
void
Einheiten::liste(Eingabe& eingabe)
{
  for (auto const& i : ::Einheit::liste())
    *this->ui->ostr << i << '\n';
} // void Einheiten::liste(Eingabe& eingabe)

/** Gibt eine neue Einheit ein
 ** Eingabe: Name,  Abkürzung
 **
 ** @param    eingabe    Eingabe
 **/
void
Einheiten::neu(Eingabe& eingabe)
{
  // Namen abfragen
  auto name = eingabe.naechstes_argument();

  // Gegebenenfalls Plural finden
  string name_plural;
  auto const p = name.find('/');
  if (p != string::npos) {
    name_plural = string(name, p+1, string::npos);
    name.erase(p, string::npos);
  }
  ::Einheit::neu(name, name_plural);

  auto e = ::Einheit(name);
  auto einheit = Einheit(*this->ui, e);

  einheit.aendere(eingabe);
} // void Einheiten::neu(Eingabe& eingabe)

/** Löscht eine Einheit
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **/
void
Einheiten::loesche(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    auto e = eingabe.argument_einheit();
    if (!e)
      break;
    e.loesche();
  }
} // void Einheiten::loesche(Eingabe& eingabe)

/** Ändert eine Einheit
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **
 ** @todo     alles
 **/
void
Einheiten::aendere(Eingabe& eingabe)
{
  do { // while (!eingabe.ist_leer())
    // Namen abfragen
    auto name = eingabe.argument_einheit();
    //this->db_zugriff->loesche(name);
  } while (!eingabe.ist_leer()); 
} // void Einheiten::aendere(Eingabe& eingabe)

} // namespace UI_TEXT
