#pragma once

#include "kontext.h"
#include "../../rezept.h"

namespace UI_TEXT {
class Rezepte;

/** Ein Rezept
 **/
class Rezept : public Kontext {
  friend class Rezepte;
  public:
  Rezept(UI_Text& ui, ::Rezept rezept);

  // Die Hilfe zum Kontext
  string hilfe() const override;

  // Einen Befehl ausführen
  bool befehl(Eingabe& eingabe) override;

  private:
  // Anzeigen
  void zeige(Eingabe& eingabe);
  // in einer Zeile ausgeben
  void schreibe();
  // Diese Rezept löschen
  void loesche(Eingabe& eingabe);
  // Eigenschaft ändern
  void aendere(Eingabe& eingabe);

  // Rezept-Objekt
  ::Rezept rezept;
}; // class Rezept : public Kontext

} // namespace UI_TEXT
