#pragma once

#include "kontext.h"
#include "../../zutat.h"

namespace UI_TEXT {
class Zutaten;

/** Eine Zutat
 **/
class Zutat : public Kontext {
  friend class Zutaten;
  public:
  Zutat(UI_Text& ui, ::Zutat zutat);

  // Die Hilfe zum Kontext
  string hilfe() const override;

  // Einen Befehl ausführen
  bool befehl(Eingabe& eingabe) override;

  private:
  // Anzeigen
  void zeige(Eingabe& eingabe);
  // in einer Zeile ausgeben
  void schreibe();
  // Diese Zutat löschen
  void loesche(Eingabe& eingabe);
  // Eigenschaft ändern
  void aendere(Eingabe& eingabe);

  // Zutat-Objekt
  ::Zutat zutat;
}; // class Zutat : public Kontext

} // namespace UI_TEXT
