#pragma once

#include "kontext.h"

namespace UI_TEXT {
class Zutat;

/** Die Zutatenliste
 **
 ** @todo   Um einen Filter erweitern, um nur bestimmte Zutaten aufzulisten
 **/
class Zutaten : public Kontext {
  public:
    explicit Zutaten(UI_Text& ui);

    // Die Hilfe zum Kontext
    string hilfe() const override;

    // Einen Befehl ausführen
    bool befehl(Eingabe& eingabe) override;

  private:
    // Alle Zutaten auflisten
    void liste(Eingabe& eingabe);
    // Alle Kategorieen auflisten
    void liste_kategorieen(Eingabe& eingabe);
    // Eine neue Zutat eingeben
    void neu(Eingabe& eingabe);
    // Eine Zutat löschen
    void loesche(Eingabe& eingabe);
    // Eine Zutat ändern
    void aendere(Eingabe& eingabe);

    // Zugriff auf eine Zutat
    Zutat zutat(string const& name);
}; // class Zutaten : public Kontext

} // namespace UI_TEXT
