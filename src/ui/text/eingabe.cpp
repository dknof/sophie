#include "constants.h"
#include "eingabe.h"

#include "text.h"

#include "einheiten.h"
#include "zutaten.h"

#include "../../einstellung.h"

#include "../../einheit.h"
#include "../../zutat.h"
#include "../../geschaeft.h"
#include "../../rezept.h"

#include <cstring>

namespace UI_TEXT {

/** Konstruktor
 **
 ** @param   ui   Zugehörige Benutzerschnittstelle
 **/
Eingabe::Eingabe(UI_Text& ui) :
  ui(&ui)
{ }

/** Konstruktor
 **
 ** @param   ui      Zugehörige Benutzerschnittstelle
 ** @param   zeile   Zeile
 **/
Eingabe::Eingabe(UI_Text& ui, string const zeile) :
  ui(&ui), zeile_(zeile)
{ }

/** Liest den nächsten Eingabe aus dem Eingabstrom
 ** Zeigt den Prompt an
 **/
void
Eingabe::lese_zeile()
{
  // Prompt anzeigen
  if (::einstellung(Einstellung::PROMPT))
    *this->ui->ostr << this->ui->prompt();
  this->zeile_ = this->ui->lese_zeile();
  if (::einstellung(Einstellung::BEFEHLE_AUSGEBEN))
    *this->ui->ostr << this->zeile_ << '\n';
  this->pos = 0;
} // void Eingabe::lese_zeile()

/** -> Rückgabe
 **
 ** @return   die gelesene Zeile
 **/
string
Eingabe::zeile() const
{
  return this->zeile_;
} // stringe Eingabe::zeile() const

/** -> Rückgabe
 **
 ** @return   Die Zeile als Konfiguration
 **/
Config
Eingabe::zeile_config() const
{
  return Config(this->zeile_);
} // string Eingabe::zeile_config() const

/** -> Rückgabe
 **
 ** @return   ob der Resttext leer ist (keine Argumente mehr hat)
 **/
bool
Eingabe::ist_leer() const
{
  return (this->pos >= this->zeile_.size());
} // bool Eingabe::ist_leer() const

/** -> Rückgabe
 **
 ** @return   Der verbleibende Text der Zeile ab pos
 **/
string
Eingabe::text() const
{
  if (this->pos >= this->zeile_.size())
    return "";
  return string(this->zeile_, pos, string::npos);
} // string Eingabe::text() const

/** -> Rückgabe
 **
 ** @return   ob der Eingabe aus einem Wort besteht
 **/
bool
Eingabe::ist_ein_wort() const
{
  for (auto& c : this->text())
    if (isspace(c))
      return false;
  return true;
} // bool Eingabe::ist_ein_wort() const

/** -> Rückgabe
 **
 ** @return   das erste Wort des Resttextes (bis Leerzeichen)
 **/
string
Eingabe::erstes_wort() const
{
  auto const& text = this->text();
  bool in_anfuehrungszeichen = false;
  for (auto i = begin(text); i != end(text); ++i) {
    if (*i == '"')
      in_anfuehrungszeichen = !in_anfuehrungszeichen;
    if (!in_anfuehrungszeichen)
      if (isspace(*i))
        return string(begin(text), i);
  }
  return text;
} // string Eingabe::erstes_wort() const

/** -> Rückgabe
 ** Der Name ist der gesamte Text, wenn kein ':' im Text ist.
 ** Ansonsten ist der Name der Text bis zum letzen Leerzeichen vor dem :.
 **
 ** @return   der Name
 **/
string
Eingabe::name() const
{
  auto const& text = this->text();
  auto i = text.find(':');
  if (i == string::npos)
    return text;

  auto j = text.rfind(' ', i-1);
  if (j == string::npos)
    return "";
  while (j > 0 && text[j] == ' ')
    --j;
  if (j == 0)
    return "";

  return string(text, 0, j+1);
} // string Eingabe::name() const

/** Löscht das erste Zeichen
 **/
void
Eingabe::loesche_erstes_zeichen()
{
  if (!this->zeile_.empty())
    this->zeile_.erase(0, 1);
  return ;
} // void Eingabe::loesche_erstes_zeichen()

/** Stringvergleich
 **
 ** @param    text   Text, mit dem verglichen wird
 **
 ** @return   ob die (Rest-)Eingabe dem Text entspricht
 **/
bool
Eingabe::operator==(string const text) const
{
  return (this->text() == text);
}

/** Prüfe, ob die Eingabe dem Befehl entspricht
 ** Es wird ohne Berücksichtigung von Groß-/Kleinschreibung geprüft.
 **
 ** @param    befehl     Befehl, der zu prüfen ist
 **
 ** @return   ob die Eingabe dem Befehl entspricht
 **/
bool
Eingabe::pruefe_befehl(string const befehl)
{
  return pruefe_befehl(befehl, befehl);
}

/** Prüfe, ob die Eingabe dem Befehl entspricht
 ** Es wird ohne Berücksichtigung von Groß-/Kleinschreibung geprüft.
 ** Es reicht aus, die Kurzform einzugeben.
 **
 ** @param    befehl     Befehl, der zu prüfen ist
 ** @param    kurzform   Kurzform des Befehls (es kommt nur auf die Länge des Strings an)
 **
 ** @return   ob die Eingabe dem Befehl entspricht
 **/
bool
Eingabe::pruefe_befehl(string const befehl, string const kurzform)
{
  size_t i; 
  for (i = 0; (i < this->zeile_.length() && i < befehl.length()); ++i)
    if (tolower(this->zeile_[i]) != tolower(befehl[i]))
      break;

  if (i < kurzform.length())
    return false;

  // Die Kurzform passt, nun wird noch geprüft, ob der Rest auch passt.
  while (   (i < this->zeile_.length())
         && (i < befehl.length())) {
    if (this->zeile_[i] != befehl[i]) {
      if (   ::isspace(this->zeile_[i])
          || (this->zeile_[i] == ':'))
        break;
      return false;
    }
  }
  this->pos = i;
  this->ignoriere_leerzeichen();
  return true;
} // bool Eingabe::pruefe_befehl(string befehl, string kurzform)

/** Sucht einen Befehl, der zur Eingabe passt
 ** Es reicht, wenn die ersten eindeutigen Zeichen eingegeben sind
 **
 ** @param    befehle    mögliche Befehle
 **
 ** @return   gefundener Befehl oder leerer String, wenn keiner gefunden wurde
 **/
string
Eingabe::pruefe_befehle(vector<string> const befehle)
{
  string const* befehl = nullptr;
  size_t i;
  size_t i_max = 0; // maximale Länge des gefundenen Eingabes
  for (auto const& b : befehle) {
    for (i = 0; (i < this->zeile_.length() && i < b.length()); ++i)
      if (tolower(this->zeile_[i]) != tolower(b[i]))
        break;

    if (  (i == this->zeile_.length())
        || isspace(this->zeile_[i])
        || (this->zeile_[i] == ':')
        || ((b.length() == 1) && !isalpha(b[0])) ) {
      // Wort zuende
      if (i == b.length()) { // genauen Befehl gefunden
        befehl = &b;
        break;
      } else if (i == i_max) { // Mehrdeutigkeit gefunden
        befehl = nullptr;
      } else if (i > i_max) { // besseren Treffer gefunden
        befehl = &b;
        i_max = i;
      }
    } // if (Wort zuende)
  } // for (auto b : befehle)

  if (befehl) {
    this->pos = i;
    this->ignoriere_leerzeichen();
    return *befehl;
  }
  return "";
} // string Eingabe::pruefe_befehle(vector<string> befehle)

/** Holt den nächsten Wert aus den Argumenten
 **
 ** @return   Wert
 **/
string
Eingabe::naechstes_argument()
{
  if (this->ist_leer()) {
    return "";
  } else {
    auto const wert = this->erstes_wort();
    this->pos += wert.size();
    this->ignoriere_leerzeichen();
    return wert;
  }
} // string Eingabe::naechstes_argument()

/** Holt den nächsten Namen aus den Argumenten
 **
 ** @return   Name
 **/
string
Eingabe::naechster_name()
{
  if (this->ist_leer()) {
    return "";
  } else {
    auto const wert = this->name();
    this->pos += wert.size();
    this->ignoriere_leerzeichen();
    return wert;
  }
} // string Eingabe::naechster_name()

/** -> Rückgabe
 **
 ** @return   das nächste Wort als Konfiguration
 **/
Config
Eingabe::naechstes_config()
{
  auto text = this->text();
  auto i = text.find(':');
  if (i == string::npos) {
    this->pos += text.size();
    return Config(text);
  }

  i = text.find(' ', i+1);
  if (i == string::npos) {
    this->pos += text.size();
    return Config(text);
  }

  i = text.find(':', i+1);
  if (i == string::npos) {
    this->pos += text.size();
    return Config(text);
  }

  auto j = text.rfind(' ', i-2);
    while (text[j] == ' ')
      --j;

  text.erase(j+1);
  this->pos += text.size();
  return Config(text);
} // string Eingabe::naechstes_config()

/** -> Rückgabe
 ** Wird keine Zutat gefunden, ist die Rückgabe leer
 **
 ** @return   Zutat
 **
 ** @todo     Zutaten mit Leerzeichen unterstützen: Liste aller Zutaten durchgehen und prüfen, ob eine passt
 **/
::Zutat
Eingabe::argument_zutat()
{
  if (this->text().empty())
    return ::Zutat();

  auto const zutat = ::Zutat(this->erstes_wort());
  if (!zutat)
    return ::Zutat();

  this->pos += zutat.name().size();
  this->ignoriere_leerzeichen();

  return ::Zutat(zutat);
} // ::Zutat Eingabe::argument_zutat()

/** -> Rückgabe
 ** Wird keine Einheit gefunden, ist die Rückgabe leer
 **
 ** @return   Einheit
 **
 ** @todo     Einheiten mit Leerzeichen unterstützen
 **/
::Einheit
Eingabe::argument_einheit()
{
  if (this->text().empty())
    return ::Einheit();

  auto const einheit = ::Einheit(this->erstes_wort());
  if (!einheit)
    return ::Einheit();

  this->pos += einheit.name().size();
  this->ignoriere_leerzeichen();

  return einheit;
} // ::Einheit Eingabe::argument_einheit()

/** -> Rückgabe
 ** Wird keine Anzahl gefunden, ist die Rückgabe 0
 **
 ** @return   Anzahl
 **/
double
Eingabe::argument_anzahl()
{
  if (this->text().empty())
    return 0;

  auto anzahls = this->erstes_wort();
  auto const anzahl = std::stod(anzahls);

  this->pos += anzahls.size();
  this->ignoriere_leerzeichen();

  return anzahl;
} // double Eingabe::argument_anzahl()

/** -> Rückgabe
 ** Wird keine Menge gefunden, ist die Rückgabe leer
 **
 ** @return   Menge
 **/
Menge
Eingabe::argument_menge()
{
  if (this->text().empty())
    return Menge();
  if (!isdigit(this->text()[0]))
    return Menge();

  Menge menge;
  menge.anzahl = this->argument_anzahl();
  menge.einheit = this->argument_einheit();
  menge.menge = true;

  return menge;
} // string Eingabe::argument_einheit()

/** -> Rückgabe
 ** Wird kein Geschäft gefunden, ist die Rückgabe leer
 **
 ** @return   Geschäft
 **
 ** @todo     Geschäfte mit Leerzeichen unterstützen
 **/
::Geschaeft
Eingabe::argument_geschaeft()
{
  if (this->text().empty())
    return ::Geschaeft();

  auto const geschaeft = ::Geschaeft(this->erstes_wort());
  if (!geschaeft)
    return ::Geschaeft();

  this->pos += geschaeft.name().size();
  this->ignoriere_leerzeichen();

  return geschaeft;
} // ::Geschaeft Eingabe::argument_geschaeft()

/** -> Rückgabe
 ** Wird kein Rezept gefunden, ist die Rückgabe leer
 **
 ** @return   Rezept
 **
 ** @todo     Rezepte mit Leerzeichen unterstützen
 **/
::Rezept
Eingabe::argument_rezept()
{
  if (this->text().empty())
    return ::Rezept();

  auto const rezept = ::Rezept(this->erstes_wort());
  if (!rezept)
    return ::Rezept();

  this->pos += rezept.name().size();
  this->ignoriere_leerzeichen();

  return rezept;
} // ::Rezept Eingabe::argument_rezept()

/** Gibt zurück, ob text leer ist
 ** Abschlussroutine für die parse-Templates
 **/
bool
Eingabe::parse(string& text)
{
  return text.empty();
} // bool Eingabe::parse(string& text)

/** Liest zutat aus text, entfernt den gelesenen Text und die folgenden Leerzeichen
 ** Ändert text nur, wenn er mit einer Zutat beginnt.
 **
 ** @return   true, wenn text mit einer Zutat beginnt, sonst false
 **/
bool
Eingabe::parse_(string& text, ::Zutat& zutat)
{
  auto const wort = Eingabe::erstes_wort(text);
  auto const z = ::Zutat(wort);
  if (!z)
    return false;
  zutat = z;

  text.erase(0, wort.size());
  Eingabe::entferne_leerzeichen(text);
  return true;
} // bool Eingabe::parse_(string& text, ::Zutat& zutat)


/** Liest einheit aus text, entfernt den gelesenen Text und die folgenden Leerzeichen
 ** Ändert text nur, wenn er mit einer Einheit beginnt.
 **
 ** @return   true, wenn text mit einer Einheit beginnt, sonst false
 **/
bool
Eingabe::parse_(string& text, ::Einheit& einheit)
{
  auto const wort = Eingabe::erstes_wort(text);
  auto const e = ::Einheit(wort);
  if (!e)
    return false;
  einheit = e;

  text.erase(0, wort.size());
  Eingabe::entferne_leerzeichen(text);
  return true;
} // bool Eingabe::parse_(string& text, ::Einheit& einheit)

/** Liest anzahl aus text, entfernt den gelesenen Text und die folgenden Leerzeichen
 ** Ändert text nur, wenn er mit einer Anzahl beginnt.
 ** Anzahl hat das Format [0-9]*\.[0-9]* (Beispiel: 1, 1.1, 1., .1)
 **
 ** @return   true, wenn text mit einer Anzahl beginnt, sonst false
 **/
bool
Eingabe::parse_(string& text, Anzahl& anzahl)
{
  if (text.empty())
    return false;
  if (!::isdigit(text.front()))
    return false;
  anzahl = std::stod(text);

  while (!text.empty() && ::isdigit(text.front()))
    text.erase(0, 1);
  while (!text.empty() && text.front() == '.')
    text.erase(0, 1);
  while (!text.empty() && ::isdigit(text.front()))
    text.erase(0, 1);
  Eingabe::entferne_leerzeichen(text);
  return true;
} // bool Eingabe::parse_(string& text, Anzahl& anzahl)

/** Liest menge aus text, entfernt den gelesen Text und die folgenden Leerzeichen
 ** Ändert text nur, wenn er mit einer Menge beginnt.
 **
 ** @return   true, wenn text mit einer Menge beginnt, sonst false
 **/
bool
Eingabe::parse_(string& text, Menge& menge)
{
  Anzahl anzahl;
  ::Einheit einheit;
  if (!this->parse_(text, anzahl))
    return false;
  if (!this->parse_(text, einheit))
    return false;
  menge = {anzahl, einheit};
  return true;
} // bool Eingabe::parse_(string& text, Menge& menge)

/** Liest fixtext aus text, entfernt ihn und die folgenden Leerzeichen
 ** Ändert text nur, wenn er mit fixtext beginnt.
 **
 ** @return   true, wenn text mit fixtext beginnt, sonst false
 **/
bool
Eingabe::parse_(string& text, char const* fixtext)
{
  auto const n = strlen(fixtext);
  if (text.compare(0, n, fixtext))
    return false;
  text.erase(0, n);
  Eingabe::entferne_leerzeichen(text);
  return true;
} // bool Eingabe::parse_(string& text, char const* fixtext)

/** Überspringt das folgende Zeichen
 **
 ** @param   zeichen   zu ignorierendes Zeichen
 **/
void
Eingabe::ignoriere(char const zeichen)
{
  if (   (this->pos < this->zeile_.size())
      && (this->zeile_[this->pos] == zeichen) )
    this->pos += 1;
  this->ignoriere_leerzeichen();
  return ;
} // void Eingabe::ignoriere(char zeichen)

/** Überspringt die folgenden Leerzeichen
 **/
void
Eingabe::ignoriere_leerzeichen()
{
  while (   (this->pos < this->zeile_.size())
         && ::isspace(this->zeile_[this->pos]) )
    this->pos += 1;
  return ;
} // void Eingabe::ignoriere_leerzeichen()

/** Entfernt die Leerzeichen am Anfang von text
 **/
void
Eingabe::entferne_leerzeichen(string& text)
{
  while (!text.empty() && ::isspace(text.front()))
    text.erase(0, 1);
  return ;
} // static void Eingabe::entferne_leerzeichen(string& text)

/** -> Rückgabe
 **
 ** @return   das erste Wort des Textes (bis Leerzeichen)
 **/
string
Eingabe::erstes_wort(string const& text)
{
  bool in_anfuehrungszeichen = false;
  for (auto i = begin(text); i != end(text); ++i) {
    if (*i == '"')
      in_anfuehrungszeichen = !in_anfuehrungszeichen;
    if (!in_anfuehrungszeichen)
      if (isspace(*i))
        return string(begin(text), i);
  }
  return text;
} // static string Eingabe::erstes_wort(string text)

} // namespace UI_TEXT
