#include "constants.h"
#include "rezepte.h"
#include "rezept.h"

#include "text.h"
#include "eingabe.h"
#include "rezept.h"

namespace UI_TEXT {
/** Konstruktor
 **/
Rezepte::Rezepte(UI_Text& ui) :
  Kontext(ui, "Rezepte")
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Rezepteliste
 **/
string 
Rezepte::hilfe() const
{
  return 
    "Liste:  Rezepte auflisten\n"
    "+:      Neues Rezept eingeben\n"
    "-:      Rezept löschen\n"
    "In ein Rezept kann über den Namen gewechselt werden\n"
    ;
} // string Rezepte::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Rezepte::befehl(Eingabe& eingabe)
{
  auto liste = ::Rezept::liste_namen();
  liste.push_back("Liste");
  liste.push_back("L");
  liste.push_back("Zeige");
  liste.push_back("+");
  liste.push_back("-");
  liste.push_back("=");
  auto const befehl = eingabe.pruefe_befehle(liste);

  if (befehl.empty()) {
    return false;
  } else if ((befehl == "Liste") || (befehl == "L")
             || (befehl == "Zeige")) {
    this->liste(eingabe);
    return true;
  } else if (befehl == "+") {
    this->neu(eingabe);
    return true;
  } else if (befehl == "-") {
    this->loesche(eingabe);
    return true;
  } else if (befehl == "=") {
    this->aendere(eingabe);
    return true;
  } else { // Befehl für ein Rezept
    if (eingabe.text().empty()) {
      // In ein Rezept wechseln
      auto rezept = ::Rezept(befehl);
      if (rezept)
      this->ui->zu_kontext(make_unique<Rezept>(*this->ui, rezept));
      return true;
    } else {
      // Befehl an die Rezept weiterreichen
      Eingabe eingabe2(*this->ui, eingabe.text());
      auto rezept = ::Rezept(befehl);
      if (rezept)
        return this->rezept(rezept.name()).befehl(eingabe2);
    } // Befehl für ein Rezept
  }

  return false;
} // bool Rezepte::befehl(Eingabe& eingabe)

/** Listet alle Rezepte auf
 **
 ** @param    filter   (optionaler) Filter
 **/
void
Rezepte::liste(Eingabe& eingabe)
{
  for (auto const& i : ::Rezept::liste())
    *this->ui->ostr << i << '\n';
} // void liste(Eingabe& eingabe)

/** Gibt ein neues Rezept ein
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **/
void
Rezepte::neu(Eingabe& eingabe)
{
  // Namen abfragen
  auto const name = eingabe.naechster_name();
  if (name.empty())
    return ;

  auto rezept = ::Rezept::neu(name);
  this->rezept(name).aendere(eingabe);
} // void neu(Eingabe& eingabe)

/** Löscht ein Rezept
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **/
void
Rezepte::loesche(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    auto r = eingabe.argument_rezept();
    if (!r)
      break;
    r.loesche();
  }
} // void loesche(Eingabe& eingabe)

/** Ändert ein Rezept
 ** Eingabe: Name, zu ändernde Parameter
 **
 ** @param    eingabe    Eingabe
 **
 ** @todo   alles
 **/
void
Rezepte::aendere(Eingabe& eingabe)
{
  do { // while (!eingabe.ist_leer())
    // Namen abfragen
    auto name = eingabe.text();
    //this->db_zugriff->loesche(name);
  } while (!eingabe.ist_leer()); 
} // void aendere(Eingabe& eingabe)

/** Kontext ein einzelnen Rezepts
 **
 ** @param    name   Name der Rezept
 **
 ** @return   Kontext zur angegeben Zusage
 **/
Rezept
Rezepte::rezept(string const& name)
{
  return Rezept(*this->ui, ::Rezept(name));
} // Rezept Rezepte::rezept(string name)

} // namespace UI_TEXT
