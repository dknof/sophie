#pragma once

#include "kontext.h"
#include "../../einkaufsliste.h"

namespace UI_TEXT {

/** Die Einkaufsliste
 **/
class Einkaufsliste : public Kontext {
  public:
    explicit Einkaufsliste(UI_Text& ui);

    // Die Hilfe zum Kontext
    string hilfe() const;

    // Einen Befehl ausführen
    bool befehl(Eingabe& eingabe);

  private:
    // Alle Einträge auflisten
    void liste(Eingabe& eingabe);
    // Einen neuen Eintrag hinzufügen
    void hinzufuege(Eingabe& eingabe);
    // Eine Einheit entfernen
    void entferne(Eingabe& eingabe);

    // Einkaufsliste-Objekt
    ::Einkaufsliste& einkaufsliste;
}; // class Einkaufsliste : public Kontext

} // namespace UI_TEXT
