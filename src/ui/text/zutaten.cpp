#include "constants.h"
#include "zutaten.h"
#include "zutat.h"

#include "text.h"
#include "eingabe.h"

#include "../../zutat.h"
#include "../../zutatenkategorie.h"

namespace UI_TEXT {
/** Konstruktor
 **
 ** @param    ui            zugehörige Benutzerschnittstelle
 **/
Zutaten::Zutaten(UI_Text& ui) :
  Kontext(ui, "Zutaten")
{ }

/** -> Rückgabe
 **
 ** @result   Die Hilfe zur Zutatenliste
 **/
string 
Zutaten::hilfe() const
{
  return 
    "Liste:  Zutaten auflisten\n"
    "Kategorieen:  Kategorieen auflisten\n"
    "+:      Neue Zutat eingeben\n"
    "-:      Eine Zutat löschen\n"
    "=:      Eine Zutat ändern\n"
    "In eine Zutat kann über den Namen gewechselt werden\n"
    ;
} // string Zutaten::hilfe() const

/** Führt einen Befehl aus
 **
 ** @param    eingabe   die Eingabe (Befehl + Argumente)
 **
 ** @result   ob ein Befehl interpretiert wurde
 **/
bool 
Zutaten::befehl(Eingabe& eingabe)
{
  auto liste = ::Zutat::liste_namen();
  liste.push_back("Liste");
  liste.push_back("L");
  liste.push_back("Zeige");
  liste.push_back("Kategorieen");
  liste.push_back("+");
  liste.push_back("-");
  liste.push_back("=");
  auto const befehl = eingabe.pruefe_befehle(liste);

  if (befehl.empty()) {
    return false;
  } else if ((befehl == "Liste") || (befehl == "L")
             || (befehl == "Zeige")) {
    this->liste(eingabe);
    return true;
  } else if (befehl == "Kategorieen") {
    this->liste_kategorieen(eingabe);
    return true;
  } else if (befehl == "+") {
    this->neu(eingabe);
    return true;
  } else if (befehl == "-") {
    this->loesche(eingabe);
    return true;
  } else if (befehl == "=") {
    this->aendere(eingabe);
    return true;
  } else // Befehl für eine Zutat
    if (eingabe.text().empty()) {
      // In eine Zutat wechseln
      auto z = ::Zutat(befehl);
      if (z)
        this->ui->zu_kontext(make_unique<Zutat>(*this->ui, z));
      return true;
    } else {
      // Befehl an die Zutat weiterreichen
      Eingabe eingabe2(*this->ui, eingabe.text());
      return Zutat(*this->ui, ::Zutat(befehl)).befehl(eingabe2);
    } // Befehl für eine Zutat

  return false;
} // bool Zutaten::befehl(Eingabe& eingabe)

/** Listet alle Zutaten auf
 **
 ** @param    filter   (optionaler) Filter
 **/
void
Zutaten::liste(Eingabe& eingabe)
{
  auto kategorie = ::ZutatenKategorie();
  for (auto const& i : ::Zutat::liste()) {
    if (i.kategorie() != kategorie) {
      kategorie = i.kategorie();
      *this->ui->ostr << "* " << (kategorie ? kategorie.name() : "Unkategorisiert"s) << '\n';
    }
    *this->ui->ostr << i.name() << '\n';
  }
} // void liste(Eingabe& eingabe)

/** Listet alle Kategorieen auf
 **
 ** @param    filter   (optionaler) Filter
 **/
void
Zutaten::liste_kategorieen(Eingabe& eingabe)
{
  for (auto const& i : ::ZutatenKategorie::liste())
    *this->ui->ostr << i.name() << '\n';
} // void liste_kategorieen(Eingabe& eingabe)

/** Gibt eine neue Zutat ein
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **/
void
Zutaten::neu(Eingabe& eingabe)
{
  // Namen abfragen
  auto name = eingabe.naechster_name();
  if (name.empty())
    return ;

  // Gegebenenfalls Plural finden
  string name_mehrzahl;
  auto const p = name.find('/');
  if (p != string::npos) {
    name_mehrzahl = string(name, p+1, string::npos);
    name.erase(p, string::npos);
  }

  auto zutat = ::Zutat::neu(name, name_mehrzahl);
  if (zutat)
    this->zutat(name).aendere(eingabe);
} // void neu(Eingabe& eingabe)

/** Löscht eine Zutat
 ** Eingabe: Name
 **
 ** @param    eingabe    Eingabe
 **/
void
Zutaten::loesche(Eingabe& eingabe)
{
  while (!eingabe.ist_leer()) {
    auto z = eingabe.argument_zutat();
    if (!z)
      break;
    z.loesche();
  }
} // void Zutaten::loesche(Eingabe& eingabe)

/** Ändert eine Zutat
 ** Eingabe: Name, zu ändernde Parameter
 **
 ** @param    eingabe    Eingabe
 **
 ** @todo   alles
 **/
void
Zutaten::aendere(Eingabe& eingabe)
{
  do { // while (!eingabe.ist_leer())
    // Namen abfragen
    auto name = eingabe.argument_zutat();
    //this->db_zugriff->loesche(name);
  } while (!eingabe.ist_leer()); 
} // void aendere(Eingabe& eingabe)

/** Kontext einer einzelne Zutat
 **
 ** @param    name   Name der Zutat
 **
 ** @return   Kontext zur angegeben Zusage
 **/
Zutat 
Zutaten::zutat(string const& name)
{
  return Zutat(*this->ui, ::Zutat(name));
} // Zutat Zutaten::zutat(string name)

} // namespace UI_TEXT
