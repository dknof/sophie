#pragma once

/** Eine Benutzerschnittstelle
 **/
class UI {
  public:
    // Destruktor
    virtual ~UI() {}

    // die Initialisierung
    virtual void initialisiere() {}
    // die Hauptroutine
    virtual void starte() = 0;
    // schließe die UI
    virtual void schliesse() {}

    // Fehlermeldung
    virtual void fehler(string text) { cerr << text << endl; }
  private:
}; // class UI

// Die verwendete Benutzerschnittstelle
extern unique_ptr<UI> ui;
