#pragma once

#ifdef USE_UI_GTKMM

#include "ansicht.h"
#include "einheitenumrechner.h"

#include <gtkmm/treemodel.h>
namespace Gtk {
class Toolbar;
class Box;
class Image;
class Label;
class TreeView;
class ListStore;
};

#include "../../einheit.h"

namespace UI_GTKMM {
/** Details über eine Einheit
 **/
class Einheit : public Ansicht {
  public:
    Einheit(Hauptfenster& hauptfenster, ::Einheit const& einheit);
    ~Einheit() override = default;

    void initialisiere();
    void aktualisiere();

    Gtk::Widget& toolbar() override;
    Gtk::Widget& inhalt() override;

  private: // die einzelnen Elemente
    std::unique_ptr<Gtk::Toolbar> toolbar_;
    std::unique_ptr<Gtk::Box> inhalt_;
    Gtk::Image* bild_;
    Gtk::Label* name_;

    Gtk::TreeView* formeln_tabelle_ = nullptr;
    Glib::RefPtr<Gtk::ListStore> formeln_daten_;
    std::unique_ptr<Einheitenumrechner::Spalten> formeln_spalten_;

    ::Einheit einheit;
}; // class Einheit : public Ansicht

} // namespace UI_GTKMM
#endif // #ifdef USE_UI_GTKMM
