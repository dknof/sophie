#pragma once

#ifdef USE_UI_GTKMM

#include "ansicht.h"

namespace Gtk {
class Toolbar;
class Box;
class Image;
class Label;
};
#include "../../geschaeft.h"

namespace UI_GTKMM {

/** Details über ein Geschäft
 **/
class Geschaeft : public Ansicht {
  public:
    Geschaeft(Hauptfenster& hauptfenster, ::Geschaeft const& geschaeft);
    ~Geschaeft() override = default;

    void initialisiere();
    void aktualisiere();

    Gtk::Widget& toolbar() override;
    Gtk::Widget& inhalt() override;

  private: // die einzelnen Elemente
    std::unique_ptr<Gtk::Toolbar> toolbar_;
    std::unique_ptr<Gtk::Box> inhalt_;
    Gtk::Image* bild_;
    Gtk::Label* name_;

    ::Geschaeft geschaeft;
}; // class Geschaeft : public Ansicht

} // namespace UI_GTKMM
#endif // #ifdef USE_UI_GTKMM
