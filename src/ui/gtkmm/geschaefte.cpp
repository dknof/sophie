#ifdef USE_UI_GTKMM

#include "constants.h"

#include "geschaefte.h"
#include "geschaeft.h"
#include "hauptfenster.h"

#include <gtkmm/toolbar.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include "widgets/pixbuf.h"

#include "../../geschaeft.h"

namespace UI_GTKMM {

/** Spalten der Geschäfte-Tabelle
 **/
struct Geschaefte::Spalten : public Gtk::TreeModel::ColumnRecord
{
  Spalten()
  { add(bild_pixbuf); add(name); }

  Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> bild_pixbuf;
  Gtk::TreeModelColumn<Glib::ustring> name;
}; // struct Geschaefte::Spalten

/** Konstruktor
 **/
Geschaefte::Geschaefte(Hauptfenster& hauptfenster) :
  Ansicht{hauptfenster, "Geschäfte"}
{ this->initialisiere(); }

/** Destruktor
 **/
Geschaefte::~Geschaefte()
{ }

/** -> Rückgabe
 **
 ** @return    der Toolbar-Container
 **/
Gtk::Widget& 
Geschaefte::toolbar()
{
  return *this->toolbar_;
} // Gtk::Widget& Geschaefte::toolbar()

/** -> Rückgabe
 **
 ** @return    das Widget mit dem Inhalt
 **/
Gtk::Widget& 
Geschaefte::inhalt()
{
  return *this->inhalt_;
} // Gtk::Widget& Geschaefte::inhalt()

/** Initialisiert die Ansicht
 **/
void
Geschaefte::initialisiere()
{
  { // Toolbar
    this->toolbar_ = std::make_unique<Gtk::Toolbar>();
    auto neu = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::NEW));
    this->toolbar_->add(*neu);
    auto loeschen = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::DELETE));
    this->toolbar_->add(*loeschen);
    this->toolbar_->show_all();
  } // Toolbar

  this->inhalt_ = std::make_unique<Gtk::ScrolledWindow>();
  { // Tabelle
    this->spalten_ = std::make_unique<Spalten>();
    this->daten_ = Gtk::ListStore::create(*this->spalten_);
    this->tabelle_ = Gtk::manage(new Gtk::TreeView{this->daten_});
    this->tabelle_->append_column("Bild", this->spalten_->bild_pixbuf);
    this->tabelle_->append_column("Name", this->spalten_->name);
    this->tabelle_->set_activate_on_single_click();
    this->tabelle_->set_headers_visible(false);
    this->tabelle_->get_column_cell_renderer(0)->set_fixed_size(-1, 3 * EX);

    this->tabelle_->signal_realize().connect(sigc::mem_fun(*this,
                                                           &Geschaefte::aktualisiere));
    this->tabelle_->signal_row_activated().connect(sigc::mem_fun(*this,
                                                                 &Geschaefte::row_activated)); 

    // Funktioniert nicht
    this->tabelle_->add_events(Gdk::KEY_PRESS_MASK);
    this->tabelle_->signal_key_press_event().connect(sigc::mem_fun(*this,
                                                                   &Geschaefte::on_key_press_event));

    this->inhalt_->add(*this->tabelle_);
  } // Tabelle

  this->inhalt_->show_all();

  return ;
} // void Geschaefte::initialisiere()

/** Aktualisiert die Tabelle
 **/
void
Geschaefte::aktualisiere()
{
  if (!this->tabelle_->get_selection()->get_selected_rows().empty()) {
    this->row_activated(this->tabelle_->get_selection()->get_selected_rows()[0],
                        nullptr);
    return ;
  }

  this->daten_->clear();
  auto& spalten = *this->spalten_;
  for (auto const& i : ::Geschaeft::liste()) {
    auto zeile = *this->daten_->append();
    zeile[spalten.name] = i.name();
    if (!i.bild().empty())
      zeile[spalten.bild_pixbuf] = Gdk::Pixbuf_create(i.bild(),
                                                      3 * EX, 3 * EX);
  }

  // Vorauswahl: erster Eintrag
  if (!this->daten_->children().empty())
    this->tabelle_->row_activated(Gtk::TreeModel::Path{"0"},
                                  *this->tabelle_->get_column(0));

  return ;
} // void Geschaefte::aktualisiere()

/** Eine Zeile ist ausgewählt
 **
 ** @param    path     Pfad zur ausgewählten Zeile
 ** @param    --       Spalte mit der Aktivierung
 **/
void
Geschaefte::row_activated(Gtk::TreeModel::Path const& path,
                          Gtk::TreeViewColumn*)
{
  auto zeile = *this->daten_->get_iter(path);
  Glib::ustring const name = zeile[this->spalten_->name];
  this->hauptfenster().geschaeft(::Geschaeft(name)).setze_als_ansicht2();

  return ;
} // void Geschaefte::row_activated(Gtk::TreeModel::Path const& path, Gtk::TreeViewColumn*);

/** Ereignis: Tastendruck
 **
 ** @param    key   gedrückte Taste
 **
 ** @return   ob der Tastendruck verarbeitet wurde
 **/
bool
Geschaefte::on_key_press_event(GdkEventKey* const key)
{
  cout << "Taste (Geschäfte): " << gdk_keyval_name(key->keyval) << '\n';
  if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK))) {
    switch (key->keyval) {
    case GDK_KEY_l: // Geschäfte auflisten
      for (auto const& i : ::Geschaeft::liste())
        cout << i << '\n';
      return true;
    } // switch (key->keyval)
  } // if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK)))

  return false;
} // bool Geschaefte::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
