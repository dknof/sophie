#pragma once

#include "../ui.h"

namespace UI_GTKMM {
class Hauptfenster;

/** Eine Benutzerschnittstelle basierend auf der Bibliothek gtkmm
 **/
class UI_Gtkmm : public UI {
  public:
    UI_Gtkmm();
    ~UI_Gtkmm() override;

    // die Initialisierung
    void initialisiere() override;
    // die Hauptroutine
    void starte() override;
    // schließe die UI
    void schliesse() override;

    // Fehlermeldung
    void fehler(string text) override;
  private:
    // das Hauptfenster
    unique_ptr<Hauptfenster> hauptfenster;
}; // class UI_Gtkmm

} // namespace UI_GTKMM

using UI_GTKMM::UI_Gtkmm;
