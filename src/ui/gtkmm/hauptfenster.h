#pragma once

#ifdef USE_UI_GTKMM

#include <gtkmm/window.h>
namespace Gtk {
class Bin;
class Frame;
class ListBoxRow;
};
#include "../../rezept.h"
#include "../../zutat.h"
#include "../../einheit.h"
#include "../../geschaeft.h"

#include "ansicht.h"
#include "rezepte.h"
#include "rezept.h"
#include "einkaufsliste.h"
#include "vorrat.h"
#include "zutaten.h"
#include "zutat.h"
#include "einheiten.h"
#include "einheit.h"
#include "einheitenumrechner.h"
#include "geschaefte.h"
#include "geschaeft.h"
namespace UI_GTKMM {

class Hauptfenster : public Gtk::Window {
  friend class UI_Gtkmm;
public:
  Hauptfenster();
  ~Hauptfenster() override;

  // Einzelne Ansichten
  Rezept& rezept(::Rezept const& rezept);
  Zutat& zutat(::Zutat const& zutat);
  Einheit& einheit(::Einheit const& einheit);
  Geschaeft& geschaeft(::Geschaeft const& geschaeft);

  // setzt die linke Ansicht
  void setze_ansicht1(Ansicht& ansicht);
  // setzt die rechte Ansicht
  void setze_ansicht2(Ansicht& ansicht);
  // setzt die Ansichten
  void setze_ansichten(Ansicht& ansicht1, Ansicht& ansicht2);

private:
  void initialisiere();

  // Rubrik aktiviert
  void event_row_activated(Gtk::ListBoxRow* zeile);

  bool on_key_press_event(GdkEventKey* key) override;

private: // die einzelnen Elemente
  // erste Ansicht
  Gtk::Bin* ansicht1_toolbar = nullptr;
  Gtk::Frame* ansicht1 = nullptr;
  // zweite Ansicht
  Gtk::Bin* ansicht2_toolbar = nullptr;
  Gtk::Frame* ansicht2 = nullptr;

  // Rubriken
  std::unique_ptr<Rezepte> rezepte;
  std::map<::Rezept, std::unique_ptr<Rezept>> rezept_;
  std::unique_ptr<Einkaufsliste> einkaufsliste;
  std::unique_ptr<Vorrat> vorrat;
  std::unique_ptr<Zutaten> zutaten;
  std::map<::Zutat, std::unique_ptr<Zutat>> zutat_;
  std::unique_ptr<Einheiten> einheiten;
  std::map<::Einheit, std::unique_ptr<Einheit>> einheit_;
  std::unique_ptr<Einheitenumrechner> einheitenumrechner;
  std::unique_ptr<Geschaefte> geschaefte;
  std::map<::Geschaeft, std::unique_ptr<Geschaeft>> geschaeft_;
}; // class Hauptfenster : public Gtk::Window

} // namespace UI_GTKMM

#endif // #idef USE_UI_GTKMM
