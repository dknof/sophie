#pragma once

#ifdef USE_UI_GTKMM

namespace Gtk {
class Widget;
};

namespace UI_GTKMM {
class Hauptfenster;
class Ansicht {
  public:
    Ansicht(Hauptfenster& hauptfenster, string const& name);
    virtual ~Ansicht() = default;

    Hauptfenster& hauptfenster();
    string const& name() const;

    // setzt die Ansicht als erste Ansicht
    virtual void setze_als_ansicht1();
    // setzt die Ansicht als erste Ansicht
    virtual void setze_als_ansicht2();

    virtual Gtk::Widget& toolbar() = 0;
    virtual Gtk::Widget& inhalt() = 0;

  private: // die einzelnen Elemente
    Hauptfenster& hauptfenster_;
    string const name_;
  private: // unused
    Ansicht() = delete;
    Ansicht(Ansicht const&) = delete;
    Ansicht& operator=(Ansicht const&) = delete;
}; // class Ansicht

} // namespace UI_GTKMM
#endif // #ifdef USE_UI_GTKMM
