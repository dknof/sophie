#ifdef USE_UI_GTKMM

#include "constants.h"

#include "vorrat.h"
#include "zutat.h"
#include "hauptfenster.h"

#include <gtkmm/toolbar.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include "widgets/pixbuf.h"

#include "../../vorrat.h"
#include "../../zutat.h"

namespace UI_GTKMM {

/** Spalten der Vorrats-Tabelle
 **/
struct Vorrat::Spalten : public Gtk::TreeModel::ColumnRecord
{
public:
  Spalten()
  { add(anzahl); add(einheit); add(bild_zutat); add(zutat); }

  Gtk::TreeModelColumn<Glib::ustring> anzahl;
  Gtk::TreeModelColumn<Glib::ustring> einheit;
  Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> bild_zutat;
  Gtk::TreeModelColumn<Glib::ustring> zutat;
}; // struct Vorrat::Spalten


/** Konstruktor
 **/
Vorrat::Vorrat(Hauptfenster& hauptfenster) :
  Ansicht{hauptfenster, "Vorrat"}
{ this->initialisiere(); }

/** Destruktor
 **/
Vorrat::~Vorrat()
{ }

/** -> Rückgabe
 **
 ** @return    der Toolbar-Container
 **/
Gtk::Widget& 
Vorrat::toolbar()
{
  return *this->toolbar_;
} // Gtk::Widget& Vorrat::toolbar()

/** -> Rückgabe
 **
 ** @return    das Widget mit dem Inhalt
 **/
Gtk::Widget& 
Vorrat::inhalt()
{
  return *this->inhalt_;
} // Gtk::Widget& Vorrat::inhalt()

/** Initialisiert die Ansicht
 **/
void
Vorrat::initialisiere()
{
  { // Toolbar
    this->toolbar_ = std::make_unique<Gtk::Toolbar>();
    auto neu = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::NEW));
    this->toolbar_->add(*neu);
    auto loeschen = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::DELETE));
    this->toolbar_->add(*loeschen);
    auto drucken = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::PRINT));
    this->toolbar_->add(*drucken);
    this->toolbar_->show_all();
  } // Toolbar

  this->inhalt_ = std::make_unique<Gtk::ScrolledWindow>();
  { // Tabelle
    this->spalten_ = std::make_unique<Spalten>();
    this->daten_ = Gtk::ListStore::create(*this->spalten_);
    this->tabelle_ = Gtk::manage(new Gtk::TreeView{this->daten_});
    this->tabelle_->append_column("Bild", this->spalten_->bild_zutat);
    this->tabelle_->append_column("Zutat", this->spalten_->zutat);
    this->tabelle_->append_column("Anzahl", this->spalten_->anzahl);
    this->tabelle_->append_column("Einheit", this->spalten_->einheit);
    this->tabelle_->set_activate_on_single_click();
    this->tabelle_->set_headers_visible(false);

    this->tabelle_->signal_realize().connect(sigc::mem_fun(*this,
                                                           &Vorrat::aktualisiere));
    this->tabelle_->signal_row_activated().connect(sigc::mem_fun(*this,
                                                                 &Vorrat::row_activated)); 

    // Funktioniert nicht
    this->tabelle_->add_events(Gdk::KEY_PRESS_MASK);
    this->tabelle_->signal_key_press_event().connect(sigc::mem_fun(*this,
                                                                  &Vorrat::on_key_press_event));

    this->inhalt_->add(*this->tabelle_);
    this->inhalt_->show_all();
  } // Tabelle

  return ;
} // void Vorrat::initialisiere()

/** Aktualisiert die Ansicht (Tabelle)
 **/
void
Vorrat::aktualisiere()
{
  if (!this->tabelle_->get_selection()->get_selected_rows().empty()) {
    this->row_activated(this->tabelle_->get_selection()->get_selected_rows()[0],
                        nullptr);
    return ;
  }

  auto const liste = ::vorrat.liste();
  this->daten_->clear();
  auto& spalten = *this->spalten_;
  for (auto const& i : liste) {
    auto zeile = *this->daten_->append();
    zeile[spalten.zutat] = i.zutat.name();
    if (i.menge) {
      zeile[spalten.anzahl] = i.menge.anzahl_text();
      zeile[spalten.einheit] = i.menge.einheit.name();
    }
    zeile[spalten.bild_zutat] = Gdk::Pixbuf_create(i.zutat.bild(), EX, EX);
  } // for (auto& zutat : liste)

  // Vorauswahl: erster Eintrag
  if (!liste.empty())
    this->tabelle_->row_activated(Gtk::TreeModel::Path{"0"},
                                  *this->tabelle_->get_column(0));

  return ;
} // void Vorrat::aktualisiere()

/** Eine Zeile ist ausgewählt
 **
 ** @param    path     Pfad zur ausgewählten Zeile
 ** @param    --       Spalte mit der Aktivierung
 **/
void
Vorrat::row_activated(Gtk::TreeModel::Path const& path,
                      Gtk::TreeViewColumn*)
{
  auto zeile = *this->daten_->get_iter(path);
  Glib::ustring const name = zeile[this->spalten_->zutat];
  this->hauptfenster().zutat(::Zutat(name)).setze_als_ansicht2();

  return ;
} // void Vorrat::row_activated(Gtk::TreeModel::Path const& path, Gtk::TreeViewColumn* column);

/** Ereignis: Tastendruck
 **
 ** @param    key   gedrückte Taste
 **
 ** @return   ob der Tastendruck verarbeitet wurde
 **/
bool
Vorrat::on_key_press_event(GdkEventKey* const key)
{
  cout << "Taste (Vorrat): " << gdk_keyval_name(key->keyval) << '\n';
  if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK))) {
    switch (key->keyval) {
    case GDK_KEY_l: // Vorrat auflisten
      cout << ::vorrat.liste();
      return true;
    } // switch (key->keyval)
  } // if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK)))

  return false;
} // bool Vorrat::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
