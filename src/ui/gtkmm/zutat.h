#pragma once

#ifdef USE_UI_GTKMM

#include "ansicht.h"
#include "einheitenumrechner.h"

namespace Gtk {
class Toolbar;
class Box;
class Image;
class Amount;
class TreeView;
class ListStore;
};
#include "../../zutat.h"

namespace UI_GTKMM {
/** Details über eine Zutat
 **/
class Zutat : public Ansicht {
  public:
    Zutat(Hauptfenster& hauptfenster, ::Zutat const& zutat);
    ~Zutat() override = default;

    void initialisiere();
    void aktualisiere();

    Gtk::Widget& toolbar() override;
    Gtk::Widget& inhalt() override;

  private: // die einzelnen Elemente
    std::unique_ptr<Gtk::Toolbar> toolbar_;
    std::unique_ptr<Gtk::Box> inhalt_;
    Gtk::Image* bild_ = nullptr;
    Gtk::Label* name_ = nullptr;

    Gtk::Amount* vorrat_ = nullptr;
    Gtk::Amount* einkaufsliste_ = nullptr;

    Gtk::TreeView* formeln_tabelle_ = nullptr;
    Glib::RefPtr<Gtk::ListStore> formeln_daten_;
    std::unique_ptr<Einheitenumrechner::Spalten> formeln_spalten_;

    ::Zutat zutat;
}; // class Zutat : public Ansicht

} // namespace UI_GTKMM
#endif // #ifdef USE_UI_GTKMM
