#ifdef USE_UI_GTKMM

#include "constants.h"

#include "zutat.h"
#include "einheitenumrechner.h"

#include <gtkmm/toolbar.h>
#include <gtkmm/grid.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include "widgets/pixbuf.h"
#include "widgets/amount.h"

#include "../../vorrat.h"
#include "../../einkaufsliste.h"

namespace UI_GTKMM {

/** Konstruktor
 **/
Zutat::Zutat(Hauptfenster& hauptfenster, ::Zutat const& zutat) :
  Ansicht{hauptfenster, zutat.name()},
  zutat{zutat}
{ this->initialisiere(); }

/** -> Rückgabe
 **
 ** @return    der Toolbar-Container
 **/
Gtk::Widget& 
Zutat::toolbar()
{
  return *this->toolbar_;
} // Gtk::Widget& Zutat::toolbar()

/** -> Rückgabe
 **
 ** @return    das Widget mit dem Inhalt
 **/
Gtk::Widget& 
Zutat::inhalt()
{
  return *this->inhalt_;
} // Gtk::Widget& Zutat::inhalt()

/** Initialisiert die Ansicht
 **/
void
Zutat::initialisiere()
{
  { // Toolbar
    this->toolbar_ = std::make_unique<Gtk::Toolbar>();
    auto drucken = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::PRINT));
    this->toolbar_->add(*drucken);
    this->toolbar_->show_all();
  } // Toolbar

  { // Tabelle
    this->inhalt_ = std::make_unique<Gtk::VBox>();
    this->inhalt_->set_spacing(1 * EX);

    // Name
    this->name_ = Gtk::manage(new Gtk::Label{});
    this->inhalt_->pack_start(*this->name_, Gtk::PACK_SHRINK);

    // Bild
    this->bild_ = Gtk::manage(new Gtk::Image{});
    this->inhalt_->pack_start(*this->bild_, Gtk::PACK_SHRINK);

    auto grid = Gtk::manage(new Gtk::Grid{});
    grid->set_halign(Gtk::ALIGN_CENTER);
    grid->set_row_spacing(this->inhalt_->get_spacing());
    { // Menge im Vorrat
      auto label = Gtk::manage(new Gtk::Label{"Vorrat: "});
      label->set_halign(Gtk::ALIGN_START);
      grid->attach(*label, 0, 0, 1, 1);
      this->vorrat_ = Gtk::manage(new Gtk::Amount{});
      grid->attach(*this->vorrat_, 1, 0, 1, 1);
    } // Menge im Vorrat
    { // Menge in der Einkaufsliste
      auto label = Gtk::manage(new Gtk::Label{"Einkaufsliste: "});
      grid->attach(*label, 0, 1, 1, 1);
      this->einkaufsliste_ = Gtk::manage(new Gtk::Amount());
      grid->attach(*this->einkaufsliste_, 1, 1, 1, 1);
    }

    this->inhalt_->pack_start(*grid, Gtk::PACK_SHRINK);

    { // Preise in den Geschäften
    } // Preise in den Geschäften

    { // Einheitenumrechner
      auto& tabelle = this->formeln_tabelle_;
      auto& spalten = this->formeln_spalten_;
      auto& daten = this->formeln_daten_;
      spalten = std::make_unique<Einheitenumrechner::Spalten>();
      daten = Gtk::ListStore::create(*spalten);
      tabelle = Gtk::manage(new Gtk::TreeView{daten});
      tabelle->append_column("Anzahl1", spalten->anzahl1);
      tabelle->append_column("Einheit1", spalten->einheit1);
      tabelle->append_column("gleich", spalten->gleich);
      tabelle->append_column("Anzahl2", spalten->anzahl2);
      tabelle->append_column("Einheit2", spalten->einheit2);
      tabelle->set_activate_on_single_click();
      tabelle->set_headers_visible(false);

      this->inhalt_->add(*tabelle);
    } // Einheitenumrechner

    { // Liste der Rezepte mit der Zutat
    }

    this->inhalt_->show_all();
  }

  this->aktualisiere();
  return ;
} // void Zutat::initialisiere()

/** Aktualisiert den Inhalt
 **/
void
Zutat::aktualisiere()
{
  { // Name
    auto const name = this->zutat.name();
    auto const mehrzahl = this->zutat.name_mehrzahl();
    if (mehrzahl.empty() || (mehrzahl == name))
      this->name_->set_markup("<b>" + name + "</b>");
    else
      this->name_->set_markup("<b>" + name + "/" + mehrzahl + "</b>");
  } // Name
  { // Bild
    auto const bild = this->zutat.bild();
    if (bild.empty()) {
      this->bild_->hide();
    } else {
      this->bild_->set(Gdk::Pixbuf_create(bild, 5 * EX, 5 * EX));
      this->bild_->show();
    }
  } // Bild

  { // Menge im Vorrat
    this->vorrat_->set_menge(::vorrat.menge(this->zutat));
  } // Menge im Vorrat
  { // Menge in der Einkaufsliste
    this->einkaufsliste_->set_menge(::einkaufsliste.menge(this->zutat));
  } // Menge in der Einkaufsliste

  { // Einheitenumrechner
    auto& spalten = this->formeln_spalten_;
    auto& daten = this->formeln_daten_;
    daten->clear();

    // Liste der passenden Umrechnungen erstellen
    // Zuerst die speziellen, dann die allgemeinen
    auto const liste = ::einheitenumrechner.liste();
    for (auto const& i : liste)
      if (i.zutat == this->zutat)
        spalten->setze(*daten->append(), i);
    for (auto const& i : liste)
      if (!i.zutat)
        spalten->setze(*daten->append(), i);
  } // Einheitenumrechner

  return ;
} // void Zutat::aktualisiere()

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
