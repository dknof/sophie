#ifdef USE_UI_GTKMM

#include "constants.h"

#include "einheitenumrechner.h"

#include <gtkmm/toolbar.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include "widgets/pixbuf.h"

#include "../../datenbank/einheitenumrechner.h"
#include "../../datenbank/zutat.h"
#include "../../datenbank/einheit.h"

namespace UI_GTKMM {

/** setzt die Formel in die Zeile
 **
 ** @param    row      Zeile zum einsetzen
 ** @param    formel   Formel für die Zeile
 **/
void
Einheitenumrechner::Spalten::setze(Gtk::TreeModel::Row zeile,
                                   Datenbank::Einheitenumrechner::Formel const& formel)
{
  if (formel.zutat) {
    zeile[this->zutat] = formel.zutat.name();
    if (!formel.zutat.bild().empty())
      zeile[this->bild_zutat] = Gdk::Pixbuf_create(formel.zutat.bild(), EX, EX);
  }
  zeile[this->anzahl1] = formel.menge1.anzahl_text();
  if (!formel.menge1.einheit.bild().empty())
    zeile[this->bild_einheit1] = Gdk::Pixbuf_create(formel.menge1.einheit.bild(), EX, EX);
  zeile[this->einheit1] = formel.menge1.einheit.name();
  zeile[this->gleich] = "=";
  zeile[this->anzahl2] = formel.menge2.anzahl_text();
  if (!formel.menge2.einheit.bild().empty())
    zeile[this->bild_einheit2] = Gdk::Pixbuf_create(formel.menge2.einheit.bild(), EX, EX);
  zeile[this->einheit2] = formel.menge2.einheit.name();

  return ;
} // void Einheitenumrechner::Spalten::setze(Gtk::TreeModel::Row& row, Datenbank::Einheitenumrechner::Formel formel)

/** Konstruktor
 **/
Einheitenumrechner::Einheitenumrechner(Hauptfenster& hauptfenster) :
  Ansicht{hauptfenster, "Einheitenumrechner"}
{ this->initialisiere(); }

/** Destruktor
 **/
Einheitenumrechner::~Einheitenumrechner()
{ }

/** -> Rückgabe
 **
 ** @return    der Toolbar-Container
 **/
Gtk::Widget& 
Einheitenumrechner::toolbar()
{
  return *this->toolbar_;
} // Gtk::Widget& Einheitenumrechner::toolbar()

/** -> Rückgabe
 **
 ** @return    das Widget mit dem Inhalt
 **/
Gtk::Widget& 
Einheitenumrechner::inhalt()
{
  return *this->inhalt_;
} // Gtk::Widget& Einheitenumrechner::inhalt()

/** Initialisiert die Ansicht
 **/
void
Einheitenumrechner::initialisiere()
{
  { // Toolbar
    this->toolbar_ = std::make_unique<Gtk::Toolbar>();
    auto neu = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::NEW));
    this->toolbar_->add(*neu);
    auto loeschen = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::DELETE));
    this->toolbar_->add(*loeschen);
    auto drucken = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::PRINT));
    this->toolbar_->add(*drucken);
    this->toolbar_->show_all();
  } // Toolbar

  this->inhalt_ = std::make_unique<Gtk::ScrolledWindow>();
  { // Tabelle
    this->spalten_ = std::make_unique<Spalten>();
    this->daten_ = Gtk::ListStore::create(*this->spalten_);
    this->tabelle_ = Gtk::manage(new Gtk::TreeView{this->daten_});
    this->tabelle_->append_column("Zutat Bild", this->spalten_->bild_zutat);
    //this->tabelle_->append_column("Einheit1 Bild", this->spalten_->bild_einheit1);
    //this->tabelle_->append_column("Einheit2 Bild", this->spalten_->bild_einheit2);
    this->tabelle_->append_column("Zutat", this->spalten_->zutat);
    this->tabelle_->append_column("Anzahl1", this->spalten_->anzahl1);
    this->tabelle_->append_column("Einheit1", this->spalten_->einheit1);
    this->tabelle_->append_column("gleich", this->spalten_->gleich);
    this->tabelle_->append_column("Anzahl2", this->spalten_->anzahl2);
    this->tabelle_->append_column("Einheit2", this->spalten_->einheit2);
    this->tabelle_->set_activate_on_single_click();
    this->tabelle_->set_headers_visible(false);

    this->tabelle_->signal_realize().connect(sigc::mem_fun(*this,
                                                           &Einheitenumrechner::aktualisiere));
    this->tabelle_->signal_row_activated().connect(sigc::mem_fun(*this,
                                                                 &Einheitenumrechner::row_activated)); 

    // Funktioniert nicht
    this->tabelle_->add_events(Gdk::KEY_PRESS_MASK);
    this->tabelle_->signal_key_press_event().connect(sigc::mem_fun(*this,
                                                                   &Einheitenumrechner::on_key_press_event));

    this->inhalt_->add(*this->tabelle_);
    this->inhalt_->show_all();
  } // Tabelle


  return ;
} // void Einheitenumrechner::initialisiere()

/** Aktualisiert die Tabelle
 **/
void
Einheitenumrechner::aktualisiere()
{
  if (!this->tabelle_->get_selection()->get_selected_rows().empty()) {
    this->row_activated(this->tabelle_->get_selection()->get_selected_rows()[0],
                        nullptr);
    return ;
  }

  this->daten_->clear();
  auto& spalten = *this->spalten_;

  auto const liste = ::einheitenumrechner.liste();
  for (auto const& i : liste)
    spalten.setze(*this->daten_->append(), i);

  // Vorauswahl: erster Eintrag
  if (!liste.empty())
    this->tabelle_->row_activated(Gtk::TreeModel::Path{"0"},
                                  *this->tabelle_->get_column(0));

  return ;
} // void Einheitenumrechner::aktualisiere()

/** Eine Zeile ist ausgewählt
 **
 ** @param    path     Pfad zur ausgewählten Zeile
 ** @param    --       Spalte mit der Aktivierung
 **/
void
Einheitenumrechner::row_activated(Gtk::TreeModel::Path const& path,
                                  Gtk::TreeViewColumn*)
{
  //auto zeile = *this->daten_->get_iter(path);
  //Glib::ustring const name = zeile[this->spalten_->name];
  //this->hauptfenster().zutat(name).setze_als_ansicht2();

  return ;
} // void Einheitenumrechner::row_activated(Gtk::TreeModel::Path const& path, Gtk::TreeViewColumn*);

/** Ereignis: Tastendruck
 **
 ** @param    key   gedrückte Taste
 **
 ** @return   ob der Tastendruck verarbeitet wurde
 **/
bool
Einheitenumrechner::on_key_press_event(GdkEventKey* const key)
{
  cout << "Taste (Einheitenumrechner): " << gdk_keyval_name(key->keyval) << '\n';
  if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK))) {
    switch (key->keyval) {
    case GDK_KEY_l: // Einheitenumrechner auflisten
      for (auto& i : ::einheitenumrechner.liste()) {
        cout << i << '\n';
      }
      return true;
    } // switch (key->keyval)
  } // if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK)))

  return false;
} // bool Einheitenumrechner::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
