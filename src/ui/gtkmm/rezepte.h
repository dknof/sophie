#pragma once

#ifdef USE_UI_GTKMM

#include "ansicht.h"

#include <gtkmm/treemodel.h>
namespace Gtk {
class Toolbar;
class TreeView;
class TreeViewColumn;
class ListStore;
class ScrolledWindow;
};

namespace UI_GTKMM {
/** Übersicht über die Rezepte
 **/
class Rezepte : public Ansicht {
struct Spalten;
  public:
    explicit Rezepte(Hauptfenster& hauptfenster);
    ~Rezepte() override;

    void initialisiere();
    void aktualisiere();

    Gtk::Widget& toolbar() override;
    Gtk::Widget& inhalt() override;

  private:
    void row_activated(Gtk::TreeModel::Path const& path,
                       Gtk::TreeViewColumn*);

    bool on_key_press_event(GdkEventKey* key);

  private: // die einzelnen Elemente
    std::unique_ptr<Gtk::Toolbar> toolbar_;
    std::unique_ptr<Gtk::ScrolledWindow> inhalt_;
    Gtk::TreeView* tabelle_ = nullptr;
    Glib::RefPtr<Gtk::ListStore> daten_;
    std::unique_ptr<Spalten> spalten_;

}; // class Rezepte : public Ansicht

} // namespace UI_GTKMM
#endif // #ifdef USE_UI_GTKMM
