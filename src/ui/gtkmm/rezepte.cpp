#ifdef USE_UI_GTKMM

#include "constants.h"

#include "rezepte.h"
#include "rezept.h"
#include "hauptfenster.h"

#include <gtkmm/toolbar.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include "widgets/pixbuf.h"

#include "../../rezept.h"

namespace UI_GTKMM {

/** Spalten der Rezepte-Tabelle
 **/
struct Rezepte::Spalten : public Gtk::TreeModel::ColumnRecord
{
  Spalten()
  { add(bild_pixbuf); add(name); }

  Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> bild_pixbuf;
  Gtk::TreeModelColumn<Glib::ustring> name;
}; // struct Rezepte::Spalten

/** Konstruktor
 **
 ** @param   hauptfenster   das Hauptfenster
 **/
Rezepte::Rezepte(Hauptfenster& hauptfenster) :
  Ansicht{hauptfenster, "Rezepte"}
{ this->initialisiere(); }

/** Destruktor
 **/
Rezepte::~Rezepte()
{ }

/** -> Rückgabe
 **
 ** @return    der Toolbar-Container
 **/
Gtk::Widget& 
Rezepte::toolbar()
{
  return *this->toolbar_;
} // Gtk::Widget& Rezepte::toolbar()

/** -> Rückgabe
 **
 ** @return    das Widget mit dem Inhalt
 **/
Gtk::Widget& 
Rezepte::inhalt()
{
  return *this->inhalt_;
} // Gtk::Widget& Rezepte::inhalt()

/** Initialisiert die Ansicht
 **/
void
Rezepte::initialisiere()
{
  { // Toolbar
    this->toolbar_ = std::make_unique<Gtk::Toolbar>();
    auto neu = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::NEW));
    this->toolbar_->add(*neu);
    auto loeschen = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::DELETE));
    this->toolbar_->add(*loeschen);
    auto drucken = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::PRINT));
    this->toolbar_->add(*drucken);
    this->toolbar_->show_all();
  } // Toolbar

  this->inhalt_ = std::make_unique<Gtk::ScrolledWindow>();
  { // Tabelle
    this->spalten_ = std::make_unique<Spalten>();
    this->daten_ = Gtk::ListStore::create(*this->spalten_);
    this->tabelle_ = Gtk::manage(new Gtk::TreeView{this->daten_});
    this->tabelle_->append_column("Bild", this->spalten_->bild_pixbuf);
    this->tabelle_->append_column("Name", this->spalten_->name);
    this->tabelle_->set_activate_on_single_click();
    this->tabelle_->set_headers_visible(false);
    this->tabelle_->get_column_cell_renderer(0)->set_fixed_size(-1, EX);

    this->tabelle_->signal_realize().connect(sigc::mem_fun(*this,
                                                           &Rezepte::aktualisiere));
    this->tabelle_->signal_row_activated().connect(sigc::mem_fun(*this,
                                                                 &Rezepte::row_activated)); 

    // Funktioniert nicht
    this->tabelle_->add_events(Gdk::KEY_PRESS_MASK);
    this->tabelle_->signal_key_press_event().connect(sigc::mem_fun(*this,
                                                                   &Rezepte::on_key_press_event));

    this->inhalt_->add(*this->tabelle_);
  } // Tabelle

  this->inhalt_->show_all();

  return ;
} // void Rezepte::initialisiere()

/** Aktualisiert die Tabelle
 **/
void
Rezepte::aktualisiere()
{
  if (!this->tabelle_->get_selection()->get_selected_rows().empty()) {
    this->row_activated(this->tabelle_->get_selection()->get_selected_rows()[0],
                        nullptr);
    return ;
  }

  this->daten_->clear();
  auto& spalten = *this->spalten_;
  for (auto const& i : ::Rezept::liste()) {
    auto zeile = *this->daten_->append();
    zeile[spalten.name] = i.name();
    if (!i.bild().empty())
      zeile[spalten.bild_pixbuf] = Gdk::Pixbuf_create(i.bild(),
                                                      EX, EX);
  }

  // Vorauswahl: erster Eintrag
  if (!this->daten_->children().empty())
    this->tabelle_->row_activated(Gtk::TreeModel::Path{"0"},
                                  *this->tabelle_->get_column(0));

  return ;
} // void Rezepte::aktualisiere()

/** Eine Zeile ist ausgewählt
 **
 ** @param    path     Pfad zur ausgewählten Zeile
 ** @param    --       Spalte mit der Aktivierung
 **/
void
Rezepte::row_activated(Gtk::TreeModel::Path const& path,
                       Gtk::TreeViewColumn*)
{
  auto zeile = *this->daten_->get_iter(path);
  Glib::ustring const name = zeile[this->spalten_->name];
  this->hauptfenster().rezept(::Rezept(name)).setze_als_ansicht2();

  return ;
} // void Rezepte::row_activated(Gtk::TreeModel::Path const& path, Gtk::TreeViewColumn*);

/** Ereignis: Tastendruck
 **
 ** @param    key   gedrückte Taste
 **
 ** @return   ob der Tastendruck verarbeitet wurde
 **/
bool
Rezepte::on_key_press_event(GdkEventKey* const key)
{
  cout << "Taste (Rezepte): " << gdk_keyval_name(key->keyval) << '\n';
  if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK))) {
    switch (key->keyval) {
    case GDK_KEY_l: // Rezepte auflisten
      for (auto const& i : ::Rezept::liste())
        cout << i << '\n';
      return true;
    } // switch (key->keyval)
  } // if (!(key->state & ~(GDK_SHIFT_MASK | GDK_MOD1_MASK | GDK_MOD2_MASK)))

  return false;
} // bool Rezepte::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
