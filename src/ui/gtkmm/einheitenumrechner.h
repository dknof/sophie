#pragma once

#ifdef USE_UI_GTKMM

#include "ansicht.h"

#include <gtkmm/treemodel.h>
#include <gdkmm/pixbuf.h>
namespace Gtk {
class Toolbar;
class TreeViewColumn;
class ListStore;
class TreeView;
class ScrolledWindow;
class Label;
};
#include "../../datenbank/einheitenumrechner.h"

namespace UI_GTKMM {
/** Übersicht über die Umrechnungen von Einheiten
 **/
class Einheitenumrechner : public Ansicht {
  public:
    /** Spalten für die Formel-Tabellen
     **/
    struct Spalten : public Gtk::TreeModel::ColumnRecord {
      Spalten()
      { add(zutat); add(bild_zutat); add(anzahl1); add(einheit1); add(bild_einheit1); add(anzahl2); add(einheit2); add(bild_einheit2); add(gleich); }

      // setzt die Formel in die Zeile
      void setze(Gtk::TreeModel::Row row, 
                 Datenbank::Einheitenumrechner::Formel const& formel);

      Gtk::TreeModelColumn<Glib::ustring> zutat;
      Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> bild_zutat;
      Gtk::TreeModelColumn<Glib::ustring> anzahl1;
      Gtk::TreeModelColumn<Glib::ustring> einheit1;
      Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> bild_einheit1;
      Gtk::TreeModelColumn<Glib::ustring> anzahl2;
      Gtk::TreeModelColumn<Glib::ustring> einheit2;
      Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> bild_einheit2;
      Gtk::TreeModelColumn<Glib::ustring> gleich;
    }; // struct Spalten

  public:
    explicit Einheitenumrechner(Hauptfenster& hauptfenster);
    ~Einheitenumrechner() override;

    void initialisiere();
    void aktualisiere();

    Gtk::Widget& toolbar() override;
    Gtk::Widget& inhalt() override;

  private:
    void row_activated(Gtk::TreeModel::Path const& path,
                       Gtk::TreeViewColumn*);

    bool on_key_press_event(GdkEventKey* key);

  private: // die einzelnen Elemente
    std::unique_ptr<Gtk::Toolbar> toolbar_;
    std::unique_ptr<Gtk::ScrolledWindow> inhalt_;
    Gtk::TreeView* tabelle_ = nullptr;
    Glib::RefPtr<Gtk::ListStore> daten_;
    std::unique_ptr<Spalten> spalten_;
}; // class Einheitenumrechner : public Ansicht

} // namespace UI_GTKMM
#endif // #ifdef USE_UI_GTKMM
