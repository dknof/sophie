#ifdef USE_UI_GTKMM

#include "constants.h"

#include "einheit.h"
#include "einheitenumrechner.h"

#include <gtkmm/toolbar.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include <gtkmm/box.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include "widgets/pixbuf.h"

#include "../../einheitenumrechner.h"

namespace UI_GTKMM {

/** Konstruktor
 **/
Einheit::Einheit(Hauptfenster& hauptfenster, ::Einheit const& einheit) :
  Ansicht{hauptfenster, einheit.name()},
  einheit{einheit}
{ this->initialisiere(); }

/** -> Rückgabe
 **
 ** @return    der Toolbar-Container
 **/
Gtk::Widget& 
Einheit::toolbar()
{
  return *this->toolbar_;
} // Gtk::Widget& Einheit::toolbar()

/** -> Rückgabe
 **
 ** @return    das Widget mit dem Inhalt
 **/
Gtk::Widget& 
Einheit::inhalt()
{
  return *this->inhalt_;
} // Gtk::Widget& Einheit::inhalt()

/** Initialisiert die Ansicht
 **/
void
Einheit::initialisiere()
{
  { // Toolbar
    this->toolbar_ = std::make_unique<Gtk::Toolbar>();
    auto drucken = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::PRINT));
    this->toolbar_->add(*drucken);
    this->toolbar_->show_all();
  } // Toolbar

  { // Tabelle
    this->inhalt_ = std::make_unique<Gtk::VBox>();
    this->inhalt_->set_spacing(1 * EX);

    // Name
    this->name_ = Gtk::manage(new Gtk::Label{});
    this->inhalt_->pack_start(*this->name_, Gtk::PACK_SHRINK);

    // Bild
    this->bild_ = Gtk::manage(new Gtk::Image{});
    this->inhalt_->pack_start(*this->bild_, Gtk::PACK_SHRINK);

    { // Einheitenumrechner
      auto& tabelle = this->formeln_tabelle_;
      auto& spalten = this->formeln_spalten_;
      auto& daten = this->formeln_daten_;
      spalten = std::make_unique<Einheitenumrechner::Spalten>();
      daten = Gtk::ListStore::create(*spalten);
      tabelle = Gtk::manage(new Gtk::TreeView{daten});
      tabelle->append_column("Zutat Bild", spalten->bild_zutat);
      //this->tabelle_->append_column("Einheit1 Bild", this->spalten_->bild_einheit1);
      //this->tabelle_->append_column("Einheit2 Bild", this->spalten_->bild_einheit2);
      tabelle->append_column("Zutat", spalten->zutat);
      tabelle->append_column("Anzahl1", spalten->anzahl1);
      tabelle->append_column("Einheit1", spalten->einheit1);
      tabelle->append_column("gleich", spalten->gleich);
      tabelle->append_column("Anzahl2", spalten->anzahl2);
      tabelle->append_column("Einheit2", spalten->einheit2);
      tabelle->set_activate_on_single_click();
      tabelle->set_headers_visible(false);

      this->inhalt_->add(*tabelle);
    } // Einheitenumrechner

    this->inhalt_->show_all();
  }

  this->inhalt_->signal_realize().connect(sigc::mem_fun(*this,
                                                        &Einheit::aktualisiere));

  this->aktualisiere();
  return ;
} // void Einheit::initialisiere()

/** Aktualisiert den Inhalt
 **/
void
Einheit::aktualisiere()
{
  { // Name
    auto const name = this->einheit.name();
    auto const mehrzahl = this->einheit.name_mehrzahl();
    if (   mehrzahl.empty() || (mehrzahl == name))
      this->name_->set_markup("<b>" + name + "</b>");
    else
      this->name_->set_markup("<b>" + name + "/" + mehrzahl + "</b>");
  } // Name
  { // Bild
    auto const bild = this->einheit.bild();
    if (bild.empty()) {
      this->bild_->hide();
    } else {
      this->bild_->set(Gdk::Pixbuf_create(bild, 5 * EX, 5 * EX));
      this->bild_->show();
    }
  } // Bild

  { // Einheitenumrechner
    auto& spalten = this->formeln_spalten_;
    auto& daten = this->formeln_daten_;
    daten->clear();

    auto const liste = ::einheitenumrechner.liste();
    for (auto& i : liste) {
      if (i.menge1.einheit == einheit) {
        spalten->setze(*daten->append(), i);
      } else if (i.menge2.einheit == einheit) {
        auto j = i;
        std::swap(j.menge1, j.menge2);
        spalten->setze(*daten->append(), j);
      }
    } // for (auto& i : liste)
  } // Einheitenumrechner

  return ;
} // void Einheit::aktualisiere()

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
