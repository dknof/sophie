#ifdef USE_UI_GTKMM

#include "constants.h"
#include "gtkmm.h"

#include <gtkmm/application.h>

#include "hauptfenster.h"

namespace UI_GTKMM {

Glib::RefPtr<Gtk::Application> app;

/** Konstruktor
 **/
UI_Gtkmm::UI_Gtkmm()
{ }

/** Destruktor
 **/
UI_Gtkmm::~UI_Gtkmm()
{ }

/** Initialisiere die UI
 **
 ** @todo     alles
 **/
void
UI_Gtkmm::initialisiere()
{
  app = Gtk::Application::create();
  this->hauptfenster = make_unique<Hauptfenster>();
} // void UI_Gtkmm::initialisiere()

/** Die Hauptroutine
 **/
void
UI_Gtkmm::starte()
{
  app->run(*this->hauptfenster);
} // void UI_Gtkmm::starte()

/** Schließt das Hauptfenster
 **/
void
UI_Gtkmm::schliesse()
{
  this->hauptfenster->close();
} // void UI_Gtkmm::schliesse()

/** Zeigt die Fehlermeldung an
 **
 ** @param    text   Fehlermeldung
 **
 ** @todo     alles
 **/
void 
UI_Gtkmm::fehler(string text)
{
  cerr << text << '\n';
  return ;
} // void UI_Gtkmm::fehler(string text)

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
