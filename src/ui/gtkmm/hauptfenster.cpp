#ifdef USE_UI_GTKMM

#include "constants.h"

#include "hauptfenster.h"

// Ansichten
#include "rezepte.h"
#include "rezept.h"
#include "einkaufsliste.h"
#include "vorrat.h"
#include "zutaten.h"
#include "zutat.h"
#include "einheiten.h"
#include "einheit.h"
#include "einheitenumrechner.h"
#include "geschaefte.h"
#include "geschaeft.h"

#include "widgets/imagelabel.h"
#include <gtkmm/alignment.h>
#include <gtkmm/box.h>
#include <gtkmm/frame.h>
#include <gtkmm/grid.h>
#include <gtkmm/label.h>
#include <gtkmm/listbox.h>
#include <gtkmm/toolbar.h>
#include <gtkmm/stock.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/liststore.h>

#include <giomm/memoryinputstream.h>
#include <gtkmm/image.h>

#include "../../einkaufsliste.h"
#include "../../vorrat.h"
#include "../../einheitenumrechner.h"

namespace UI_GTKMM {

/** Konstruktor
 **/
Hauptfenster::Hauptfenster() :
  Gtk::Window{Gtk::WINDOW_TOPLEVEL},
  rezepte{std::make_unique<Rezepte>(*this)},
  einkaufsliste{std::make_unique<Einkaufsliste>(*this)},
  vorrat{std::make_unique<Vorrat>(*this)},
  zutaten{std::make_unique<Zutaten>(*this)},
  einheiten{std::make_unique<Einheiten>(*this)},
  einheitenumrechner{std::make_unique<Einheitenumrechner>(*this)},
  geschaefte{std::make_unique<Geschaefte>(*this)}
{
  this->initialisiere();
} // Hauptfenster::Hauptfenster()

/** Destruktor
 **/
Hauptfenster::~Hauptfenster()
{ }

/** initialisiert das Fenster
 **/
void 
Hauptfenster::initialisiere()
{
  auto grid = Gtk::manage(new Gtk::Grid{});

  { // Icon
    try {
    auto icon = Gdk::Pixbuf::create_from_file("../Bilder/Chef-Hat--Arvin61r58.svg");
    this->set_icon(icon);
    } catch (...) {
    }
  } // Icon

  { // Toolbar
    auto toolbar = Gtk::manage(new Gtk::Toolbar{});
    auto speichern = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::SAVE));
    toolbar->add(*speichern);
    auto hilfe = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::HELP));
    toolbar->add(*hilfe);
    auto beenden = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::QUIT));
    beenden->signal_clicked().connect(sigc::mem_fun(*this, &Hauptfenster::hide));
    toolbar->add(*beenden);

    grid->attach(*toolbar, 0, 0, 1, 1);
  } // Toolbar

  { // Rechte Seite: Zwei Ansichten
    // Zum Beispiel: Rezeptübersicht, ein Rezept
    auto ansichten = Gtk::manage(new Gtk::HBox{});
    auto ansichten_toolbar = Gtk::manage(new Gtk::HBox{});
    ansichten->set_homogeneous();
    ansichten_toolbar->set_homogeneous();
    ansichten->set_hexpand();
    ansichten->set_vexpand();
    ansichten_toolbar->set_hexpand();

    // erste Ansicht
    this->ansicht1 = Gtk::manage(new Gtk::Frame{"Linke Seite"});
    this->ansicht1_toolbar = Gtk::manage(new Gtk::Alignment{Gtk::ALIGN_START});
    ansichten->add(*this->ansicht1);
    ansichten_toolbar->add(*this->ansicht1_toolbar);

    // zweite Ansicht
    this->ansicht2_toolbar = Gtk::manage(new Gtk::Alignment{Gtk::ALIGN_START});
    this->ansicht2 = Gtk::manage(new Gtk::Frame{"Rechte Seite"});
    ansichten->add(*this->ansicht2);
    ansichten_toolbar->add(*this->ansicht2_toolbar);

    grid->attach(*ansichten, 1, 1, 2, 1);
    grid->attach(*ansichten_toolbar, 1, 0, 2, 1);
  } // Rechte Seite: Zwei Ansichten

  { // Linke Seite: Auswahl zwischen den verschiedenen Rubriken:
    // 1. Rezepte
    // 2. Einkaufsliste
    // 3. Vorrat
    // 4. Zutaten
    // 5. Einheiten
    // 6. Einheitenumrechner
    // 7. Geschäfte

    auto titel = Gtk::manage(new Gtk::Label{"Rubriken"});
    titel->set_markup("<b>Rubriken</b>");

    auto rubriken = Gtk::manage(new Gtk::ListBox{});
    rubriken->set_selection_mode(Gtk::SELECTION_BROWSE);
    rubriken->set_activate_on_single_click();
    //rubriken->set_border_width(2 * EX);

    auto rezepte = Gtk::manage(new Gtk::ImageLabel{"../Bilder/Rubriken/chocolateChipsMuffin.svg", "Rezepte", 2 * EX});
    rubriken->add(*rezepte);

    auto einkaufsliste = Gtk::manage(new Gtk::ImageLabel{"../Bilder/Rubriken/Liste.svg", "Einkaufsliste", 2 * EX});
    rubriken->add(*einkaufsliste);

    auto vorrat = Gtk::manage(new Gtk::ImageLabel{"../Bilder/Rubriken/comicFlourSack.svg", "Vorrat", 2 * EX});
    rubriken->add(*vorrat);

    auto zutaten = Gtk::manage(new Gtk::ImageLabel{"../Bilder/Rubriken/carlitos-Egg.svg", "Zutaten", 2 * EX});
    rubriken->add(*zutaten);

    auto einheiten = Gtk::manage(new Gtk::ImageLabel{"../Bilder/Rubriken/Messbecher.svg", "Einheiten", 2 * EX});
    rubriken->add(*einheiten);

    auto einheitenumrechner = Gtk::manage(new Gtk::ImageLabel{"../Bilder/Rubriken/Calculator.svg", "Einheitenumrechner", 2 * EX});
    rubriken->add(*einheitenumrechner);

    auto geschaefte = Gtk::manage(new Gtk::ImageLabel{"../Bilder/Rubriken/shopping-basket.svg", "Geschäfte", 2 * EX});
    rubriken->add(*geschaefte);

    rubriken->signal_row_activated().connect(sigc::mem_fun(*this, &Hauptfenster::event_row_activated));


    titel->set_margin_bottom(EX / 2);
    for (auto& i : rubriken->get_children()) {
      i->set_margin_left(EX / 2);
      i->set_margin_right(EX / 2);
    }
    rubriken->get_row_at_index(0)->set_margin_top(EX / 2);

    auto box = Gtk::manage(new Gtk::VBox{});
    box->pack_start(*titel, Gtk::PACK_SHRINK);
    box->add(*rubriken);
    grid->attach(*box, 0, 1, 1, 1);

    { // Vorauswahl
      auto vorauswahl = rubriken->get_row_at_index(6);
      rubriken->select_row(*vorauswahl);
      this->event_row_activated(vorauswahl);
    } // Vorauswahl
  } // Linke Seite

  this->add(*grid);
  this->show_all_children();

  this->set_default_size(800, 600);

  return ;
} // void Hauptfenster::initialisiere()

/** Detailansicht des Rezeptes
 **/
Rezept&
Hauptfenster::rezept(::Rezept const& rezept)
{
  if (this->rezept_.count(rezept) == 0)
    this->rezept_[rezept] = std::make_unique<Rezept>(*this, rezept);

  return *this->rezept_[rezept];
} // Rezept& Hauptfenster::rezept(::Rezept rezept)

/** Detailansicht der Zutat
 **/
Zutat&
Hauptfenster::zutat(::Zutat const& zutat)
{
  if (this->zutat_.count(zutat) == 0)
    this->zutat_[zutat] = std::make_unique<Zutat>(*this, zutat);

  return *this->zutat_[zutat];
} // Zutat& Hauptfenster::zutat(::Zutat zutat)

/** Detailansicht der Einheit
 **/
Einheit&
Hauptfenster::einheit(::Einheit const& einheit)
{
  if (this->einheit_.count(einheit) == 0)
    this->einheit_[einheit] = std::make_unique<Einheit>(*this, einheit);

  return *this->einheit_[einheit];
} // Einheit& Hauptfenster::einheit(::Einheit einheit)

/** Detailansicht des Geschäfts
 **/
Geschaeft&
Hauptfenster::geschaeft(::Geschaeft const& geschaeft)
{
  if (this->geschaeft_.count(geschaeft) == 0)
    this->geschaeft_[geschaeft] = std::make_unique<Geschaeft>(*this, geschaeft);

  return *this->geschaeft_[geschaeft];
} // Geschaeft& Hauptfenster::geschaeft(::Geschaeft geschaeft)

/** Setzt die erste Ansicht
 **
 ** @param    ansicht   neue Ansicht
 **/
void
Hauptfenster::setze_ansicht1(Ansicht& ansicht)
{
  this->ansicht1->set_label(ansicht.name());
  //if (this->ansicht1->get_child())
  this->ansicht1->remove();
  this->ansicht1->add(ansicht.inhalt());
  this->ansicht1_toolbar->remove();
  this->ansicht1_toolbar->add(ansicht.toolbar());
  return ;
} // void Hauptfenster::setze_ansicht1(Ansicht& ansicht)

/** Setzt die erste Ansicht
 **
 ** @param    ansicht   neue Ansicht
 **/
void
Hauptfenster::setze_ansicht2(Ansicht& ansicht)
{
  this->ansicht2->set_label(ansicht.name());
  //if (this->ansicht2->get_child())
  this->ansicht2->remove();
  this->ansicht2->add(ansicht.inhalt());
  this->ansicht2_toolbar->remove();
  this->ansicht2_toolbar->add(ansicht.toolbar());
  return ;
} // void Hauptfenster::setze_ansicht2(Ansicht& ansicht)

/** Ereignis: Rubrik aktiviert
 **
 ** @param   zeile   ausgewählte Zeile (Rubrik)
 **/
void
Hauptfenster::event_row_activated(Gtk::ListBoxRow* const zeile)
{
  // Offen: direkt den richtigen Wert nehmen
  switch (zeile->get_index()) {
  case 0:
    this->rezepte->setze_als_ansicht1();
    break;
  case 1:
    this->einkaufsliste->setze_als_ansicht1();
    break;
  case 2:
    this->vorrat->setze_als_ansicht1();
    break;
  case 3:
    this->zutaten->setze_als_ansicht1();
    break;
  case 4:
    this->einheiten->setze_als_ansicht1();
    break;
  case 5:
    this->einheitenumrechner->setze_als_ansicht1();
    break;
  case 6:
    this->geschaefte->setze_als_ansicht1();
    break;
  default:
    break;
  } // switch (zeile->get_index())

  return ;
} // void Hauptfenster::event_row_activated(Gtk::ListBoxRow* zeile)

/** Ereignis: Tastendruck
 **
 ** @param    key   gedrückte Taste
 **
 ** @return   ob der Tastendruck verarbeitet wurde
 **/
bool
Hauptfenster::on_key_press_event(GdkEventKey* const key)
{
  cout << "Taste (Hauptfenster): " << gdk_keyval_name(key->keyval) << '\n';
  switch (key->keyval) {
  case GDK_KEY_Escape: // Fenster schließen
    this->hide();
    return true;
  } // switch (key->keyval)

  return false;
} // bool Hauptfenster::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
