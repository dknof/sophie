#ifdef USE_UI_GTKMM

#include "constants.h"

#include "imagelabel.h"
#include "pixbuf.h"

#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/alignment.h>
#include <gtkmm/frame.h>

namespace Gtk {

/** Konstruktor
 **
 ** @param   bild      Bilddatei
 ** @param   text      Text für das Label
 ** @param   hoehe     Höhe des Bildes, Standard: 20
 **/
ImageLabel::ImageLabel(string const& bild,
                       string const& text,
                       int const hoehe) :
  HBox(),
  image(std::make_unique<Image>(scale(Gdk::Pixbuf::create_from_file(bild),
                                      hoehe, hoehe))),
#if 0
  // Variante mit einer Lambda-Funktion
  image(std::make_unique<Image>([=](){auto pixbuf = Gdk::Pixbuf::create_from_file(bild);
                                return pixbuf->scale_simple((pixbuf->get_width() * hoehe) / pixbuf->get_height(), hoehe, Gdk::INTERP_TILES);}()
                               )),
#endif
  label(std::make_unique<Label>(text))
  {
    this->set_spacing(hoehe/2);
    this->image->set_halign(Gtk::ALIGN_CENTER);
    this->image->set_size_request(hoehe, hoehe);
    this->pack_start(*this->image, Gtk::PACK_SHRINK);
    this->label->set_halign(Gtk::ALIGN_START);
    this->pack_start(*this->label, Gtk::PACK_EXPAND_WIDGET);
    this->show_all_children();
  } // ImageLabel::ImageLabel(string bild, string text, int groesse = 20)

/** Destruktor
 **/
ImageLabel::~ImageLabel()
{ }

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
