#pragma once

#ifdef USE_UI_GTKMM

#include <gdkmm/pixbuf.h>

namespace Gdk {
// Skaliert, behält aber das Seitenverhältnis
inline Glib::RefPtr<Gdk::Pixbuf>
  scale(Glib::RefPtr<Gdk::Pixbuf> pixbuf,
        int const max_width,
        int const max_height)
  {
    auto const scale = std::min(max_width * 1.0 / pixbuf->get_width(),
                                max_height * 1.0 / pixbuf->get_height());
    return pixbuf->scale_simple(pixbuf->get_width() * scale,
                                pixbuf->get_height() * scale,
                                Gdk::INTERP_TILES);
  }

// Pixbuf aus einer Datei mit Größenangaben
inline Glib::RefPtr<Gdk::Pixbuf>
  Pixbuf_create(string const& file,
        int const max_width,
        int const max_height)
  {
    return scale(Gdk::Pixbuf::create_from_file(file), max_width, max_height);
  }
}; // namespace Gdk

#endif // #ifndef GTKMM_WIDGET_PIXBUF_H
