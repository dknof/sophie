#pragma once

#ifdef USE_UI_GTKMM

#include <gtkmm/box.h>

namespace Gtk {
/** Eine Menge (Anzahl und Einheit)
 **/
class Amount : public HBox{
  public:
    Amount();
    Amount(Menge const& menge);
    ~Amount();

    void set_menge(Menge const& menge);
    Menge get_menge() const;

  private: // die einzelnen Elemente
    Gtk::Label* anzahl = nullptr;
    Gtk::Label* einheit = nullptr;
}; // class Amount : public HBox

} // namespace UI_GTKMM
#endif // #ifndef GTKMM_WIDGET_AMOUNT_H
