#ifdef USE_UI_GTKMM

#include "constants.h"

#include "amount.h"

#include <gtkmm/label.h>

namespace Gtk {

/** Konstruktor
 **
 ** @param   menge    Menge
 **/
Amount::Amount()
               string const& text,
               int const hoehe :
  HBox(),
  image(std::make_unique<Amount>(scale(Gdk::Pixbuf::create_from_file(bild),
                                      hoehe, hoehe))),
#if 0
  // Variante mit einer Lambda-Funktion
  image(std::make_unique<Amount>([=](){auto pixbuf = Gdk::Pixbuf::create_from_file(bild);
                                return pixbuf->scale_simple((pixbuf->get_width() * hoehe) / pixbuf->get_height(), hoehe, Gdk::INTERP_TILES);}()
                               )),
#endif
  label(std::make_unique<Label>(text))
  {
    this->set_spacing(hoehe/2);
    this->image->set_halign(Gtk::ALIGN_CENTER);
    this->image->set_size_request(hoehe, hoehe);
    this->pack_start(*this->image, Gtk::PACK_SHRINK);
    this->label->set_halign(Gtk::ALIGN_START);
    this->pack_start(*this->label, Gtk::PACK_EXPAND_WIDGET);
    this->show_all_children();
  } // Amount::Amount(string bild, string text, int groesse = 20)

/** Destruktor
 **/
Amount::~Amount()
{ }

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
