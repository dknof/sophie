#pragma once

#ifdef USE_UI_GTKMM

#include <gtkmm/box.h>

namespace Gtk {
  class Image;
  class Label;
/** Ein Label mit einem Bild davor
 **/
class ImageLabel : public HBox{
  public:
    // Konstruktor
    ImageLabel(string const& bild, string const& text, int hoehe = EX);
    // Destruktor
    ~ImageLabel();

  private: // die einzelnen Elemente
    std::unique_ptr<Image> image;
    std::unique_ptr<Label> label;
}; // class ImageLabel : public HBox

} // namespace UI_GTKMM
#endif // #ifndef GTKMM_WIDGET_IMAGELABEL_H
