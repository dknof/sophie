#ifdef USE_UI_GTKMM

#include "constants.h"

#include "amount.h"

#include <gtkmm/label.h>

namespace Gtk {

/** Konstruktor
 **/
Amount::Amount() :
  HBox(),
  anzahl(Gtk::manage(new Gtk::Label(""))),
  einheit(Gtk::manage(new Gtk::Label("")))
{
  this->set_spacing(EX/4);
  this->pack_start(*this->anzahl, Gtk::PACK_SHRINK);
  this->pack_start(*this->einheit, Gtk::PACK_SHRINK);
  this->show_all_children();
} // Amount::Amount()

/** Konstruktor
 **/
Amount::Amount(Menge const& menge) :
  Amount()
{
  this->set_menge(menge);
} // Amount::Amount(Menge menge)

/** Destruktor
 **/
Amount::~Amount()
{ }

/** setze die Menge
 **
 ** @param   menge   Menge
 **/
void
Amount::set_menge(Menge const& menge)
{
  if (menge) {
    this->anzahl->set_text(menge.anzahl_text());
    this->einheit->set_text(menge.einheit.name());
  } else {
    this->anzahl->set_text("");
    this->einheit->set_text("--");
  }
  return ;
} // void Amount::set_menge(Menge menge)

/** -> Rückgabe
 **
 ** @return   die Menge
 **/
Menge
Amount::get_menge() const
{
  return Menge{this->anzahl->get_text(), this->einheit->get_text()};
} // Menge Amount::get_menge() const

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
