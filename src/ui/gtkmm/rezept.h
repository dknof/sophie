#pragma once

#ifdef USE_UI_GTKMM

#include "ansicht.h"

namespace Gtk {
class Toolbar;
class Box;
class Image;
class Label;
};
#include "../../rezept.h"

namespace UI_GTKMM {

/** Details über eine Rezept
 **/
class Rezept : public Ansicht {
  public:
    Rezept(Hauptfenster& hauptfenster, ::Rezept const& rezept);
    ~Rezept() override = default;

    void initialisiere();
    void aktualisiere();

    Gtk::Widget& toolbar() override;
    Gtk::Widget& inhalt() override;

  private: // die einzelnen Elemente
    std::unique_ptr<Gtk::Toolbar> toolbar_;
    std::unique_ptr<Gtk::Box> inhalt_;
    Gtk::Image* bild_ = nullptr;
    Gtk::Label* name_ = nullptr;

    ::Rezept rezept;
}; // class Rezept : public Ansicht

} // namespace UI_GTKMM
#endif // #ifdef USE_UI_GTKMM
