#ifdef USE_UI_GTKMM

#include "constants.h"

#include "ansicht.h"
#include "hauptfenster.h"

namespace UI_GTKMM {

/** Konstruktor
 **/
Ansicht::Ansicht(Hauptfenster& hauptfenster,
                 string const& name) :
  hauptfenster_(hauptfenster),
  name_(name)
{ }

/** -> Rückgabe
 **
 ** @return   das Hauptfenster
 **/
Hauptfenster&
Ansicht::hauptfenster()
{
  return this->hauptfenster_;
}

/** -> Rückgabe
 **
 ** @return   der Name
 **/
string const&
Ansicht::name() const
{
  return this->name_;
}

/**
 ** setzt die Ansicht als erste Ansicht in das Hauptfenster
 **/
void 
Ansicht::setze_als_ansicht1()
{
  this->hauptfenster().setze_ansicht1(*this);
  return ;
} // void Ansicht::setze_als_ansicht1()

/**
 ** setzt die Ansicht als zweite Ansicht in das Hauptfenster
 **/
void 
Ansicht::setze_als_ansicht2()
{
  this->hauptfenster().setze_ansicht2(*this);
  return ;
} // void Ansicht::setze_als_ansicht2()

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
