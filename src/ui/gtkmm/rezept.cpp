#ifdef USE_UI_GTKMM

#include "constants.h"

#include "rezept.h"

#include <gtkmm/toolbar.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include <gtkmm/box.h>
#include "widgets/pixbuf.h"

namespace UI_GTKMM {

/** Konstruktor
 **/
Rezept::Rezept(Hauptfenster& hauptfenster,
               ::Rezept const& rezept) :
  Ansicht{hauptfenster, rezept.name()},
  rezept{rezept}
{ this->initialisiere(); }

/** -> Rückgabe
 **
 ** @return    der Toolbar-Container
 **/
Gtk::Widget& 
Rezept::toolbar()
{
  return *this->toolbar_;
} // Gtk::Widget& Rezept::toolbar()

/** -> Rückgabe
 **
 ** @return    das Widget mit dem Inhalt
 **/
Gtk::Widget& 
Rezept::inhalt()
{
  return *this->inhalt_;
} // Gtk::Widget& Rezept::inhalt()

/** Initialisiert die Ansicht
 **/
void
Rezept::initialisiere()
{
  { // Toolbar
    this->toolbar_ = std::make_unique<Gtk::Toolbar>();
    auto drucken = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::PRINT));
    this->toolbar_->add(*drucken);
    this->toolbar_->show_all();
  } // Toolbar

  { // Tabelle
    this->inhalt_ = std::make_unique<Gtk::VBox>();
    this->inhalt_->set_spacing(1 * EX);

    // Name
    this->name_ = Gtk::manage(new Gtk::Label{});
    this->inhalt_->pack_start(*this->name_, Gtk::PACK_SHRINK);

    // Bild
    this->bild_ = Gtk::manage(new Gtk::Image{});
    this->inhalt_->pack_start(*this->bild_, Gtk::PACK_SHRINK);

    this->inhalt_->show_all();
  }

  this->aktualisiere();
  return ;
} // void Rezept::initialisiere()

/** Aktualisiert den Inhalt
 **/
void
Rezept::aktualisiere()
{
  { // Name
    auto const name = this->rezept.name();
    this->name_->set_markup("<b>" + name + "</b>");
  } // Name
  { // Bild
    auto const bild = this->rezept.bild();
    if (bild.empty()) {
      this->bild_->hide();
    } else {
      this->bild_->set(Gdk::Pixbuf_create(bild, 5 * EX, 5 * EX));
      this->bild_->show();
    }
  } // Bild

  return ;
} // void Rezept::aktualisiere()

} // namespace UI_GTKMM

#endif // #ifdef USE_UI_GTKMM
