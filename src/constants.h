#pragma once

#include <iostream>

#include <string>
#include <vector>
#include <memory>

using namespace std::string_literals;
using namespace std;

using Preis = double;
using Anzahl = double;
using Bild = string;
#include "menge.h"

#include <cassert>

#define DEBUG // debuggen

//extern std::ostream* cdebug_;
#define cdebug cerr // (*cdebug_)
#define CLOG cdebug << __FILE__ << '#' << __LINE__ << ' '

// Absturz erzeugen
#define SEGFAULT abort()

// Die Lizenz
extern char const* const GPL_string;
// Die Version
#include "class/datum/datum.h"
using Version = Datum;
extern Version version;
