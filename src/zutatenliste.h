#pragma once

#include "zutatmenge.h"

using ZutatenListeBase = vector<ZutatMenge>;
/** Liste von Zutaten mit Menge
 ** Beispiel:
 ** 1 kg Zucker
 ** Salz
 **/
class ZutatenListe : private ZutatenListeBase {
  public:
    // Konstruktor
    ZutatenListe();

    // Übernommene Funktionen
    using ZutatenListeBase::begin;
    using ZutatenListeBase::end;
    using ZutatenListeBase::empty;

    // gibt zurück, ob die Zutat enthalten ist
    bool enthaelt(Zutat const& zutat) const;
    // gibt die Menge von der Zutat zurück (muss nicht eindeutig sein)
    Menge menge(Zutat const& zutat) const;

    // Fügt eine Zutat hinzu
    void hinzufuege(ZutatMenge const& zutat);
    void hinzufuege(Zutat const& zutat);
    void hinzufuege(Zutat const& zutat, Menge const& menge);

    // Entfernt eine Zutat
    bool entferne(ZutatMenge const& zutat);
    bool entferne(Zutat const& zutat);
    bool entferne(Zutat const& zutat, Menge const& menge);
}; // class ZutatenListe : private vector<ZutatMenge>

// ZutatenListe ausgeben
ostream& operator<<(ostream& ostr, ZutatenListe const& liste);
