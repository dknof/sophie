#pragma once

#include "einheit.h"

/** Menge
 ** Besteht aus Anzahl und Einheit
 ** Beispiele
 ** 1 kg
 ** 250 g
 **/
struct Menge {
  // Konstruktor
  Menge();
  Menge(Anzahl anzahl, Einheit einheit);
  Menge(string anzahl, string einheit);

  // Ob die Menge gesetzt ist
  operator bool() const;
  
  // die Anzahl als Text
  string anzahl_text() const;

  // Die Eigenschaften
  Anzahl anzahl = 0;
  Einheit einheit;
  bool menge = false; // ob eine Menge gesetzt ist
}; // struct Menge

// Menge ausgeben
ostream& operator<<(ostream& ostr, Menge const& menge);

namespace std {
// Menge in String konvertieren
string to_string(Menge const& menge);
}
