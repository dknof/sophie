#include "constants.h"

#include <sstream>

/** Konstruktor
 **/
Menge::Menge()
{ }

/** Konstruktor
 **
 ** @param    anzahl    Anzahl
 ** @param    einheit   Einheit
 **/
Menge::Menge(Anzahl const anzahl, Einheit const einheit) :
  anzahl(anzahl), einheit(einheit), menge(true)
{ }

/** Konstruktor
 **
 ** @param    anzahl    Anzahl
 ** @param    einheit   Einheit
 **/
Menge::Menge(string const anzahl, string const einheit) :
  anzahl(std::stod(anzahl)), einheit(einheit), menge(true)
{ }

/** -> Rückgabe
 **
 ** @return   Ob die Menge gesetzt ist
 **/
Menge::operator bool() const
{
  return this->menge;
} // Menge::operator bool() const

/** -> Rückgabe
 **
 ** @return   die Anzahl als Text
 **/
string
Menge::anzahl_text() const
{
  if (!menge)
    return "";
  std::ostringstream ostr;
  ostr << this->anzahl;
  return ostr.str();
} // string Menge::anzahl_text() const

/** Menge ausgeben
 **
 ** @param     ostr    Ausgabestrom
 ** @param     menge   Menge
 **
 ** @return    Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, Menge const& menge)
{
  if (!menge)
    return ostr;
  ostr << menge.anzahl << ' ' << menge.einheit.name();
  return ostr;
} // ostream& operator<<(ostream& ostr, Menge const& menge)

/** In String konvertieren
 **
 ** @param    menge   Menge
 **
 ** @return   String, der die Menge ausgibt
 **/
string
std::to_string(Menge const& menge)
{
  if (!menge)
    return "";
  ostringstream ostr;
  ostr << menge;
  return ostr.str();
} // string std::to_string(Menge const& menge)
