#include "constants.h"

#include "einstellung.h"
#include <fstream>

#include "ui/ui.h"

#if 0
#ifdef LINUX
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#endif
#include <sys/stat.h>

#include "../utils/file.h"
#include "../utils/string.h"
#include "../utils/windows.h"
#endif

Einstellung einstellung;

/** Konstruktor
 **/
Einstellung::Einstellung()
{
  this->zuruecksetzen();
  return ;
} // Einstellung::Einstellung()

/** Dektruktor
 **/
Einstellung::~Einstellung()
{ }

/** Setzt die Einstellungen auf die Voreinstellungen zurück
 **/
void
Einstellung::zuruecksetzen()
{
  this->setze(EINSTELLUNGEN_SPEICHERN,    true);
  this->setze(PROMPT,                     true);
  this->setze(BEFEHLE_AUSGEBEN,           false);

  this->setze(DATEIPFAD,		"/home/dknof/.sophie/rezeptbuch.sophie");

  return ;
} // void Einstellung::zuruecksetzen()

/** -> Rückgabe
 **
 ** @param	typ   Einstellung, die ausgegeben wird (Bool)
 **
 ** @return	Wert von der Einstellung typ
 **/
bool
Einstellung::operator()(TypBool const typ) const
{
  return this->bool_[typ - BOOL_ERSTER];
} // bool Einstellung::operator()(TypBool typ) const

/** -> Rückgabe
 **
 ** @param	typ	Einstellung, die ausgegeben wird (String)
 **
 ** @return	Wert von der Einstellung typ
 **/
string
Einstellung::operator()(TypString const typ) const
{
  return this->string_[typ - STRING_ERSTER];
} // string Einstellung::operator()(TypString string) const

/** -> Rückgabe
 **
 ** @param	typ	Einstellung, die ausgegeben wird (String)
 **
 ** @return	Wert von der Einstellung typ
 **/
string
Einstellung::operator()(TypStringKonstant const typ) const
{
  switch(typ) {
  case VERZEICHNIS_PRIVAT:
    return "/home/dknof/.sophie";
  case EINSTELLUNGEN_DATEI:
    return (*this)(VERZEICHNIS_PRIVAT) + "/sophierc";
  } // switch(typ)

  return "";
} // string Einstellung::operator()(TypStringKonstant typ) const

/** -> Rückgabe
 **
 ** @param	name	Name des Typs
 **
 ** @return	Typ mit dem Namen, -1, wenn der Name nicht erkannt wurde
 **/
int
Einstellung::typ(string const& name) const
{
  for (int t = BOOL_ERSTER; t <= BOOL_LETZTER; ++t)
    if (name == Einstellung::name(static_cast<TypBool>(t)))
      return t;

  for (int t = STRING_ERSTER; t <= STRING_LETZTER; ++t)
    if (name == Einstellung::name(static_cast<TypString>(t)))
      return t;

  return -1;
} // int Einstellung::typ(string name) const

/** Setzt die Einstellung
 **
 ** @param	typ	Einstellung
 ** @param	wert    neuer Wert
 **
 ** @return     Ob typ gefunden wurde
 **/
bool
Einstellung::setze(string const& typ, string const& wert)
{
  for (int t = BOOL_ERSTER; t <= BOOL_LETZTER; ++t) {
    if (typ == Einstellung::name(static_cast<TypBool>(t))) {
      this->setze(static_cast<TypBool>(t), wert);
      return true;
    }
  }
  for (int t = STRING_ERSTER; t <= STRING_LETZTER; ++t) {
    if (typ == Einstellung::name(static_cast<TypString>(t))) {
      this->setze(static_cast<TypString>(t), wert);
      return true;
    }
  }

  return false;
} // bool Einstellung::setze(string typ, string wert)

/** Setzt die Einstellung
 **
 ** @param	config   Konfigurationswert
 **
 ** @return     Ob der Wert gefunden wurde
 **/
bool
Einstellung::setze(Config const config)
{
  return this->setze(config.name, config.value);
}

/** Setzt die Einstellung
 **
 ** @param	typ	Einstellung (Bool)
 ** @param	wert    neuer Wert
 **/
void
Einstellung::setze(TypBool const typ, bool const wert)
{
  this->bool_[typ - BOOL_ERSTER] = wert;

  return ;
} // void Einstellung::setze(TypBool typ, bool wert)

/** Setzt die Einstellung
 **
 ** @param	typ	Einstellung (Bool)
 ** @param	wert    neuer Wert
 **/
void
Einstellung::setze(TypBool const typ, string const& wert)
{
  if (   (wert != "true")
      && (wert != "false")
      && (wert != "yes")
      && (wert != "no")
      && (wert != "wahr")
      && (wert != "falsch")
      && (wert != "ja")
      && (wert != "nein")
      && (wert != "0")
      && (wert != "1")) {
    ::ui->fehler("Unerlaubter Wert '" + wert + "' für Einstellung '" + Einstellung::name(typ) + "'. Nehme nein.\n");
  }

  this->setze(typ, ((   (wert == "true")
                    || (wert == "yes")
                    || (wert == "wahr")
                    || (wert == "ja")
                    || (wert == "x"))
                   ? true : false));
  return ;
} // void Einstellung::setze(TypBool typ, string wert)

/** Setzt die Einstellung
 **
 ** @param	typ	Einstellung (String)
 ** @param	wert    neuer Wert
 **/
void
Einstellung::setze(TypString const typ, string const& wert)
{
  this->string_[typ - STRING_ERSTER] = wert;

  return ;
} // void Einstellung::setze(TypString typ, string wert)

/** Lädt die Einstellungen von der Standarddatei
 **/
void
Einstellung::lade()
{
  this->lade((*this)(EINSTELLUNGEN_DATEI));
  return ;
} // void Einstellung::lade()

/** Lädt die Einstellungen aus der angegebenen Datei
 **
 ** @param    dateipfad    Pfad zur Datei mit den Einstellungen
 **/
void
Einstellung::lade(string const& dateipfad)
{
  std::ifstream istr(dateipfad.c_str());

  if (istr.fail()) {
      ::ui->fehler("Einstellung::lade(" + dateipfad + "):\n"
        + "  Fehler beim Öffnen der Datei");
    return ;
  }

  this->lese(istr);
  return ;
} // void Einstellung::lade(string dateipfad)

/** Lädt die Einstellungen aus dem Strom
 **
 ** @param    istr   Eingabestrom
 **/
void
Einstellung::lese(istream& istr)
{
  Config config;
  while (istr.good()) {
    istr >> config;

    // Konfiguration beendet
    if ((config.name == "") && (config.value == ""))
      break;

    if (config.separator) {
      // Eine Einstellung
      if (!this->setze(config)) {
        cerr << "Einstellungen:	"
          << "Ignoriere unbekannte Einstellung '" << config.name << "'."
          << endl;
      }
    } else { // if (config.separator)
#if 0
      if (config.name == "!input") {
        // Die angegebene Datei einfügen
        this->load((DK::Utils::File::dirname(filename) + "/"
                    + config.value));
      } else ;
#endif
      if (config.name == "!Ende") {
        // Rest der Datei ignorieren
        break;
      } else if (config.name == "!stdout") {
        // Gib den Folgetext nach stdout aus
        cout << config.value << endl;
      } else if (config.name == "!stderr") {
        // Gib den Folgetext nach stderr aus
        cerr << config.value << endl;
      } else if (config.name == "") {
        ::ui->fehler("Fehler beim Laden der Einstellungen. "
                     "Ignoriere Zeile '" + config.value + "'");
      } else {
        ::ui->fehler("Fehler beim Laden der Einstellungen. "
                     "Einstellung '" + config.name + "' unbekannt. "
                     + "Ignoriere sie.");
      } // if (config.name == .)
    } // config.separator

  } // while (istr.good())

  return ;
} // void Einstellung::lese(istream& istr)

/** Speichert die Einstellungen in die Standarddatei
 **
 ** @return   ob das Speichern erfolgreich war
 **/
bool
Einstellung::speicher() const
{
  return this->speicher((*this)(EINSTELLUNGEN_DATEI));
} // bool Einstellung::speicher() const

/** Speichert die Einstellungen in der angegebenen Datei
 **
 ** @param    dateipfad    Pfad zur Datei mit den Einstellungen
 **
 ** @return   ob das Speichern erfolgreich war
 **/
bool
Einstellung::speicher(string const& dateipfad) const
{
  if (dateipfad.empty())
    return false;
  auto const dateipfad_tmp = dateipfad + ".tmp";
  std::ofstream ostr(dateipfad_tmp.c_str());
  if (!ostr.good()) {
    ::ui->fehler("Fehler beim Speichern der Einstellungen, konnte die temporäre Datei '" + dateipfad_tmp + "' nicht erstellen.");
    return false;
  }

  ostr << "# Einstellungen für sophie (Version " << ::version << ")\n\n";
  this->schreibe(ostr);

  if (!ostr.good()) {
    ::ui->fehler("Fehler beim Speichern der Einstellungen, siehe temporäre Datei '" + dateipfad_tmp + "'");
    return false;
  }
  ostr.close();

  if (rename(dateipfad_tmp.c_str(), dateipfad.c_str())) {
    ::ui->fehler("Fehler beim Umbenennen der Einstellungsdatei '" + dateipfad_tmp + "' in '" + dateipfad + "'.");
    return false;
  }

  return true;
} // bool Einstellung::speicher(string dateipfad) const

/** Schreibt die Einstellungen in den Ausgabestrom
 **
 ** @param	ostr	Ausgabestrom
 **/
void
Einstellung::schreibe(ostream& ostr) const
{
  std::ios_base::fmtflags const flags = ostr.flags();
  ostr << std::boolalpha;

  for (int t = BOOL_ERSTER; t <= BOOL_LETZTER; ++t)
    ostr << Einstellung::name(static_cast<TypBool>(t)) << ": "
      << ((*this)(static_cast<TypBool>(t)) ? "ja" : "nein") << "\n";

  for (int t = STRING_ERSTER; t <= STRING_LETZTER; ++t)
    ostr << Einstellung::name(static_cast<TypString>(t)) << ": "
      << (*this)(static_cast<TypString>(t)) << "\n";

  ostr.flags(flags);
  return ;
} // void Einstellung::schreibe(ostream& ostr) const

/** -> Rückgabe
 **
 ** @param	typ	Einstellung (bool)
 **
 ** @return	Name von typ
 **/
char const*
Einstellung::name(Einstellung::TypBool const typ)
{
  switch(typ) {
  case Einstellung::EINSTELLUNGEN_SPEICHERN:
    return "Einstellungen speichern";
  case Einstellung::PROMPT:
    return "Prompt";
  case Einstellung::BEFEHLE_AUSGEBEN:
    return "Befehle ausgeben";
  } // switch(typ)

  return "";
} // static char const* Einstellung::name(Einstellung::TypBool typ)

/** -> Rückgabe
 **
 ** @param	typ	Einstellung (String)
 **
 ** @return	Name von typ
 **/
char const*
Einstellung::name(Einstellung::TypString const typ)
{
  switch(typ) {
  case Einstellung::DATEIPFAD:
    return "Pfad der Datei";
  } // switch(typ)

  return "";
} // static char const* Einstellung::name(Einstellung::TypString typ)

/** Die Einstellung in den Ausgabestrom schreiben
 **
 ** @param	ostr	      Ausgabestrom
 ** @param	einstellung   Einstellung
 **
 ** @return	Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, Einstellung const& einstellung)
{
  einstellung.schreibe(ostr);

  return ostr;
} // ostream& operator<<(ostream& ostr, Einstellung einstellung)

/** Die Einstellung aus dem Eingabestrom lesen
 **
 ** @param	istr	      Eingabestrom
 ** @param	einstellung   Einstellung
 **
 ** @return	Eingabestrom
 **/
istream&
operator>>(istream& istr, Einstellung& einstellung)
{
  einstellung.lese(istr);

  return istr;
} // istream& operator>>(istream& istr, Einstellung& einstellung)

/** -> Rückgabe
 **
 ** @param	einstellung_a	erste Einstellung für den Vergleich
 ** @param	einstellung_b	zweite Einstellung für den Vergleich
 **
 ** @return	Ob die beiden Einstellungen verschieden sind
 **/
bool
operator==(Einstellung const& einstellung_a,
           Einstellung const& einstellung_b)
{
  for (int i = Einstellung::BOOL_ERSTER; i <= Einstellung::BOOL_LETZTER; ++i)
    if (einstellung_a(static_cast<Einstellung::TypBool>(i))
        != einstellung_b(static_cast<Einstellung::TypBool>(i)))
      return false;

  for (int i = Einstellung::STRING_ERSTER; i <= Einstellung::STRING_LETZTER; ++i)
    if (einstellung_a(static_cast<Einstellung::TypString>(i))
        != einstellung_b(static_cast<Einstellung::TypString>(i)))
      return false;

  return true;
} // bool operator==(Einstellung einstellung_a, Einstellung einstellung_b)

/** -> Rückgabe
 **
 ** @param	einstellung_a	erste Einstellung für den Vergleich
 ** @param	einstellung_b	zweite Einstellung für den Vergleich
 **
 ** @return	Ob die beiden Einstellungen verschieden sind
 **/
bool
operator!=(Einstellung const& einstellung_a, Einstellung const& einstellung_b)
{
  return !(einstellung_a == einstellung_b);
} // bool operator!=(Einstellung einstellung_a, Einstellung einstellung_b)
