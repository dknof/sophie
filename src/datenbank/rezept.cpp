#include "constants.h"
#include "rezept.h"

#include "datenbank.h"

namespace Datenbank {

/** Erstellt ein neues Rezept
 **/
Rezept
  Rezept::neu(string const& name)
  {
    if (!Rezept(name))
      ::db->sql("INSERT INTO Rezepte "
                "( Name ) "
                "VALUES "
                "( '" + name + "' "
                + " );");
    return Rezept(name);
  } // static Rezept Rezept::neu(string name)

/** Gibt die Liste der Rezepte zurück
 **/
vector<Rezept>
  Rezept::liste()
  {
    ::db->sql("SELECT RezepteID "
              " FROM Rezepte "
              " ORDER BY Name"
              ";");

    auto liste = vector<Rezept>(::db->ergebnisse_anzahl());
    for (size_t i = 0; i < ::db->ergebnisse_anzahl(); ++i)
      liste[i].id_ = ::db->ergebnisse_spalte_strings()[i];
    return liste;
  } // static vector<Rezept> Rezept::liste()

/** Gibt die Liste der Rezeptenamen zurück
 **/
vector<string>
  Rezept::liste_namen()
  {
    ::db->sql("SELECT Name"
              " FROM Rezepte"
              ";");
    if (::db->ergebnisse_anzahl() == 0)
      return {};
    return ::db->ergebnisse_spalte_strings();
  } // static vector<string> Rezept::liste_namen()


/** Konstruktor
 **/
Rezept::Rezept() :
  Objekt("Rezepte")
{ }

/** Konstruktor
 **
 ** @param   name   Name der Rezept
 **/
Rezept::Rezept(string const& name) :
  Objekt("Rezepte", name)
{ }

/** -> Rückgabe
 **
 ** @return   Pfad zum Bild
 **/
string
Rezept::bild() const
{
  return this->wert("Bild");
} // string Rezept::bild() const

/** setzt das Bild
 **
 ** @param    bild    neues Bild
 **/
void
Rezept::setze_bild(string const& bild)
{
  this->setze_wert("Bild", bild);
  return ;
} // void Rezept::setze_bild(string bild)

/** Gibt das Rezept aus
 **
 ** @param    ostr     Ausgabestrom
 ** @param    rezept   Rezept
 **
 ** @return   Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, Rezept const& rezept)
{
  ostr << rezept.name();
  if (!rezept.bild().empty())
    ostr << " Bild:" << rezept.bild();
  return ostr;
} // ostream& operator<<(ostream& ostr, Rezept rezept)

} // namespace Datenbank
