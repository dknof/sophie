#pragma once

#include "objekt.h"

namespace Datenbank {

/** Eine Rezept
 **/
class Rezept : public Objekt {
  public:
    static Rezept neu(string const& name);
    static vector<Rezept> liste();
    static vector<string> liste_namen();

  public:
    Rezept();
    explicit Rezept(string const& name);

    // Zugriffsfunktionen

    string bild() const;

    void setze_bild(string const& bild);

}; // class Geschaeft : public Objekt

ostream& operator<<(ostream& ostr, Rezept const& rezept);

} // namespace Datenbank
