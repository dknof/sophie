#include "constants.h"
#include "zutat.h"
#include "zutatenkategorie.h"
#include "datenbank.h"

namespace Datenbank {

/** Erstellt eine neue Zutat
 **/
Zutat
  Zutat::neu(string const& name,
             string const& name_mehrzahl)
  {
    if (!Zutat(name))
      ::db->sql("INSERT INTO Zutaten "
                "( Name, Mehrzahl ) "
                "VALUES "
                "( '" + name + "', "
                + (  (   !name_mehrzahl.empty()
                      && (name_mehrzahl != name))
                   ? "'" + name_mehrzahl + "'"
                   : "null"s )
                + " );");
    return Zutat(name);
  } // static Zutat Zutat::neu(string name, string name_mehrzahl = "")

/** Gibt die Liste der Zutaten zurück
 **/
vector<Zutat>
  Zutat::liste()
  {
#if 1
    // Erst nach Kategorie, dann nach Name sortiert zurückgeben
    ::db->sql("SELECT K.Name || '/' || Z.Name"
              " FROM Zutaten AS Z, ZutatenKategorieen AS K"
              " WHERE Kategorie = K.ZutatenKategorieenID"
              " UNION"
              " SELECT '\xFF\xFF\xFF\xFF/' || Z.Name"
              " FROM Zutaten AS Z "
              " WHERE Kategorie IS NULL"
              " ORDER BY 1"
              ";");

    auto liste = vector<Zutat>();
    liste.reserve(::db->ergebnisse_anzahl());
    for (auto const& i : ::db->ergebnisse_spalte_strings()) {
      liste.emplace_back(Zutat({i, i.find_last_of('/') + 1}));
    }
    return liste;
#else
    // Nur nach Name sortiert ausgeben
    ::db->sql("SELECT ZutatenID "
              " FROM Zutaten "
              " ORDER BY Name"
              ";");

    auto liste = vector<Zutat>(::db->ergebnisse_anzahl());
    for (size_t i = 0; i < ::db->ergebnisse_anzahl(); ++i)
      liste[i].id_ = ::db->ergebnisse_spalte_strings()[i];
    return liste;
#endif
  } // static vector<Zutat> Zutat::liste()

/** Gibt die Liste der Zutatennamen zurück
 **/
vector<string>
  Zutat::liste_namen()
  {
    ::db->sql("SELECT Name"
              " FROM Zutaten"
              ";");
    if (::db->ergebnisse_anzahl() == 0)
      return {};
    return ::db->ergebnisse_spalte_strings();
  } // static vector<string> Zutat::liste_namen()

/** Konstruktor
 **/
Zutat::Zutat() :
  Objekt("Zutaten")
{ }

/** Konstruktor
 **
 ** @param   name   Name der Zutat
 **/
Zutat::Zutat(string const& name) :
  Objekt("Zutaten", name)
{ }

/** -> Rückgabe
 **
 ** @return   Name Mehrzahl der Zutat
 **/
string
Zutat::name_mehrzahl() const
{
  auto const mehrzahl = this->wert("Mehrzahl");
  return (mehrzahl == this->name()
          ? ""
          : mehrzahl);
} // string Zutaten::name_mehrzahl() const

/** -> Rückgabe
 **
 ** @return   Pfad zum Bild
 **/
Bild
Zutat::bild() const
{
  return this->wert("Bild");
} // Bild Zutaten::bild() const

/** -> Rückgabe
 **
 ** @return   Zutatenkategorie
 **/
ZutatenKategorie
Zutat::kategorie() const
{
  return ZutatenKategorie::aus_id(this->wert("Kategorie"));
} // ZutatenKategorie Zutaten::kategorie() const

/** setzt den Namen (Mehrzahl)
 **
 ** @param    name_mehrzahl    neuer Name (Mehrzahl)
 **/
void
Zutat::setze_name_mehrzahl(string const& name_mehrzahl)
{
  this->setze_wert("Mehrzahl", name_mehrzahl);
  return ;
} // void Zutat::setze_name_mehrzahl(string name_mehrzahl)

/** setzt das Bild
 **
 ** @param    bild    neues Bild (Pfad)
 **/
void
Zutat::setze_bild(Bild const& bild)
{
  this->setze_wert("Bild", bild);
  return ;
} // void Zutat::setze_bild(Bild bild)

/** setzt die Kategorie
 **
 ** @param    name   neue Kategorie
 **/
void
Zutat::setze_kategorie(ZutatenKategorie const& kategorie)
{
  this->setze_wert("Kategorie", kategorie.id());
  return ;
} // void Zutat::setze_kategorie(ZutatenKategorie kategorie)

/** setzt die Kategorie
 **
 ** @param    name   neue Kategorie
 **/
void
Zutat::setze_kategorie(string const& kategorie)
{
  auto zk = ZutatenKategorie(kategorie);
  if (!zk)
    zk = ZutatenKategorie::neu(kategorie);
  this->setze_wert("Kategorie", zk.id());
  return ;
} // void Zutat::setze_kategorie(string kategorie)

/** Gibt die Zutat aus
 **
 ** @param    ostr    Ausgabestrom
 ** @param    zutat   Zutat
 **
 ** @return   Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, Zutat const& zutat)
{
  ostr << zutat.name();
  if (!zutat.name_mehrzahl().empty())
    ostr << '/' << zutat.name_mehrzahl();
  if (zutat.kategorie())
    ostr << " Kategorie:" << zutat.kategorie().name();
  if (!zutat.bild().empty())
    ostr << " Bild:" << zutat.bild();
  return ostr;
} // ostream& operator<<(ostream& ostr, Zutat zutat)

} // namespace Datenbank
