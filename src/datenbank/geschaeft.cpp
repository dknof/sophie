#include "constants.h"
#include "geschaeft.h"
#include "zutat.h"
#include "einheit.h"
#include "datenbank.h"

namespace Datenbank {

/** Erstellt ein neues Geschäft
 **/
Geschaeft
  Geschaeft::neu(string const& name)
  {
    if (!Geschaeft(name))
      ::db->sql("INSERT INTO Geschäfte "
                "( Name ) "
                "VALUES "
                "( '" + name + "' "
                + " );");
    return Geschaeft(name);
  } // static Geschaeft Geschaeft::neu(string name)

/** Gibt die Liste der Geschäfte zurück
 **/
vector<Geschaeft>
  Geschaeft::liste()
  {
    ::db->sql("SELECT GeschäfteID "
              " FROM Geschäfte "
              " ORDER BY Name"
              ";");

    auto liste = vector<Geschaeft>(::db->ergebnisse_anzahl());
    for (size_t i = 0; i < ::db->ergebnisse_anzahl(); ++i)
      liste[i].id_ = ::db->ergebnisse_spalte_strings()[i];
    return liste;
  } // static vector<Geschaeft> Geschaeft::liste()

/** Gibt die Liste der Geschäftnamen zurück
 **/
vector<string>
  Geschaeft::liste_namen()
  {
    ::db->sql("SELECT Name"
              " FROM Geschäfte"
              ";");
    if (::db->ergebnisse_anzahl() == 0)
      return {};
    return ::db->ergebnisse_spalte_strings();
  } // static vector<string> Geschaeft::liste_namen()

/** Konstruktor
 **/
Geschaeft::Geschaeft() :
  Objekt("Geschäfte")
{ }

/** Konstruktor
 **
 ** @param   name   Name des Geschäfts
 **/
Geschaeft::Geschaeft(string const& name) :
  Objekt("Geschäfte", name)
{ }

/** -> Rückgabe
 **
 ** @return   Pfad zum Bild
 **/
Bild
Geschaeft::bild() const
{
  return this->wert("Bild");
} // Bild Geschaeft::bild() const

/** setzt das Bild
 **
 ** @param    bild    neues Bild
 **/
void
Geschaeft::setze_bild(Bild const& bild)
{
  this->setze_wert("Bild", bild);
  return ;
} // void Geschaeft::setze_bild(Bild bild)

/** -> Rückgabe
 **
 ** @return   alle Einträge in dem Geschäft
 **/
ZutatenPreisListe
Geschaeft::zutaten() const
{
  // Da die Einheit auch NULL sein kann, wird die Übersetzung von ID in Name erst in der Schleife nach der Abfrage vorgenommen. Ansonsten gibt es einen Fehler, da in der Tabelle Einheiten nicht nach ID=NULL gesucht werden kann.
  ::db->sql("SELECT Z.Name, GZ.Einheit, GZ.Anzahl, GZ.Preis"
            "  FROM GeschäfteZutaten as GZ, Zutaten AS Z "
            "  WHERE GZ.Geschäft = " + this->id()
            + "  AND Z.ZutatenID = GZ.Zutat"
            + "  ORDER BY GZ.Position"
            + ";");

  ZutatenPreisListe liste;
  for (auto e : ::db->ergebnisse()) {
    e[1] = ::db->name("Einheiten", "EinheitenID", e[1]);
    liste.hinzufuege(ZutatMengePreis{e});
  }

  return liste;
} // ZutatenPreisListe Geschaeft::zutaten() const

/** Gibt zurück, ob keine Zutaten im Geschäft sind.
 **/
bool
Geschaeft::istleer() const
{
  ::db->sql("SELECT Geschäft"
            "  FROM GeschäfteZutaten "
            "  WHERE Geschäft = " + this->id()
            + ";");
  return (::db->ergebnisse_anzahl() == 0);
} // bool Geschaeft::istleer() const

/** Gibt zurück, ob eine Zutat enthalten ist
 **/
bool
Geschaeft::enthaelt(Zutat const& zutat) const
{
  // Prüfe, ob die Zutat bereits enthalten ist
  ::db->sql("SELECT Zutat "
            "FROM GeschäfteZutaten as GZ, Zutaten AS Z "
            "WHERE GZ.Geschäft = " + this->id() + " "
            + "AND GZ.Zutat = " + zutat.id()
            + ";");
  return (::db->ergebnisse_anzahl() != 0);
} // bool Geschaeft::enthaelt(Zutat const& zutat)

/** Fügt eine Zutat hinzu
 **/
void
Geschaeft::hinzufuege(Zutat const& zutat)
{
  if (this->enthaelt(zutat))
    return ;
  ::db->sql("INSERT INTO GeschäfteZutaten "
            "( Geschäft, Position, Zutat, Einheit, Anzahl, Preis ) "
            "VALUES "
            "( " + this->id() + ", "
            + (this->istleer()
               ? "1, "s
               : ("( SELECT MAX(Position)"
                  "    FROM GeschäfteZutaten"
                  "    WHERE Geschäft = " + this->id()
                  + ")+1, ")
              )
            + zutat.id() + ", NULL, NULL, NULL "
            + " );");
} // void Geschaeft::hinzufuege(Zutat const& zutat)

/** Fügt eine Zutat mit Menge hinzu
 **/
void
Geschaeft::hinzufuege(Zutat const& zutat, Menge const& menge)
{
  if (this->enthaelt(zutat))
    return ;
  ::db->sql("INSERT INTO GeschäfteZutaten "
            "( Geschäft, Position, Zutat, Einheit, Anzahl, Preis ) "
            "VALUES "
            "( " + this->id() + ", "
            + (this->istleer()
               ? "1, "s
               : ("( SELECT MAX(Position)"
                  "    FROM GeschäfteZutaten"
                  "    WHERE Geschäft = " + this->id()
                  + ")+1, ")
              )
            + zutat.id() + ", "
            + menge.einheit.id() + ", "
            + to_string(menge.anzahl) + ", "
            + "NULL "
            + " );");
} // void Geschaeft::hinzufuege(Zutat zutat, Menge menge)

/** Fügt eine Zutat mit Menge und Preis hinzu
 **/
void
Geschaeft::hinzufuege(Zutat const& zutat,
                      Menge const& menge, Preis const preis)
{
  if (this->enthaelt(zutat))
    return ;
  ::db->sql("INSERT INTO GeschäfteZutaten "
            "( Geschäft, Position, Zutat, Einheit, Anzahl, Preis ) "
            "VALUES "
            "( " + this->id() + ", "
            + (this->istleer()
               ? "1, "s
               : ("( SELECT MAX(Position)"
                  "    FROM GeschäfteZutaten"
                  "    WHERE Geschäft = " + this->id()
                  + ")+1, ")
              )
            + zutat.id() + ", "
            + menge.einheit.id() + ", "
            + to_string(menge.anzahl) + ", "
            + to_string(preis)
            + " );");
} // void Geschaeft::hinzufuege(Zutat zutat, Menge menge, Preis preis)

/** Eine Zutat aus der Geschäft löschen
 **
 ** @param   zutat    Zutat
 **
 ** @return   ob die Zutat in der Liste enthalten war
 **/
bool
Geschaeft::entferne(Zutat const& zutat)
{
  if (!this->enthaelt(zutat))
    return false;

  ::db->sql("DELETE FROM GeschäfteZutaten"
            "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat.id()
            + ";");
  return true;
} // bool Geschaeft::entferne(string zutat)

/** Entfernt alle Einträge in der Geschäft
 **/
void
Geschaeft::entferne_alles()
{
  ::db->sql("DELETE FROM GeschäfteZutaten "
            "  WHERE Geschäft = " + this->id());
  return ;
} // void Geschaeft::entferne_alles()

/** Verschiebt zutat an die Position vor referenz
 **/
void
Geschaeft::verschiebe_vor(Zutat const& zutat, Zutat const& referenz)
{
  if (!this->enthaelt(zutat) || !this->enthaelt(referenz))
    return ;
  ::db->sql("SELECT Position"
            "  FROM GeschäfteZutaten"
            "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + referenz.id()
            + "  ;");
  auto const n = ::db->ergebnis_string();
  ::db->sql("UPDATE GeschäfteZutaten "
            "  SET Position = Position+1"
            "  WHERE Geschäft = " + this->id()
            + "  AND Position >= " + n
            + "  ;");
  ::db->sql("UPDATE GeschäfteZutaten "
            "  SET Position = " + n
            + "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat.id()
            + "  ;");
} // void Geschaeft::verschiebe_vor(Zutat zutat, Zutat referenz)

/** Verschiebt zutat an die Position hinter referenz
 **/
void
Geschaeft::verschiebe_hinter(Zutat const& zutat, Zutat const& referenz)
{
  if (!this->enthaelt(zutat) || !this->enthaelt(referenz))
    return ;
  ::db->sql("SELECT Position"
            "  FROM GeschäfteZutaten"
            "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + referenz.id()
            + "  ;");
  auto const n = ::db->ergebnis_string();
  ::db->sql("UPDATE GeschäfteZutaten "
            "  SET Position = Position+1"
            "  WHERE Geschäft = " + this->id()
            + "  AND Position > " + n
            + "  ;");
  ::db->sql("UPDATE GeschäfteZutaten "
            "  SET Position = " + n + "+1"
            + "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat.id()
            + "  ;");
} // void Geschaeft::verschiebe_hinter(Zutat zutat, Zutat referenz)

/** Verschiebt zutat an den Anfang der Liste
 **/
void
Geschaeft::verschiebe_an_anfang(Zutat const& zutat)
{
  if (!this->enthaelt(zutat))
    return ;
  ::db->sql("SELECT MIN(Position)"
            "  FROM GeschäfteZutaten"
            "  WHERE Geschäft = " + this->id()
            + "  ;");
  auto const n = ::db->ergebnis_string();
  ::db->sql("UPDATE GeschäfteZutaten "
            "  SET Position = " + n + "-1"
            + "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat.id()
            + "  ;");
} // void Geschaeft::verschiebe_an_anfang(Zutat zutat)

/** Verschiebt zutat an das Ende der Liste
 **/
void
Geschaeft::verschiebe_an_ende(Zutat const& zutat)
{
  if (!this->enthaelt(zutat))
    return ;
  ::db->sql("SELECT MAX(Position)"
            "  FROM GeschäfteZutaten"
            "  WHERE Geschäft = " + this->id()
            + "  ;");
  auto const n = ::db->ergebnis_string();
  ::db->sql("UPDATE GeschäfteZutaten "
            "  SET Position = " + n + "+1"
            + "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat.id()
            + "  ;");
} // void Geschaeft::verschiebe_an_ende(Zutat zutat)

/** Vertauscht die Positionen von zutat1 und zutat2
 **/
void
Geschaeft::vertausche(Zutat const& zutat1, Zutat const& zutat2)
{
  if (!this->enthaelt(zutat1) || !this->enthaelt(zutat2))
    return ;
  ::db->sql("SELECT Position"
            "  FROM GeschäfteZutaten"
            "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat1.id()
            + "  ;");
  auto const n1 = ::db->ergebnis_string();
  ::db->sql("SELECT Position"
            "  FROM GeschäfteZutaten"
            "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat2.id()
            + "  ;");
  auto const n2 = ::db->ergebnis_string();
  ::db->sql("UPDATE GeschäfteZutaten "
            "  SET Position = " + n2
            + "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat1.id()
            + "  ;"
            + "UPDATE GeschäfteZutaten "
            + "  SET Position = " + n1
            + "  WHERE Geschäft = " + this->id()
            + "  AND Zutat = " + zutat2.id()
            + "  ;");
} // void Geschaeft::vertausche(Zutat zutat1, Zutat zutat2)

/** Gibt das Geschäft aus
 **
 ** @param    ostr        Ausgabestrom
 ** @param    geschaeft   Geschäft
 **
 ** @return   Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, Geschaeft const& geschaeft)
{
  ostr << geschaeft.name();
  if (!geschaeft.bild().empty())
    ostr << " Bild:" << geschaeft.bild();
  return ostr;
} // ostream& operator<<(ostream& ostr, Geschaeft geschaeft)

} // namespace Datenbank
