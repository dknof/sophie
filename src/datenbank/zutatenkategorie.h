#pragma once

#include "objekt.h"

namespace Datenbank {

/** Eine ZutatenKategorie
 **/
class ZutatenKategorie : public Objekt {
  public:
    static ZutatenKategorie neu(string const& name);
    static ZutatenKategorie aus_id(string const& id);
    static vector<ZutatenKategorie> liste();
    static vector<string> liste_namen();

  public:
    ZutatenKategorie();
    explicit ZutatenKategorie(string const& name);

    // Zugriffsfunktionen

    string bild() const;

    void setze_bild(string const& bild);

}; // class ZutatenKategorie : public Objekt

// Gibt das ZutatenKategorie aus
ostream& operator<<(ostream& ostr, ZutatenKategorie const& zutatenkategorie);

} // namespace Datenbank
