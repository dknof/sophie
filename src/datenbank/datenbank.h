#pragma once

struct sqlite3;

namespace Datenbank {

/** Zugriff auf die Daten.
 ** Auf die Daten wird ausschließlich über Objekte-Klassen zugegriffen.
 ** Aktuell ist dies ein direkter Zugriff auf eine sqlite3-Datenbank.
 **/
class Datenbank {
  friend int sql_callback(void *NotUsed, int argc, char **argv, char **azColName);
public:
  // Konstruktor
  Datenbank();
  // Destruktor
  ~Datenbank();

  // Prüft, ob die Datenbank konsistent ist
  bool konsistent() const;

  // Öffnet die Datenbank
  void oeffnen();
  // Initialisiert die Datenbank
  void initialisieren();

  // Speichert die Daten
  void schreibe(ostream& ostr);
  bool speicher();
  bool speicher(string const& datei);
  bool speicher_sqlite3(string const& datei);
  // Lädt die Daten
  void lese(istream& istr);
  void lade();
  void lade(string const& datei);

  // Einen sql-Befehl ausführen
  bool sql(string const& befehl);
  // gibt die Identifikationsnummer für ein Element zurück
  string id(string const& tabelle,
            string const& schluessel, string const& wert);
  // gibt den Namen für ein Element zurück
  string name(string const& tabelle,
              string const& schluessel, string const& wert);
  // Anzahl der Ergebnisse
  size_t ergebnisse_anzahl() const;
  // Die Ergebnisse
  vector<vector<string>> const ergebnisse() const;
  // Die Ergebnissespalten
  vector<string> const ergebnisse_spalten() const;
  // Das (einzige) Ergebnis als String
  string ergebnis_string() const;
  // Das (einzige) Ergebnis als Ganzzahl
  int ergebnis_int() const;
  // Das (einzige) Ergebnis als Fließkommazahl
  double ergebnis_double() const;
  // Die Ergebnisse als String-Feld (eine Spalte)
  vector<string> ergebnisse_spalte_strings(string const& spalte = "") const;

private:
  // Callback-Funktion für die Übernahme der Ergebnisse
  static int sql_callback(void *NotUsed,
                          int argc, char **argv, char **azColName);
  // Prüfe, dass genau ein Ergebnis vorhanden ist
  bool pruefe_auf_ein_ergebnis() const;
  // Prüfe, dass genau eine Spalte vorhanden ist
  bool pruefe_auf_eine_ergebnisspalte() const;
public:
  // Alle Ergebnisse ausgeben
  void schreibe_ergebnisse(ostream& ostr) const;

private:
  // Die Datenbank
  sqlite3* db = nullptr;

  // Die Ergebnisse der Datenbankauswertung
  vector<vector<string>> ergebnisse_;
  // Die Spalten zu den Ergebnissen der Datenbankauswertung
  vector<string> ergebnisse_spalten_;
}; // class Datenbank
} // namespace Datenbank

extern unique_ptr<Datenbank::Datenbank> db;
