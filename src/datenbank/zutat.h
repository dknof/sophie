#pragma once

#include "objekt.h"
#include "zutatenkategorie.h"

namespace Datenbank {

/** Eine Zutat
 **/
class Zutat : public Objekt {
  public:
    static Zutat neu(string const& name,
                     string const& name_mehrzahl = "");
    static vector<Zutat> liste();
    static vector<string> liste_namen();

  public:
    Zutat();
    explicit Zutat(string const& name);

    // Zugriffsfunktionen

    string name_mehrzahl() const;
    Bild bild() const;
    ZutatenKategorie kategorie() const;

    void setze_name_mehrzahl(string const& name_mehrzahl);
    void setze_bild(Bild const& bild);
    void setze_kategorie(ZutatenKategorie const& kategorie);
    void setze_kategorie(string const& kategorie);

}; // class Zutat : public Objekt

ostream& operator<<(ostream& ostr, Zutat const& zutat);

} // namespace Datenbank
