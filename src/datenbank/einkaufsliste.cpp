#include "constants.h"

#include "einkaufsliste.h"
#include "datenbank.h"
#include "einheit.h"
#include "zutat.h"
#include "einheitenumrechner.h"
#include "vorrat.h"

#include <map>
#include <iomanip>
using std::setw;

Datenbank::Einkaufsliste einkaufsliste;

namespace Datenbank {

/** Konstruktor
 **/
Einkaufsliste::Einkaufsliste() :
  tabelle("Einkaufsliste")
{ }

/** -> Rückgabe
 **
 ** @return   alle Einträge in der Einkaufsliste
 **/
ZutatenListe
Einkaufsliste::liste() const
{
  ::db->sql("SELECT Z.Name "
            "FROM " + this->tabelle + ", Zutaten AS Z "
            + "WHERE Einkaufsliste.Zutat = Z.ZutatenID "
            + "AND Einkaufsliste.Einheit IS NULL "
            + "ORDER BY Z.Name"
            + ";");

  ZutatenListe liste;
  for (auto e : ::db->ergebnisse())
    liste.hinzufuege(ZutatMenge{e});

  ::db->sql("SELECT Z.Name, E.Name, Einkaufsliste.Anzahl "
            "FROM " + this->tabelle + ", Zutaten AS Z, Einheiten AS E "
            + "WHERE Einkaufsliste.Zutat = Z.ZutatenID "
            + "AND Einkaufsliste.Einheit = E.EinheitenID "
            + "ORDER BY Z.Name, E.Name"
            + ";");

  for (auto e : ::db->ergebnisse())
    liste.hinzufuege(ZutatMenge{e});

  return liste;
} // ZutatenListe Einkaufsliste::liste() const

/** -> Rückgabe
 **
 ** @return   alle Einträge in der Einkaufsliste, die im Vorrat sind
 **/
ZutatenListe
Einkaufsliste::liste_im_vorrat() const
{
  return ::einheitenumrechner.schnitt(this->liste(), ::vorrat.liste());
} // ZutatenListe Einkaufsliste::liste_im_vorrat() const

/** -> Rückgabe
 **
 ** @return   alle Einträge in der Einkaufsliste, die nicht im Vorrat sind
 **/
ZutatenListe
Einkaufsliste::liste_nicht_im_vorrat() const
{
  return ::einheitenumrechner.differenz(this->liste(), ::vorrat.liste());
} // ZutatenListe Einkaufsliste::liste_nicht_im_vorrat() const

/** -> Rückgabe
 **
 ** @param    zutat    Zutat
 **
 ** @return   Menge der Zutat
 **/
Menge
Einkaufsliste::menge(Zutat const& zutat) const
{
  ::db->sql("SELECT Z.Name, E.Name, Einkaufsliste.Anzahl"
            "  FROM " + this->tabelle + ", Zutaten AS Z, Einheiten AS E"
            + "  WHERE Einkaufsliste.Zutat = " + zutat.id()
            + "  AND Einkaufsliste.Einheit = E.EinheitenID"
            + "  AND Z.ZutatenID = " + zutat.id()
            + "  ORDER BY Z.Name, E.Name"
            + ";");


  if (::db->ergebnisse_anzahl())
    return ZutatMenge{::db->ergebnisse()[0]}.menge;

  ::db->sql("SELECT Z.Name "
            "FROM " + this->tabelle + ", Zutaten AS Z"
            + "  WHERE Einkaufsliste.Zutat = " + zutat.id()
            + "  AND Einkaufsliste.Einheit IS NULL"
            + "  AND Z.ZutatenID = " + zutat.id()
            + "  ORDER BY Z.Name"
            + ";");

  if (::db->ergebnisse_anzahl())
    return ZutatMenge{::db->ergebnisse()[0]}.menge;

  return Menge{};
} // Menge Einkaufsliste::menge(Zutat zutat)


/** Fügt eine neue Zutat ohne Anzahl hinzu
 **
 ** @param   zutat      Zutat
 **/
void
Einkaufsliste::hinzufuege(Zutat const& zutat)
{
  // Prüfe, ob die Zutat bereits in der Einkaufsliste enthalten ist
  ::db->sql("SELECT Zutat "
            "FROM " + this->tabelle
            + "  WHERE Einkaufsliste.Zutat = " + zutat.id()
            + "  AND Einkaufsliste.Einheit IS NULL "
            + ";");
  if (::db->ergebnisse_anzahl())
    return ;
  ::db->sql("INSERT INTO " + this->tabelle + " "
            "( Zutat, Einheit, Anzahl ) "
            "VALUES (" + zutat.id() + ", NULL, NULL);");
  return ;
} // void Einkaufsliste::hinzufuege(Zutat zutat)

/** Fügt eine neue Zutat mit Menge hinzu
 ** Ist die Zutat bereits in der Liste drin, wird, wenn möglich, die Menge zusamanzahlfasst
 **
 ** @param   zutat   Zutat
 ** @param   menge   Menge
 **/
void
Einkaufsliste::hinzufuege(Zutat const& zutat, Menge const& menge)
{
  if (!menge || (menge.anzahl == 0))
    return ;
  // Suche alle Einträge mit der Zutat
  ::db->sql("SELECT Einkaufsliste.EinkaufslisteID, E.Name, Einkaufsliste.Anzahl "
            "  FROM " + this->tabelle + ", Einheiten AS E "
            + "  WHERE Einkaufsliste.Zutat = " + zutat.id()
            + "  AND Einkaufsliste.Einheit = E.EinheitenID "
            + "  AND Einkaufsliste.Einheit IS NOT NULL "
            + ";");
  for (auto const e : ::db->ergebnisse()) {
    // Versuche, die Einträge zusammenzufassen
    auto const& id = e[0];
    auto const einheit2 = Einheit(e[1]);
    auto const anzahl2 = std::stod(e[2]);
    auto menge1 = ::einheitenumrechner.umrechnen(menge, einheit2, zutat);
    if (menge1) {
      // Kann die Anzahl ändern
      ::db->sql("UPDATE " + this->tabelle + " "
                "SET Anzahl = " + std::to_string(menge1.anzahl + anzahl2) + " "
                + "WHERE Einkaufsliste.EinkaufslisteID = " + id
                + ";");
      return ;
    }
  }

  // Füge den Eintrag hinzu
  ::db->sql("INSERT INTO " + this->tabelle + " "
            "( Zutat, Einheit, Anzahl ) "
            "VALUES "
            "(" + zutat.id() + ", "
            + menge.einheit.id() + ", "
            + std::to_string(menge.anzahl)
            + " );");
  return ;
} // void Einkaufsliste::hinzufuege(Zutat zutat)

/** Eine Zutat aus der Einkaufsliste löschen
 **
 ** @param   zutat    Zutat
 **
 ** @return   ob die Zutat in der Liste enthalten war
 **/
bool
Einkaufsliste::entferne(Zutat const& zutat)
{
  // Prüfe, ob die Zutat in der Einkaufsliste enthalten ist
  ::db->sql("SELECT Zutat "
            "  FROM " + this->tabelle
            + "  WHERE Einkaufsliste.Zutat = " + zutat.id()
            + ";");
  if (!::db->ergebnisse_anzahl())
    return false;

  ::db->sql("DELETE FROM " + this->tabelle
            + "  WHERE Zutat = " + zutat.id()
            + ";");
  return true;
} // bool Einkaufsliste::entferne(Zutat zutat)

/** Entfernt die angegebene Menge der Zutat aus der Liste
 **
 ** @param   zutat    Zutat
 ** @param   menge    Menge
 **
 ** @return   ob die Zutat in der Liste enthalten war
 **/
bool
Einkaufsliste::entferne(Zutat const& zutat, Menge const& menge)
{
  if (!menge || (menge.anzahl == 0))
    return true;
  // Suche erst einmal die Zutat
  ::db->sql("SELECT Einkaufsliste.EinkaufslisteID, E.Name, Einkaufsliste.Anzahl "
            "  FROM " + this->tabelle + ", Einheiten AS E "
            + "  WHERE Einkaufsliste.Zutat = " + zutat.id()
            + "  AND Einkaufsliste.Einheit = E.EinheitenID "
            + "  AND Einkaufsliste.Einheit IS NOT NULL "
            + ";");
  for (auto const e : ::db->ergebnisse()) {
    // Versuche, die Einträge zusammenzufassen
    auto const& id = e[0];
    auto const einheit2 = Einheit(e[1]);
    auto const anzahl2 = std::stod(e[2]);
    auto menge1 = ::einheitenumrechner.umrechnen(menge, einheit2, zutat);
    if (menge1) {
      if (menge1.anzahl == anzahl2) {
        // Eintrag entfernen
        ::db->sql("DELETE FROM " + this->tabelle
                  + "  WHERE EinkaufslisteID = " + id
                  + ";");
      } else {
        // Eintrag verringern
        ::db->sql("UPDATE " + this->tabelle + " "
                  "  SET Anzahl = " + std::to_string(anzahl2 - menge1.anzahl) + " "
                  + "  WHERE EinkaufslisteID = " + id
                  + ";");
      }
      return true;
    }
  }

  return false;
} // bool Einkaufsliste::entferne(Zutat zutat)

/** Entfernt alle Einträge in der Einkaufsliste
 **/
void
Einkaufsliste::entferne_alles()
{
  ::db->sql("DELETE FROM " + this->tabelle + ";");
  return ;
} // void Einkaufsliste::entferne_alles()

} // namespace Datenbank
