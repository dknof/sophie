#pragma once

namespace Datenbank {

/** Ein Objekt, gekennzeichnet über den Namen
 ** Basisklasse für Zutat, …
 **/
class Objekt {
  protected:
    explicit Objekt(string const& tabelle);
    Objekt(string const& tabelle, string const& name);

  public:
    Objekt& operator=(Objekt const& objekt);

    explicit operator bool() const;

    string const& id() const;
    void entferne_id();

    // Zugriffsfunktionen
    string wert(string const& schluessel) const;
    string name() const;

    void setze_name(string const& name);
    void setze_wert(string const& schluessel, string const& wert);
    void setze_wert(string const& schluessel, int wert);

    void loesche();

  public:
    // Tabelle in der Datenbank
    string const tabelle;
  protected:
    // Id der Datenbank für die Referenz
    string id_ = "";
}; // class Objekt

bool operator==(Objekt const& lhs, Objekt const& rhs);
bool operator!=(Objekt const& lhs, Objekt const& rhs);
bool operator<(Objekt const& lhs, Objekt const& rhs);

} // namespace Datenbank
