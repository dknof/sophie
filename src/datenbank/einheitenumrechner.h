#pragma once

#include "../zutatenliste.h"

namespace Datenbank {

/** Umrechnung von Einheiten
 ** Formel ohne Zutat: 1 l = 1000 ml
 ** Formel mit Zutat:  1 TL Zucker = 20 g
 **/
class Einheitenumrechner {
  public:
    /** Eine Umrechnungsformel
     **/
    struct Formel {
      Formel();
      Formel(Menge const& menge1, Menge const& menge2);
      Formel(Menge const& menge1, Menge const& menge2, Zutat const& zutat);
      Formel(vector<string> const& formel);
      Menge menge1;
      Menge menge2;
      Zutat zutat;
    }; // struct Formel
  public:
    // Konstruktor
    Einheitenumrechner();

    // Zugriffsfunktionen

    // Liste aller Umrechnungen
    vector<Formel> liste() const;

    // Erstellt eine Umrechnung
    void neu(Menge const& menge1, Menge const& menge2);
    void neu(Menge const& menge1, Menge const& menge2, Zutat const& zutat);

    // lösche Umrechnungen
    void loesche(Einheit const& einheit1, Einheit const& einheit2);
    void loesche(Einheit const& einheit1);
    void loesche(Einheit const& einheit1, Einheit const& einheit2,
                 Zutat const& zutat);
    void loesche(Einheit const& einheit1,
                 Zutat const& zutat);

    // Umrechnen
    Menge umrechnen(Menge const& menge1, Einheit const& einheit2);
    Menge umrechnen(Menge const& menge1, Einheit const& einheit2,
                    Zutat const& zutat);
    Menge umrechnen(ZutatMenge const& zutat, Einheit const& einheit);
    Menge umrechnen_mit_matrix(Menge const& menge1, Einheit const& einheit2);

    // Alle Zutaten, die in beiden Listen sind
    ZutatenListe schnitt(ZutatenListe const& liste1,
                         ZutatenListe liste2);
    // liste1 zuzüglich liste2
    ZutatenListe summe(ZutatenListe const& liste1,
                       ZutatenListe const& liste2);
    // liste1 abzüglich liste2
    ZutatenListe differenz(ZutatenListe const& liste1,
                           ZutatenListe liste2);

  private:
    // Tabellenname
    string const tabelle;
}; // class Einheitenumrechner

// Umrechnungsformel ausgeben
ostream& operator<<(ostream& ostr,
                    Einheitenumrechner::Formel const& formel);

} // namespace Datenbank

extern Datenbank::Einheitenumrechner einheitenumrechner;
