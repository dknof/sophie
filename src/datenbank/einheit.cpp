#include "constants.h"
#include "einheit.h"
#include "datenbank.h"

namespace Datenbank {

/** Erstellt eine neue Einheit
 **/
Einheit
  Einheit::neu(string const& name, string const& name_mehrzahl)
  {
    if (!Einheit(name))
      ::db->sql("INSERT INTO Einheiten "
                "( Name, Mehrzahl ) "
                "VALUES "
                "( '" + name + "', "
                + (  (   !name_mehrzahl.empty()
                      && (name_mehrzahl != name))
                   ? "'" + name_mehrzahl + "'"
                   : "null"s )
                + " );");
    return Einheit(name);
  } // static Einheit Einheit::neu(string name, string name_mehrzahl = "")

/** Gibt die Liste der Einheiten zurück
 **/
vector<Einheit>
  Einheit::liste()
  {
    ::db->sql("SELECT EinheitenID "
              " FROM Einheiten "
              " ORDER BY Name"
              ";");

    auto liste = vector<Einheit>(::db->ergebnisse_anzahl());
    for (size_t i = 0; i < ::db->ergebnisse_anzahl(); ++i)
      liste[i].id_ = ::db->ergebnisse_spalte_strings()[i];
    return liste;
  } // static vector<Einheit> Einheit::liste()

/** Gibt die Liste der Einheitnamen zurück
 **/
vector<string>
  Einheit::liste_namen()
  {
    ::db->sql("SELECT Name"
              " FROM Einheiten"
              ";");
    if (::db->ergebnisse_anzahl() == 0)
      return {};
    return ::db->ergebnisse_spalte_strings();
  } // static vector<string> Einheit::liste_namen()

/** Konstruktor
 **/
Einheit::Einheit() :
  Objekt("Einheiten")
{ }

/** Konstruktor
 **
 ** @param   name   Name der Einheit
 **/
Einheit::Einheit(string const& name) :
  Objekt("Einheiten", name)
{ }

/** -> Rückgabe
 **
 ** @return   Name Mehrzahl der Einheit
 **/
string
Einheit::name_mehrzahl() const
{
  return this->wert("Mehrzahl");
} // string Einheiten::name_mehrzahl() const

/** -> Rückgabe
 **
 ** @return   Bild der Einheit
 **/
Bild
Einheit::bild() const
{
  return this->wert("Bild");
} // Bild Einheiten::bild() const

/** setzt den Namen (Mehrzahl)
 **
 ** @param    name_mehrzahl    neuer Name (Mehrzahl)
 **/
void
Einheit::setze_name_mehrzahl(string const& name_mehrzahl)
{
  this->setze_wert("Mehrzahl", name_mehrzahl);
  return ;
} // void Einheit::setze_name_mehrzahl(string name_mehrzahl)

/** setzt das Bild
 **
 ** @param    bild   neues Bild
 **/
void
Einheit::setze_bild(Bild const& bild)
{
  this->setze_wert("Bild", bild);
  return ;
} // void Einheit::setze_bild(Bild bild)

/** Gibt die Einheit aus
 **
 ** @param    ostr    Ausgabestrom
 ** @param    einheit   Einheit
 **
 ** @return   Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, Einheit const& einheit)
{
  ostr << einheit.name();
  if (!einheit.name_mehrzahl().empty())
    ostr << '/' << einheit.name_mehrzahl();
  if (!einheit.bild().empty())
    ostr << " Bild:" << einheit.bild();
  return ostr;
} // ostream& operator<<(ostream& ostr, Einheit einheit)

} // namespace Datenbank
