#pragma once

#include "objekt.h"
#include "../zutat.h"
#include "../zutatenpreisliste.h"

namespace Datenbank {

/** Eine Geschaeft
 **/
class Geschaeft : public Objekt {
  public:
    static Geschaeft neu(string const& name);
    static vector<Geschaeft> liste();
    static vector<string> liste_namen();

  public:
    Geschaeft();
    explicit Geschaeft(string const& name);

    Bild bild() const;
    void setze_bild(Bild const& bild);

    ZutatenPreisListe zutaten() const;
    bool istleer() const;

    bool enthaelt(Zutat const& zutat) const;

    void hinzufuege(Zutat const& zutat);
    void hinzufuege(Zutat const& zutat, Menge const& menge);
    void hinzufuege(Zutat const& zutat, Menge const& menge, Preis preis);

    bool entferne(Zutat const& zutat);
    void entferne_alles();

    void verschiebe_vor(Zutat const& zutat, Zutat const& referenz);
    void verschiebe_hinter(Zutat const& zutat, Zutat const& referenz);
    void verschiebe_an_anfang(Zutat const& zutat);
    void verschiebe_an_ende(Zutat const& zutat);
    void vertausche(Zutat const& zutat1, Zutat const& zutat2);
}; // class Geschaeft : public Objekt

ostream& operator<<(ostream& ostr, Geschaeft const& geschaeft);

} // namespace Datenbank
