#pragma once

#include "../zutatenliste.h"

namespace Datenbank {

/** Eine Einkaufsliste
 ** Zutat mit Menge (Einheit und Anzahl), optional ohne Menge
 ** Eine Zutat kann auch mehrfach enthalten sein, die Zusammenfassung erfolgt separat.
 ** Beispiel:
 ** 1 kg Zucker
 ** Salz
 **/
class Einkaufsliste {
  public:
    Einkaufsliste();

    // Zugriffsfunktionen

    ZutatenListe liste() const;
    ZutatenListe liste_im_vorrat() const;
    ZutatenListe liste_nicht_im_vorrat() const;

    Menge menge(Zutat const& zutat) const;

    void hinzufuege(Zutat const& zutat);
    void hinzufuege(Zutat const& zutat, Menge const& menge);

    bool entferne(Zutat const& zutat);
    bool entferne(Zutat const& zutat, Menge const& menge);
    void entferne_alles();

  private:
    // Tabellenname
    string const tabelle;
}; // class Einkaufsliste

} // namespace Datenbank

extern Datenbank::Einkaufsliste einkaufsliste;
