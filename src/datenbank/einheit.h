#pragma once

#include "objekt.h"

namespace Datenbank {

/** Eine Einheit
 **/
class Einheit : public Objekt {
  public:
    static Einheit neu(string const& name,
                       string const& name_mehrzahl = "");
    static vector<Einheit> liste();
    static vector<string> liste_namen();

  public:
    Einheit();
    explicit Einheit(string const& name);

    // Zugriffsfunktionen

    string name_mehrzahl() const;
    Bild bild() const;

    void setze_name_mehrzahl(string const& name_mehrzahl);
    void setze_bild(Bild const& bild);

  private:
}; // class Einheit : public Objekt

// Gibt die Einheit aus
ostream& operator<<(ostream& ostr, Einheit const& einheit);

} // namespace Datenbank
