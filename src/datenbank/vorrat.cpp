#include "constants.h"

#include "vorrat.h"
#include "einheit.h"
#include "zutat.h"
#include "einheitenumrechner.h"
#include "datenbank.h"

#include <map>
#include <iomanip>
using std::setw;

Datenbank::Vorrat vorrat;

namespace Datenbank {

/** Konstruktor
 **/
Vorrat::Vorrat() :
  tabelle("Vorrat")
{ }

/** -> Rückgabe
 **
 ** @return   alle Einträge im Vorrat
 **/
ZutatenListe
Vorrat::liste() const
{
  ::db->sql("SELECT Z.Name "
            "FROM " + this->tabelle + ", Zutaten AS Z "
            + "WHERE Vorrat.Zutat = Z.ZutatenID "
            + "AND Vorrat.Einheit IS NULL "
            + "ORDER BY Z.Name"
            + ";");

  ZutatenListe liste;
  for (auto e : ::db->ergebnisse())
    liste.hinzufuege(ZutatMenge{e});

  ::db->sql("SELECT Z.Name, E.Name, Vorrat.Anzahl "
            "FROM " + this->tabelle + ", Zutaten AS Z, Einheiten AS E "
            + "WHERE Vorrat.Zutat = Z.ZutatenID "
            + "AND Vorrat.Einheit = E.EinheitenID "
            + "ORDER BY Z.Name, E.Name"
            + ";");

  for (auto e : ::db->ergebnisse())
    liste.hinzufuege(ZutatMenge{e});

  return liste;
} // ZutatenListe Vorrat::liste() const

/** -> Rückgabe
 **
 ** @param    zutat    Zutat
 **
 ** @return   Menge der Zutat
 **/
Menge
Vorrat::menge(Zutat const& zutat) const
{
  ::db->sql("SELECT Z.Name, E.Name, Vorrat.Anzahl "
            "  FROM " + this->tabelle + ", Zutaten AS Z, Einheiten AS E "
            + "  WHERE Vorrat.Zutat = " + zutat.id()
            + "  AND Vorrat.Einheit = E.EinheitenID "
            + "  AND Z.ZutatenID = " + zutat.id()
            + "  ORDER BY Z.Name, E.Name"
            + ";");


  if (::db->ergebnisse_anzahl())
    return ZutatMenge{::db->ergebnisse()[0]}.menge;

  ::db->sql("SELECT Z.Name "
            "  FROM " + this->tabelle + ", Zutaten AS Z "
            + "  WHERE Vorrat.Zutat = " + zutat.id()
            + "  AND Vorrat.Einheit IS NULL "
            + "  AND Z.ZutatenID = " + zutat.id()
            + "  ORDER BY Z.Name"
            + ";");

  if (::db->ergebnisse_anzahl())
    return ZutatMenge{::db->ergebnisse()[0]}.menge;

  return Menge{};
} // Menge Vorrat::menge(zutat zutat)

/** Fügt eine neue Zutat ohne Anzahl hinzu
 **
 ** @param   zutat      Zutat
 **/
void
Vorrat::hinzufuege(Zutat const& zutat)
{
  // Prüfe, ob die Zutat bereits in der Vorrat enthalten ist
  ::db->sql("SELECT Zutat"
            "  FROM " + this->tabelle
            + "  WHERE Vorrat.Zutat = " + zutat.id()
            + "  AND Vorrat.Einheit IS NULL "
            + ";");
  if (::db->ergebnisse_anzahl())
    return ;
  ::db->sql("INSERT INTO " + this->tabelle
            + "  ( Zutat, Einheit, Anzahl ) "
            + "  VALUES (" + zutat.id() + ", NULL, NULL);");
  return ;
} // void Vorrat::hinzufuege(Zutat zutat)

/** Fügt eine neue Zutat mit Menge hinzu
 ** Ist die Zutat bereits in der Liste drin, wird, wenn möglich, die Menge zusamanzahlfasst
 **
 ** @param   zutat   Zutat
 ** @param   menge   Menge
 **/
void
Vorrat::hinzufuege(Zutat const& zutat, Menge const& menge)
{
  if (!menge || (menge.anzahl == 0))
    return ;
  // Suche alle Einträge mit der Zutat
  ::db->sql("SELECT Vorrat.VorratID, E.Name, Vorrat.Anzahl "
            "  FROM " + this->tabelle + ", Einheiten AS E "
            + "  WHERE Vorrat.Zutat = " + zutat.id()
            + "  AND Vorrat.Einheit = E.EinheitenID "
            + "  AND Vorrat.Einheit IS NOT NULL "
            + ";");
  for (auto const e : ::db->ergebnisse()) {
    // Versuche, die Einträge zusammenzufassen
    auto const& id = e[0];
    auto const einheit2 = Einheit(e[1]);
    auto const anzahl2 = std::stod(e[2]);
    auto menge1 = ::einheitenumrechner.umrechnen(menge, einheit2, zutat);
    if (menge1) {
      // Kann die Anzahl ändern
      ::db->sql("UPDATE " + this->tabelle + " "
                "SET Anzahl = " + std::to_string(menge1.anzahl + anzahl2) + " "
                + "WHERE Vorrat.VorratID = " + id
                + ";");
      return ;
    }
  }

  // Füge den Eintrag hinzu
  ::db->sql("INSERT INTO " + this->tabelle + " "
            "( Zutat, Einheit, Anzahl ) "
            "VALUES "
            "( " + zutat.id() + ", "
            + menge.einheit.id() + ", "
            + std::to_string(menge.anzahl)
            + " );");
  return ;
} // void Vorrat::hinzufuege(Zutat zutat)

/** Eine Zutat aus der Vorrat löschen
 **
 ** @param   zutat    Zutat
 **
 ** @return   ob die Zutat in der Liste enthalten war
 **/
bool
Vorrat::entferne(Zutat const& zutat)
{
  // Prüfe, ob die Zutat in der Vorrat enthalten ist
  ::db->sql("SELECT Zutat "
            "  FROM " + this->tabelle
            + "  WHERE Vorrat.Zutat = " + zutat.id()
            + ";");
  if (!::db->ergebnisse_anzahl())
    return false;

  ::db->sql("DELETE FROM " + this->tabelle
            + "  WHERE Zutat = " + zutat.id()
            + ";");
  return true;
} // bool Vorrat::entferne(Zutat zutat)

/** Entfernt die angegebene Menge der Zutat aus der Liste
 **
 ** @param   zutat    Zutat
 ** @param   menge    Menge
 **
 ** @return   ob die Zutat in der Liste enthalten war
 **/
bool
Vorrat::entferne(Zutat const& zutat, Menge const& menge)
{
  if (!menge || (menge.anzahl == 0))
    return true;
  // Suche alle Einträge mit der Zutat
  ::db->sql("SELECT Vorrat.VorratID, E.Name, Vorrat.Anzahl"
            "  FROM " + this->tabelle + ", Zutaten AS Z, Einheiten AS E"
            + "  WHERE Vorrat.Zutat = " + zutat.id()
            + "  AND Vorrat.Einheit = E.EinheitenID "
            + "  AND Vorrat.Einheit IS NOT NULL "
            + ";");
  for (auto const e : ::db->ergebnisse()) {
    // Versuche, die Einträge zusammenzufassen
    auto const& id = e[0];
    auto const einheit2 = Einheit(e[1]);
    auto const anzahl2 = std::stod(e[2]);
    auto menge1 = ::einheitenumrechner.umrechnen(menge, einheit2, zutat);
    if (menge1) {
      if (menge1.anzahl == anzahl2) {
        // Eintrag entfernen
        ::db->sql("DELETE FROM " + this->tabelle + " "
                  + "WHERE VorratID = " + id
                  + ";");
      } else {
        // Eintrag verringern
        ::db->sql("UPDATE " + this->tabelle + " "
                  "SET Anzahl = " + std::to_string(anzahl2 - menge1.anzahl) + " "
                  + "WHERE VorratID = " + id
                  + ";");
      }
      return true;
    }
  }

  return false;
} // bool Vorrat::entferne(Zutat zutat)

/** Entfernt alle Einträge in der Vorrat
 **/
void
Vorrat::entferne_alles()
{
  ::db->sql("DELETE FROM " + this->tabelle + ";");
  return ;
} // void Vorrat::entferne_alles()

} // namespace Datenbank
