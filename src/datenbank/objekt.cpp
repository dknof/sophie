#include "constants.h"
#include "objekt.h"
#include "datenbank.h"

namespace Datenbank {

/** Konstruktor
 **
 ** @param   name   Name der Objekt
 **/
Objekt::Objekt(string const& tabelle) :
  tabelle(tabelle)
{ }

/** Konstruktor
 **
 ** @param   name   Name der Objekt
 **/
Objekt::Objekt(string const& tabelle, string const& name) :
  tabelle(tabelle),
  id_(::db->id(tabelle, "Name", name))
{ }

/** -> Rückgabe
 **
 ** @param    schluessel   Feldname
 **
 ** @return   Wert für schluessel
 **/
string
Objekt::wert(string const& schluessel) const
{
  ::db->sql("SELECT " + schluessel + " FROM " + this->tabelle 
            + " WHERE " + this->tabelle + "ID = '" + this->id_ + "';");
  if (::db->ergebnisse_anzahl() == 0)
    return {};
  return ::db->ergebnis_string();
} // string Objekten::wert(string schluessel) const

/** Kopieroperator
 **/
Objekt&
Objekt::operator=(Objekt const& objekt)
{
  if (objekt.tabelle != this->tabelle) {
    throw "Die Objekte verweisen nicht auf dieselbe Tabelle";
  }

  this->id_ = objekt.id_;
  return *this;
} // Objekt& Objekt::operator=(Objekt const& objekt)

/** Gibt zurück, ob die ID gesetzt ist
 **/
Objekt::operator bool() const
{
  return !this->id_.empty();
} // Objekt::operator bool() const

/** Gibt die id zurück
 **/
string const&
Objekt::id() const
{
  return this->id_;
} // string Objekt::id() const

/** entfernt die id
 **/
void
Objekt::entferne_id()
{
  this->id_ = "";
} // void Objekt::entferne_id()

/** -> Rückgabe
 **
 ** @return   Name des Objekts
 **/
string
Objekt::name() const
{
  return this->wert("Name");
} // string Objekten::name() const

/** setzt den Namen
 **
 ** @param    name    neuer Name
 **/
void
Objekt::setze_name(string const& name)
{
  this->setze_wert("Name", name);
  return ;
} // void Objekt::setze_name(string name)

/** setzt das Feld 'schluessel' auf 'wert'
 **
 ** @param    schluessel   Feldname
 ** @param    wert         neuer Wert
 **/
void
Objekt::setze_wert(string const& schluessel, string const& wert)
{
  if (!this->id_.empty())
  ::db->sql("UPDATE " + this->tabelle
            + "  SET " + schluessel + " = '" + wert + "'"
            + "  WHERE " + this->tabelle + "ID = '" + this->id_ + "'"
            +";");
  return ;
} // void Objekt::setze_wert(string schluessel, string wert)

/** setzt das Feld 'schluessel' auf 'wert'
 **
 ** @param    schluessel   Feldname
 ** @param    wert         neuer Wert
 **/
void
Objekt::setze_wert(string const& schluessel, int const wert)
{
  ::db->sql("UPDATE " + this->tabelle + " "
            + "SET " + schluessel + " = " + to_string(wert) + " "
            + "WHERE " + this->tabelle + "ID = " + this->id_);
  return ;
} // void Objekt::setze_wert(string schluessel, int wert)

/** Lösche die Objekt aus der Datenbank
 **/
void
Objekt::loesche()
{
  ::db->sql("DELETE FROM " + this->tabelle + " "
            "WHERE " + this->tabelle + "ID = '" + this->id_ + "'");
  return ;
} // void Objekt::loesche

/** Vergleicht zwei Objekte
 **/
bool
operator==(Objekt const& lhs, Objekt const& rhs)
{
  return (   (lhs.tabelle == rhs.tabelle)
          && (lhs.id() == rhs.id()));
} // bool operator==(Objekt const& lhs, Objekt const& rhs)

/** Vergleicht zwei Objekte
 **/
bool
operator!=(Objekt const& lhs, Objekt const& rhs)
{
  return (   (lhs.tabelle != rhs.tabelle)
          || (lhs.id() != rhs.id()));
} // bool operator!=(Objekt const& lhs, Objekt const& rhs)

/** Vergleicht zwei Objekte
 **/
bool
operator<(Objekt const& lhs, Objekt const& rhs)
{
  return (   (lhs.tabelle < rhs.tabelle)
          || (   (lhs.tabelle == rhs.tabelle)
              && (lhs.id() < rhs.id())));
} // bool operator<(Objekt const& lhs, Objekt const& rhs)


} // namespace Datenbank
