#pragma once

#include "../zutatenliste.h"
#include "../zutatenliste.h"

namespace Datenbank {

/** Eine Vorrat
 ** Zutat mit Menge (Einheit und Anzahl), optional ohne Menge
 ** Eine Zutat kann auch mehrfach enthalten sein, die Zusammenfassung erfolgt separat.
 ** Beispiel:
 ** 1 kg Zucker
 ** Salz
 **/
class Vorrat {
public:
  Vorrat();

  // Zugriffsfunktionen

  ZutatenListe liste() const;
  Menge menge(Zutat const& zutat) const;

  void hinzufuege(Zutat const& zutat);
  void hinzufuege(Zutat const& zutat, Menge const& menge);

  bool entferne(Zutat const& zutat);
  bool entferne(Zutat const& zutat, Menge const& menge);
  void entferne_alles();

private:
  // Tabellenname
  string const tabelle;
}; // class Vorrat

} // namespace Datenbank

extern Datenbank::Vorrat vorrat;
