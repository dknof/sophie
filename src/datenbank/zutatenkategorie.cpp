#include "constants.h"
#include "zutatenkategorie.h"
#include "datenbank.h"

namespace Datenbank {

/** Erstellt eine neue ZutatenKategorie
 **/
ZutatenKategorie
  ZutatenKategorie::neu(string const& name)
  {
    if (!ZutatenKategorie(name))
      ::db->sql("INSERT INTO ZutatenKategorieen "
                "( Name ) "
                "VALUES "
                "( '" + name + "' "
                + " );");
    return ZutatenKategorie(name);
  } // static ZutatenKategorie ZutatenKategorie::neu(string name)

/** Gibt die ZutatenKategorie mit der ID zurück
 **/
ZutatenKategorie
  ZutatenKategorie::aus_id(string const& id)
  {
    ::db->sql("SELECT ZutatenKategorieenID "
              " FROM ZutatenKategorieen "
              " WHERE ZutatenKategorieenID = '" + id + "' "
              +";");
    auto zk = ZutatenKategorie();
    if (::db->ergebnisse_anzahl())
      zk.id_ = ::db->ergebnis_string();
    return zk;
  } // static ZutatenKategorie ZutatenKategorie::aus_id(string id)

/** Gibt die Liste der ZutatenKategorieen zurück
 **/
vector<ZutatenKategorie>
  ZutatenKategorie::liste()
  {
    ::db->sql("SELECT ZutatenKategorieeID "
              " FROM ZutatenKategorieen "
              " ORDER BY Name"
              ";");

    auto liste = vector<ZutatenKategorie>(::db->ergebnisse_anzahl());
    for (size_t i = 0; i < ::db->ergebnisse_anzahl(); ++i)
      liste[i].id_ = ::db->ergebnisse_spalte_strings()[i];
    return liste;
  } // static vector<ZutatenKategorie> ZutatenKategorieezept::liste()

/** Konstruktor
 **/
ZutatenKategorie::ZutatenKategorie() :
  Objekt("ZutatenKategorieen")
{ }

/** Konstruktor
 **
 ** @param   name   Name der ZutatenKategorie
 **/
ZutatenKategorie::ZutatenKategorie(string const& name) :
  Objekt("ZutatenKategorieen", name)
{ }

/** -> Rückgabe
 **
 ** @return   Pfad zum Bild
 **/
string
ZutatenKategorie::bild() const
{
  return this->wert("Bild");
} // string ZutatenKategorie::bild() const

/** setzt das Bild
 **
 ** @param    bild    neues Bild
 **/
void
ZutatenKategorie::setze_bild(string const& bild)
{
  this->setze_wert("Bild", bild);
  return ;
} // void ZutatenKategorie::setze_bild(string bild)

/** Gibt das ZutatenKategorie aus
 **
 ** @param    ostr               Ausgabestrom
 ** @param    zutatenkategorie   ZutatenKategorie
 **
 ** @return   Ausgabestrom
 **/
ostream&
operator<<(ostream& ostr, ZutatenKategorie const& zutatenkategorie)
{
  ostr << zutatenkategorie.name();
  if (!zutatenkategorie.bild().empty())
    ostr << " Bild:" << zutatenkategorie.bild();
  return ostr;
} // ostream& operator<<(ostream& ostr, ZutatenKategorie zutatenkategorie)

} // namespace Datenbank
