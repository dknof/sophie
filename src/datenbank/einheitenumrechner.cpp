#include "constants.h"

#include "einheitenumrechner.h"
#include "einheit.h"
#include "zutat.h"
#include "datenbank.h"

#include <map>
#include <iomanip>
using std::setw;

Datenbank::Einheitenumrechner einheitenumrechner;

namespace Datenbank {

/** Konstruktor
 **/
Einheitenumrechner::Formel::Formel()
{ }

/** Konstruktor
 **
 ** @param   menge1   erste Menge
 ** @param   menge2   zweite Menge
 **/
Einheitenumrechner::Formel::Formel(Menge const& menge1_,
                                   Menge const& menge2_) :
  menge1(menge1_),
  menge2(menge2_),
  zutat()
{ }

/** Konstruktor
 **
 ** @param   menge1   erste Menge
 ** @param   menge2   zweite Menge
 ** @param   zutat    Zutat
 **/
Einheitenumrechner::Formel::Formel(Menge const& menge1_,
                                   Menge const& menge2_,
                                   Zutat const& zutat_) :
  menge1(menge1_),
  menge2(menge2_),
  zutat(zutat_)
{ }

/** Konstruktor
 **
 ** @param   formel     Formel als Stringvector
 **/
Einheitenumrechner::Formel::Formel(vector<string> const& formel) :
  menge1(std::stod(formel.at(1)), Einheit(formel.at(0))),
  menge2(std::stod(formel.at(3)), Einheit(formel.at(2))),
  zutat(formel.at(4))
{ }

/** Konstruktor
 **
 ** @param   db        hinterliegende Datenbank
 **/
Einheitenumrechner::Einheitenumrechner() :
  tabelle{"Einheitenumrechner"}
{ }

/** -> Rückgabe
 **
 ** @return   Liste aller Formeln
 **/
vector<Einheitenumrechner::Formel>
Einheitenumrechner::liste() const
{
  ::db->sql("SELECT E1.Name, Einheitenumrechner.Anzahl1, E2.Name, Einheitenumrechner.Anzahl2, Zutat "
                "FROM " + this->tabelle + ", Einheiten AS E1, Einheiten AS E2 "
                + "WHERE E1.EinheitenID = Einheitenumrechner.Einheit1 "
                + "AND E2.EinheitenID = Einheitenumrechner.Einheit2 "
                + "AND Zutat IS NULL "
                + "ORDER BY E1.Name, E2.Name "
                ";");

  vector<Formel> formeln;
  formeln.reserve(::db->ergebnisse().size());
  for (auto f : ::db->ergebnisse())
    formeln.emplace_back(Formel(f));

  ::db->sql("SELECT E1.Name, Einheitenumrechner.Anzahl1, E2.Name, Einheitenumrechner.Anzahl2, Zutaten.Name "
            "FROM " + this->tabelle + ", Einheiten AS E1, Einheiten AS E2, Zutaten "
            + "WHERE E1.EinheitenID = Einheitenumrechner.Einheit1 "
            + "AND E2.EinheitenID = Einheitenumrechner.Einheit2 "
            + "AND Zutaten.ZutatenID = Einheitenumrechner.Zutat "
            + "ORDER BY Zutaten.Name, E1.Name, E2.Name "
            ";");
  formeln.reserve(formeln.size() + ::db->ergebnisse_anzahl());
  for (auto f : ::db->ergebnisse())
    formeln.emplace_back(Formel(f));

  return formeln;
} // vector<Formel> Einheitenumrechner::liste() const

/** Fügt einen neuen Umrechner ein
 **
 ** @param   menge1   erste Menge
 ** @param   menge2   zweite Menge
 **/
void
Einheitenumrechner::neu(Menge const& menge1, Menge const& menge2)
{
  ::db->sql("INSERT INTO " + this->tabelle + " "
            "( Einheit1, Anzahl1, Einheit2, Anzahl2, Zutat ) "
            "VALUES "
            "( " + menge1.einheit.id() + ", " + std::to_string(menge1.anzahl) + ", "
            "  " + menge2.einheit.id() + ", " + std::to_string(menge2.anzahl) + ", "
            + "NULL"
            + " );");
  return ;
} // void Einheitenumrechner::neu(Menge menge1, Menge menge2)

/** Fügt einen neuen Umrechner ein
 **
 ** @param   menge1   erste Menge
 ** @param   menge2   zweite Menge
 ** @param   zutat    Zutat
 **/
void
Einheitenumrechner::neu(Menge const& menge1, Menge const& menge2,
                        Zutat const& zutat)
{
  ::db->sql("INSERT INTO " + this->tabelle + " "
            "( Einheit1, Anzahl1, Einheit2, Anzahl2, Zutat ) "
            "VALUES "
            "( " + menge1.einheit.id() + ", " + std::to_string(menge1.anzahl) + ", "
            "  " + menge2.einheit.id() + ", " + std::to_string(menge2.anzahl) + ", "
            "  '" + zutat.id() + "'"
            + " );");
  return ;
} // void Einheitenumrechner::neu(Menge menge1, Menge menge2, Zutat zutat)

/** Eine Formel löschen
 **
 ** @param   einheit1   erste Einheit
 ** @param   einheit2   zweite Einheit
 **/
void
Einheitenumrechner::loesche(Einheit const& einheit1, Einheit const& einheit2)
{
  ::db->sql("DELETE FROM " + this->tabelle + " "
            "WHERE Einheit1 = '" + einheit1.id() + "'"
            "AND   Einheit2 = '" + einheit2.id() + "'"
            "AND   Zutat = NULL");
  return ;
} // void Einheitenumrechner::loesche(Einheit einheit1, Einheit einheit2)

/** Eine Formel löschen
 **
 ** @param   einheit1   erste Einheit
 **/
void
Einheitenumrechner::loesche(Einheit const& einheit1)
{
  ::db->sql("DELETE FROM " + this->tabelle + " "
            "WHERE Einheit1 = '" + einheit1.id() + "'"
            "AND   Zutat = NULL");
  return ;
} // void Einheitenumrechner::loesche(Einheit einheit1)

/** Eine Formel löschen
 **
 ** @param   einheit1   erste Einheit
 ** @param   einheit2   zweite Einheit
 ** @param   zutat      Zutat
 **/
void
Einheitenumrechner::loesche(Einheit const& einheit1, 
                            Einheit const& einheit2,
                            Zutat const& zutat)
{
  ::db->sql("DELETE FROM " + this->tabelle + " "
            "WHERE Einheit1 = '" + einheit1.id() + "'"
            "AND   Einheit2 = '" + einheit2.id() + "'"
            "AND   Zutat = '" + zutat.id() + "'");
  return ;
} // void Einheitenumrechner::loesche(Einheit einheit1, Einheit einheit2, Zutat zutat)

/** Eine Formel löschen
 **
 ** @param   einheit1   erste Einheit
 ** @param   zutat      Zutat
 **/
void
Einheitenumrechner::loesche(Einheit const& einheit1,
                            Zutat const& zutat)
{
  ::db->sql("DELETE FROM " + this->tabelle + " "
            "WHERE Einheit1 = '" + einheit1.id() + "'"
            "AND   Zutat = '" + zutat.id() + "'");
  return ;
} // void Einheitenumrechner::loesche(Einheit einheit1, Zutat zutat)

/** Eine Einheit in eine andere umrechnen
 **
 ** @param   menge1     erste Menge
 ** @param   einheit2   Zieleinheit
 **
 ** @return   Menge, so dass gilt anzahl1 einheit1 = Anzahl einheit2
 **/
Menge
Einheitenumrechner::umrechnen(Menge const& menge1,
                              Einheit const& einheit2)
{
  // direktes Umrechnen suchen
  ::db->sql("SELECT Anzahl1, Anzahl2 "
            "  FROM " + this->tabelle
            + "  WHERE Einheit1 = " + menge1.einheit.id()
            + "  AND Einheit2 = " + einheit2.id()
            + "  AND Zutat IS NULL "
            + ";");
  if (::db->ergebnisse_anzahl()) {
    return Menge(menge1.anzahl
                 / std::stod(::db->ergebnisse()[0][0])
                 * std::stod(::db->ergebnisse()[0][1]),
                 einheit2);
  }
  ::db->sql("SELECT Anzahl1, Anzahl2 "
            "FROM " + this->tabelle
            + "  WHERE Einheit1 = " + einheit2.id()
            + "  AND Einheit2 = " + menge1.einheit.id()
            + "  AND Zutat IS NULL "
            + ";");
  if (::db->ergebnisse_anzahl())
    return Menge(menge1.anzahl
                 / std::stod(::db->ergebnisse()[0][1])
                 * std::stod(::db->ergebnisse()[0][0]),
                 einheit2);

  // Nun alle Einheiten durchgehen und Ergebnis suchen
  ::db->sql("SELECT Einheit1, Einheit2, Anzahl1, Anzahl2 "
            "  FROM " + this->tabelle
            + "  WHERE Zutat IS NULL "
            + ";");
  return this->umrechnen_mit_matrix(menge1, einheit2);
} // Menge Einheitenumrechner::umrechnen(Menge menge1, Einheit einheit2)


/** Eine Einheit in eine andere umrechnen
 **
 ** @param   menge1     Quellmenge
 ** @param   einheit2   Zieleinheit
 ** @param   zutat      Zutat
 **
 ** @return   Menge, so dass gilt menge1 zutat = Anzahl einheit2 (0 bei Fehler)
 **/
Menge
Einheitenumrechner::umrechnen(Menge const& menge1,
                              Einheit const& einheit2, Zutat const& zutat)
{
  // Ohne Spezialisierung versuchen
  auto m = this->umrechnen(menge1, einheit2);
  if (m)
    return m;
  // direktes Umrechnen suchen
  ::db->sql("SELECT Anzahl1, Anzahl2 "
            "  FROM " + this->tabelle
            + "  WHERE Einheit1 = " + menge1.einheit.id()
            + "  AND Einheit2 = " + einheit2.id()
            + "  AND Zutat = " + zutat.id()
            + ";");
  if (::db->ergebnisse_anzahl()) {
    return Menge(menge1.anzahl
                 / std::stod(::db->ergebnisse()[0][0])
                 * std::stod(::db->ergebnisse()[0][1]),
                 einheit2);
  }
  ::db->sql("SELECT Anzahl1, Anzahl2 "
            "  FROM " + this->tabelle
            + "  WHERE Einheit1 = " + einheit2.id()
            + "  AND Einheit2 = " + menge1.einheit.id()
            + "  AND Zutat = " + zutat.id()
            + ";");
  if (::db->ergebnisse_anzahl())
    return Menge(menge1.anzahl
                 / std::stod(::db->ergebnisse()[0][1])
                 * std::stod(::db->ergebnisse()[0][0]),
                 einheit2);

  // Nun alle Einheiten durchgehen und Ergebnis suchen
  ::db->sql("SELECT Einheit1, Einheit2, Anzahl1, Anzahl2 "
            "  FROM " + this->tabelle
            + "  WHERE (   Zutat = " + zutat.id()
            + "         OR Zutat IS NULL ) "
            + ";");
  return this->umrechnen_mit_matrix(menge1, einheit2);
} // Menge Einheitenumrechner::umrechnen(Menge menge1, Einheit einheit2, Zutet zutat)

/** Eine Einheit in eine andere umrechnen
 ** 
 ** @param    zutat      Zutat mit Menge
 ** @param    einheit    Einheit für die Umrechnung
 **
 ** @return   Menge von zutat in der Einheit einheit
 **/
Menge
Einheitenumrechner::umrechnen(ZutatMenge const& zutat, Einheit const& einheit)
{
  return this->umrechnen(zutat.menge, einheit, zutat.zutat);
} // Menge Einheitenumrechner::umrechnen(ZutatMenge zutat, Einheit einheit);


/** Eine Einheit in eine andere umrechnen, erstelle dafür eine Matrix und löse sie
 ** in db->ergebnisse sind alle Umrechnungen enthalten
 **
 ** @param    menge1     Quellmenge
 ** @param    einheit2   Zieleinheit
 **
 ** @return   Menge, so dass gilt anzahl1 einheit1 = Anzahl einheit2 (0 bei Fehler)
 **/
Menge
Einheitenumrechner::umrechnen_mit_matrix(Menge const& menge1,
                                         Einheit const& einheit2)
{
  // Ein Ergebnis ist: Einheit1, Einheit2, Anzahl1, Anzahl2
#if 0
  ::db->schreibe_ergebnisse(cout);
  cout << endl;
#endif

  // alle Ergebnisse auf einheit1 und einheit2 durchsuchen und die Einheiten durchnummerieren
  std::map<string, size_t> einheiten;
  std::vector<string> einheiten_v;
  size_t n = 0;
  for (auto e : ::db->ergebnisse()) {
    if (einheiten.find(e[0]) == end(einheiten)) {
      einheiten[e[0]] = n;
      einheiten_v.emplace_back(e[0]);
      n += 1;
    }
    if (einheiten.find(e[1]) == end(einheiten)) {
      einheiten[e[1]] = n;
      einheiten_v.emplace_back(e[1]);
      n += 1;
    }
  }
  if (   (einheiten.find(menge1.einheit.id()) == end(einheiten))
      || (einheiten.find(einheit2.id()) == end(einheiten)) )
    return Menge();

  vector<vector<double>> matrix(n, vector<double>(n));
  for (size_t i = 0; i < n; ++i)
    matrix[i][i] = 1;
  for (auto e : ::db->ergebnisse()) {
    matrix[einheiten[e[0]]][einheiten[e[1]]]
      = std::stod(e[3]) / std::stod(e[2]);
    matrix[einheiten[e[1]]][einheiten[e[0]]]
      = std::stod(e[2]) / std::stod(e[3]);
  }

  // Ob ein neues Ergebnis berechnet wurde
  bool neues_ergebnis = false;
  do {
    neues_ergebnis = false;
    for (size_t i = 0; i+2 < n; ++i)
      for (size_t j = i+1; j+1 < n; ++j)
        for (size_t k = j+1; k < n; ++k) {
          if (   (matrix[i][j] != 0)
              && (matrix[j][k] != 0)
              && (matrix[i][k] == 0) ) {
            matrix[i][k] = matrix[i][j] * matrix[j][k];
            matrix[k][i] = 1.0 / matrix[i][k];
            neues_ergebnis = true;
          }
          if (   (matrix[i][j] != 0)
              && (matrix[j][k] == 0)
              && (matrix[i][k] != 0) ) {
            matrix[j][k] = matrix[j][i] * matrix[i][k];
            matrix[k][j] = 1.0 / matrix[j][k];
            neues_ergebnis = true;
          }
          if (   (matrix[i][j] == 0)
              && (matrix[j][k] != 0)
              && (matrix[i][k] != 0) ) {
            matrix[i][j] = matrix[i][k] * matrix[k][j];
            matrix[j][i] = 1.0 / matrix[i][j];
            neues_ergebnis = true;
          }
        }
  } while (neues_ergebnis);

#if 0
  CLOG << einheit2 << endl;
  // Matrix ausgeben
  cout << setw(10) << "";
  for (size_t j = 0; j < n; ++j)
    cout  << "|" << setw(10) << einheiten_v[j];
  cout << '\n';
  cout << "----------";
  for (size_t j = 0; j < n; ++j)
    cout  << "+" << setw(10) << "----------";
  cout << '\n';
  for (size_t i = 0; i < n; ++i) {
    cout  << setw(10) << einheiten_v[i];
    for (size_t j = 0; j < n; ++j) {
      cout  << "|" << setw(10) << matrix[i][j];
    }
    cout << '\n';
  }
  cout << endl;
#endif

  if (matrix[einheiten[menge1.einheit.id()]][einheiten[einheit2.id()]] == 0)
    return Menge();

  return Menge(menge1.anzahl * matrix[einheiten[menge1.einheit.id()]][einheiten[einheit2.id()]],
               einheit2);
} // Menge Einheitenumrechner::umrechnen_mit_matrix(Menge menge1, Einheit einheit2)

/** -> Rückgabe
 **
 ** @param    liste1   erste Liste
 ** @param    liste2   zweite Liste
 **
 ** @return   Alle Zutaten, die in beiden Listen sind
 **
 ** @todo     Fall prüfen, in dem eine Zutat mehrfach in der Liste ist aber die Mengen nicht ineinander umgerechnet werden können.
 **/
ZutatenListe
Einheitenumrechner::schnitt(ZutatenListe const& liste1,
                            ZutatenListe liste2)
{
  ZutatenListe liste;
  for (auto z1 : liste1) {
    for (auto& z2 : liste2) {
      if (z1.zutat == z2.zutat) {
        if (!z1.menge && !z2.menge) {
          liste.hinzufuege(z1);
          break;
        } else if (z1.menge && !z2.menge) {
          liste.hinzufuege(z1);
          break;
        } else if (!z1.menge && z2.menge) {
          liste.hinzufuege(z1);
        } else { // if (z1.menge && z2.menge)
          if (z2.menge.anzahl == 0)
            continue;
          auto const menge2 = this->umrechnen(z1, z2.menge.einheit);
          if (!menge2) // Keine Umrechnung gefunden
            continue;
          if (menge2.anzahl <= z2.menge.anzahl) {
            // Alles in z2 gefunden
            z2.menge.anzahl -= menge2.anzahl;
            liste.hinzufuege(z1);
            break;
          } else {
            // Nur einen Teil in z2 enthaltenen
            auto const menge1 = this->umrechnen(z2, z1.menge.einheit);
            // Den enthaltenen Teil der Liste hinzufügen
            liste.hinzufuege(ZutatMenge(z1.zutat, menge1));
            // z1 um die Menge z2 verringern
            z1.menge = Menge(z1.menge.anzahl - menge1.anzahl,
                             menge1.einheit);
            // z2 auf 0 setzen
            z2.menge.anzahl = 0;
          }
        }
      } // if (z1.zutat == z2.zutat)
    } // for (auto z2 : liste2)
  } // for (auto z1 : liste1)

  return liste;
} // ZutatenListe Einheitenumrechner::schnitt(ZutatenListe liste1, ZutatenListe liste2)

/** -> Rückgabe
 **
 ** @param    liste1   erste Liste
 ** @param    liste2   zweite Liste
 **
 ** @return   Alle Zutaten, die in der ersten Liste ohne die zweite Liste enthalten sind
 **
 ** @todo     Fall prüfen, in dem eine Zutat mehrfach in der Liste ist aber die Mengen nicht ineinander umgerechnet werden können.
 **/
ZutatenListe
Einheitenumrechner::differenz(ZutatenListe const& liste1,
                              ZutatenListe liste2)
{
  ZutatenListe liste;
  for (auto z1 : liste1) {
    for (auto& z2 : liste2) {
      if (z1.zutat == z2.zutat) {
        if (!z1.menge && !z2.menge) {
          z1.zutat.entferne_id();
          break;
        } else if (z1.menge && !z2.menge) {
          z1.zutat.entferne_id();
          break;
        } else if (!z1.menge && z2.menge) {
          z1.zutat.entferne_id();
          break;
        } else { // if (z1.menge && z2.menge)
          if (z2.menge.anzahl == 0)
            continue;
          auto const menge1 = this->umrechnen(z2, z1.menge.einheit);
          if (!menge1) // Keine Umrechnung gefunden
            continue;
          if (menge1.anzahl >= z1.menge.anzahl) {
            // Alles in z2 gefunden
            auto const menge2 = this->umrechnen(z1, z2.menge.einheit);
            z2.menge.anzahl -= menge2.anzahl;
            z1.zutat.entferne_id();
            break;
          } else {
            // Nur ein Teil von z1 ist in z2 enthalten
            // z1 um die Menge in z2 verringern
            z1.menge = Menge(z1.menge.anzahl - menge1.anzahl,
                             menge1.einheit);
            // z2 auf 0 setzen
            z2.menge.anzahl = 0;
          }
        }
      } // if (z1.zutat == z2.zutat)
    } // for (auto z2 : liste2)
    if (z1.zutat)
      liste.hinzufuege(z1);
  } // for (auto z1 : liste1)

  return liste;
} // ZutatenListe Einheitenumrechner::differenz(ZutatenListe liste1, ZutatenListe liste2)

/** Umrechnungsformel ausgeben
 **
 ** @param   ostr     Ausgabestrom
 ** @param   formel   Formel
 **/
ostream&
operator<<(ostream& ostr,
           Einheitenumrechner::Formel const& formel)
{
  ostr << formel.menge1;
  if (formel.zutat)
    ostr << ' ' << formel.zutat;
  ostr << " = " << formel.menge2;
  return ostr;
} // ostream& operator<<(ostream& ostr, Einheitenumrechner::Formel const& formel)

} // namespace Datenbank
