#include "constants.h"

#include "datenbank.h"

#include <sqlite3.h>
#include <iomanip>
#include <fstream>

// Die einzelnen Zugriffe
#include "rezept.h"
#include "zutat.h"
#include "zutatenkategorie.h"
#include "einheit.h"
#include "einheitenumrechner.h"
#include "einkaufsliste.h"
#include "vorrat.h"
#include "geschaeft.h"

#include "../einstellung.h"
#include "../ui/ui.h"
#include "../ui/text/text.h"

namespace Datenbank {

/** Ergebnisfunktion für die SQL-Abfrage
 ** Eine Zeile
 **
 ** @param   NotUsed     Unbenutzt
 ** @param   argc        Anzahl der Ergebnisse
 ** @param   argv        Feld mit den Werten
 ** @param   azColName   Feld mit den Spaltennamen
 **
 ** @return  0
 **/
int
  sql_callback(void *NotUsed, int argc, char **argv, char **azColName)
  {
    // Im ersten Durchlauf die Spaltennamen speichern
    if (::db->ergebnisse_spalten_.empty()) {
      ::db->ergebnisse_.reserve(argc);
      for (int i = 0; i < argc; i++) {
        ::db->ergebnisse_spalten_.emplace_back(azColName[i]);
      }
    }
    ::db->ergebnisse_.emplace_back(vector<string>(0));
    ::db->ergebnisse_.back().reserve(argc);
    for (int i = 0; i < argc; i++)
      ::db->ergebnisse_.back().emplace_back(argv[i] == nullptr ? "" : argv[i]);
    return 0;
  } // int static sql_callback(void *NotUsed, int argc, char **argv, char **azColName)

/** Konstruktor
 **/
Datenbank::Datenbank()
{
  this->oeffnen();
} // Datenbank::Datenbank()

/** Destruktor
 **/
Datenbank::~Datenbank()
{
  sqlite3_close(this->db);
} // Datenbank::~Datenbank()

/** Überprüft die Datenbank
 **
 ** @return   Ob die Datenbank konsistent ist
 **
 ** @todo   alles
 **/
bool
Datenbank::konsistent() const
{
  return true;
} // bool Datenbank::konsistent() const


/** Öffnet die Datenbank
 **
 ** @todo   alles
 **/
void
Datenbank::oeffnen()
{
  auto const rc = sqlite3_open(":memory:", &this->db);
  if (rc) {
    ::ui->fehler("Konnte die Datenbank ':memory:' nicht öffnen:\n  "s + sqlite3_errmsg(db));
    sqlite3_close(this->db);
    this->db = nullptr;
    return ;
  }
  // Gegebenenfalls die Datenbank initialisieren
  this->initialisieren();

  return ;
} // void Datenbank::oeffnen()

/** Initialisiert die Datenbank
 **
 ** @todo   alles
 **/
void
Datenbank::initialisieren()
{
  this->sql("CREATE TABLE Metainformationen ("
            "  Schlüssel TEXT NOT NULL, "
            "  Wert      TEXT"
            ");");
  this->sql("INSERT INTO Metainformationen "
            "(Schlüssel, Wert) "
            "VALUES "
            "('Version', '0.1');");  

  this->sql("CREATE TABLE Rezepte ("
            "  RezepteID INTEGER NOT NULL PRIMARY KEY, "
            "  Name      TEXT    NOT NULL UNIQUE, "
            "  Bild      TEXT, "
            "  Kategorie TEXT"
            ");");

  this->sql("CREATE TABLE Zutaten ("
            "  ZutatenID INTEGER NOT NULL PRIMARY KEY, "
            "  Name      TEXT    NOT NULL UNIQUE, "
            "  Mehrzahl  TEXT, "
            "  Bild      TEXT, "
            "  Kategorie INTEGER          REFERENCES ZutatenKategorieen(ZutatenKategorieenID)"
            ");");

  this->sql("CREATE TABLE ZutatenKategorieen ("
            "  ZutatenKategorieenID INTEGER NOT NULL PRIMARY KEY, "
            "  Name      TEXT    NOT NULL UNIQUE, "
            "  Bild      TEXT"
            ");");

  this->sql("CREATE TABLE Einheiten ("
            "  EinheitenID INTEGER NOT NULL PRIMARY KEY, "
            "  Name        TEXT    NOT NULL UNIQUE, "
            "  Mehrzahl    TEXT, "
            "  Bild        TEXT"
            ");");

  this->sql("CREATE TABLE Einheitenumrechner ("
            "  EinheitenumrechnerID INTEGER NOT NULL PRIMARY KEY, "
            "  Einheit1    INTEGER NOT NULL REFERENCES Einheiten(EinheitenID), "
            "  Anzahl1     DOUBLE  NOT NULL, "
            "  Einheit2    INTEGER NOT NULL REFERENCES Einheiten(EinheitenID), "
            "  Anzahl2     DOUBLE  NOT NULL, "
            "  Zutat       INTEGER          REFERENCES Zutaten(ZutatenID)"
            ");");

  this->sql("CREATE TABLE Einkaufsliste ("
            "  EinkaufslisteID INTEGER NOT NULL PRIMARY KEY, "
            "  Zutat       INTEGER NOT NULL REFERENCES Zutaten(ZutatenID), "
            "  Einheit     INTEGER          REFERENCES Einheiten(EinheitenID), "
            "  Anzahl      DOUBLE"
            ");");

  this->sql("CREATE TABLE Vorrat ("
            "  VorratID    INTEGER NOT NULL PRIMARY KEY, "
            "  Zutat       INTEGER NOT NULL REFERENCES Zutaten(ZutatenID), "
            "  Einheit     INTEGER          REFERENCES Einheiten(EinheitenID), "
            "  Anzahl      DOUBLE"
            ");");

  this->sql("CREATE TABLE Geschäfte ("
            "  GeschäfteID INTEGER NOT NULL PRIMARY KEY, "
            "  Name        TEXT    NOT NULL UNIQUE, "
            "  Bild        TEXT"
            ");");

  this->sql("CREATE TABLE GeschäfteZutaten ("
            "  Geschäft    INTEGER          REFERENCES Geschäfte(GeschäfteID), "
            "  Position    INTEGER NOT NULL, "
            "  Zutat       INTEGER NOT NULL REFERENCES Zutaten(ZutatenID), "
            "  Einheit     INTEGER          REFERENCES Einheiten(EinheitenID), "
            "  Anzahl      DOUBLE, "
            "  Preis       DOUBLE"
            ");");

  return ;
} // void Datenbank::initialisieren()

/** Liest die Datenbank
 ** Es wird keine Datenbank gelesen sondern die Eingabe entsprechend der ui text
 **
 ** @param   istr   Eingabestrom
 **/
void
Datenbank::lese(istream& istr)
{
  auto ui = make_unique<UI_Text>(istr, cout);
  ui->initialisiere();
  ui->starte();

  return ;
} // void Datenbank::lese(istream& istr)

/** Lädt die Datenbank
 ** Es wird keine Datenbank geladen sondern die Eingabe entsprechend der ui text
 **/
void
Datenbank::lade()
{
  this->lade(::einstellung(Einstellung::DATEIPFAD));
  return ;
} // void Datenbank::lade()

/** Lädt die Datenbank
 ** Es wird keine Datenbank geladen sondern die Eingabe entsprechend der ui text
 **/
void
Datenbank::lade(string const& datei)
{
  if (datei.empty() || (datei == ":memory:"))
    return ;

  std::ifstream istr(datei.c_str());

  if (istr.fail()) {
    ::ui->fehler("Datenbank::lade(" + datei + "):\n"
                 + "  Fehler beim Öffnen der Datei");
    SEGFAULT;
    return ;
  }

  this->lese(istr);
  return ;
} // void Datenbank::lade(string datei)

/** Schreibt die Daten
 ** Es wird keine Datenbank gespeichert sondern die Eingabe entsprechend der ui text
 **
 ** @param    ostr   Ausgabestrom
 **/
void
Datenbank::schreibe(ostream& ostr)
{
  ostr << "/Rezepte\n";
  for (auto const& i : Rezept::liste())
    ostr << '+' << i << '\n';
  ostr << "\n";

  ostr << "/Kategorieen\n";
  ostr << "\n";

  ostr << "/Zutaten\n";
  for (auto const& i : Zutat::liste())
    ostr << '+' << i << '\n';
  ostr << "\n";

  ostr << "/Einheiten\n";
  for (auto const& i : Einheit::liste())
    ostr << '+' << i << '\n';
  ostr << "\n";

  ostr << "/Einheitenumrechner\n";
  for (auto const& i : einheitenumrechner.liste())
    ostr << '+' << i << '\n';
  ostr << "\n";

  ostr << "/Einkaufsliste\n";
  for (auto const& i : einkaufsliste.liste())
    ostr << '+' << i << '\n';
  ostr << "\n";

  ostr << "/Vorrat\n";
  for (auto const& i : vorrat.liste())
    ostr << '+' << i << '\n';
  ostr << "\n";

  ostr << "/Geschäfte\n";
  for (auto const& i : Geschaeft::liste())
    ostr << '+' << i << '\n';
  ostr << "\n";

  return ;
} // void Datenbank::schreibe(ostream& ostr)

/** Speichert die Daten
 ** Es wird keine Datenbank gespeichert sondern die Eingabe entsprechend der ui text
 **
 ** @return   ob das Speichern erfolgreich war
 **/
bool
Datenbank::speicher()
{
  return this->speicher(::einstellung(Einstellung::DATEIPFAD));
} // bool Datenbank::speicher()

/** Speichert die Daten
 ** Es wird keine Datenbank gespeichert sondern die Eingabe entsprechend der ui text
 **
 ** @param    datei   Pfad zur Datei
 **
 ** @return   ob das Speichern erfolgreich war
 **/
bool
Datenbank::speicher(string const& datei)
{
  if (datei.empty())
    return false;

  auto const datei_tmp = datei + ".tmp";
  std::ofstream ostr(datei_tmp.c_str());
  if (!ostr.good()) {
    ::ui->fehler("Fehler beim Speichern der Datenbank, konnte die temporäre Datei '" + datei_tmp + "' nicht erstellen.");
    return false;
  }

  ostr << "# Datenbank für sophie (Version " << ::version << ")\n\n";
  this->schreibe(ostr);

  if (!ostr.good()) {
    ::ui->fehler("Fehler beim Speichern der Datenbank, siehe temporäre Datei '" + datei_tmp + "'");
    return false;
  }
  ostr.close();

  if (rename(datei_tmp.c_str(), datei.c_str())) {
    ::ui->fehler("Fehler beim Umbenennen der Datenbank '" + datei_tmp + "' in '" + datei + "'.");
    return false;
  }

  return true;
} // bool Datenbank::speicher(string datei)

/** Speichert die Daten in einer sqlite3-Datenbankdatei
 **
 ** @param    datei   Pfad zur Datei
 **
 ** @return   ob das Speichern erfolgreich war
 **/
bool
Datenbank::speicher_sqlite3(string const& datei)
{
  if (datei.empty())
    return false;

  sqlite3 *db_datei = nullptr;
  auto rc = sqlite3_open(datei.c_str(), &db_datei);
  if (rc != SQLITE_OK)
    return false;

  auto backup = sqlite3_backup_init(db_datei, "main", this->db, "main");
  if (!backup)
    return false;
  sqlite3_backup_step(backup, -1);
  sqlite3_backup_finish(backup);
  rc = sqlite3_errcode(db_datei);
  return (rc == SQLITE_OK);
} // bool Datenbank::speicher_sqlite3(string const& datei)

/** Einen Befehl an die sql-Datenbank geben
 **
 ** @param    Befehl
 **
 ** @return   Ob die Ausführung erfolgreich war
 **/
bool
Datenbank::sql(string const& befehl)
{
  //CLOG << befehl << '\n';
  char* fehlernachricht = nullptr;
  this->ergebnisse_.clear();
  this->ergebnisse_spalten_.clear();
  auto const rc = sqlite3_exec(this->db, befehl.c_str(), 
                               &::Datenbank::sql_callback, nullptr,
                               &fehlernachricht);
  if (rc != SQLITE_OK) {
    ::ui->fehler("SQL Fehler: "s + fehlernachricht + "\nBefehl: " + befehl);
    sqlite3_free(fehlernachricht);
    return false;
  }

  return true;
} // bool Datenbank::sql(string befehl)

/** -> Rückgabe
 **
 ** @param    tabelle      Tabelle
 ** @param    schluessel   Schlüssel
 ** @param    wert         Wert
 **
 ** @return    Identifikationsnummer für das des Element aus 'tabelle' mit 'schluessel' = 'wert'
 **/
string
Datenbank::id(string const& tabelle,
              string const& schluessel, string const& wert)
{
  this->sql("SELECT " + tabelle + "ID FROM " + tabelle
            + " WHERE " + schluessel + " = '" + wert + "';");
  if (this->ergebnisse_anzahl() == 0)
    return "";
  return this->ergebnis_string();
} // string Datenbank::id(string tabelle, string schluessel, string wert)

/** -> Rückgabe
 **
 ** @param    tabelle      Tabelle
 ** @param    schluessel   Schlüssel
 ** @param    wert         Wert
 **
 ** @return    Identifikationsnummer für das des Element aus 'tabelle' mit 'schluessel' = 'wert'
 **/
string
Datenbank::name(string const& tabelle,
              string const& schluessel, string const& wert)
{
  this->sql("SELECT Name FROM " + tabelle
            + " WHERE " + schluessel + " = '" + wert + "';");
  if (this->ergebnisse_anzahl() == 0)
    return "";
  return this->ergebnis_string();
} // string Datenbank::name(string tabelle, string schluessel, string wert)

/** -> Rückgabe
 **
 ** @return    Anzahl der Ergebnisse
 **/
size_t
Datenbank::ergebnisse_anzahl() const
{ return this->ergebnisse_.size(); }

/** -> Rückgabe
 **
 ** @return    die Ergebnisse
 **/
vector<vector<string>> const
Datenbank::ergebnisse() const
{ return this->ergebnisse_; }

/** -> Rückgabe
 **
 ** @return    die Spalten zu den Ergebnissen
 **/
vector<string> const
Datenbank::ergebnisse_spalten() const
{ return this->ergebnisse_spalten_; }


/** -> Rückgabe
 **
 ** @return    Das (einzige) Ergebnis als String
 **/
string
Datenbank::ergebnis_string() const
{
  this->pruefe_auf_ein_ergebnis(); 
  return this->ergebnisse_[0][0];
} // string Datenbank::ergebnis_string() const

/** -> Rückgabe
 **
 ** @return    Das (einzige) Ergebnis als Ganzzahl
 **/
// Das (einzige) Ergebnis als Ganzzahl
int
Datenbank::ergebnis_int() const
{
  return std::stoi(this->ergebnis_string());
} // int Datenbank::ergebnis_int() const

/** -> Rückgabe
 **
 ** @return    Das (einzige) Ergebnis als Fließkommazahl
 **/
double
Datenbank::ergebnis_double() const
{
  return std::stod(this->ergebnis_string());
} // double Datenbank::ergebnis_double() const

/** -> Rückgabe
 **
 ** @param     spalte   Name der auszugebende Spalte (Vorgabe: "")
 **
 ** @return    die Ergebnisse
 **/
vector<string>
Datenbank::ergebnisse_spalte_strings(string const& spalte) const
{
  size_t n = 0;
  if (spalte.empty()) {
    this->pruefe_auf_eine_ergebnisspalte();
  } else {
    for (auto& s : this->ergebnisse_spalten()) {
      if (spalte == s)
        break;
      ++n;
    }
    if (n == this->ergebnisse_spalten().size())
      return vector<string>();
  }
  vector<string> ergebnisse;
  ergebnisse.reserve(this->ergebnisse_.size());
  for (auto e : this->ergebnisse_)
    ergebnisse.emplace_back(e[n]);
  return ergebnisse;
} // vector<string> Datenbank::ergebnisse_spalte_strings() const

/** Prüft, dass genau ein Ergebnis vorhanden ist
 **
 ** @return   Ob genau ein Ergebnis vorhanden ist
 **/
bool
Datenbank::pruefe_auf_ein_ergebnis() const
{
#ifdef DEBUG
  if (this->ergebnisse_anzahl() == 0) {
    cerr << "Datenbank: Ein Ergebnis erwartet, Ergebnisse sind aber leer\n";
    SEGFAULT;
    return false;
  }
  if (   (this->ergebnisse_.size() > 1)
      || (this->ergebnisse_[0].size() != 1) ) {
    cerr << "Datenbank: Genau ein Ergebnis erwartet, erhalte aber:\n";
    this->schreibe_ergebnisse(cerr);
    return false;
  }
#endif
  return true;
} // bool Datenbank::pruefe_auf_ein_ergebnis() const

/** Prüft, dass genau eine Ergebnisspalte vorhanden ist
 **
 ** @return   Ob genau eine Ergebnisspalte vorhanden ist
 **/
bool
Datenbank::pruefe_auf_eine_ergebnisspalte() const
{
#ifdef DEBUG
  if (this->ergebnisse_anzahl() == 0) {
    abort();
    cerr << "Datenbank: Eine Ergebnisspalte erwartet, Ergebnisse sind aber leer\n";
    return false;
  }
  if (this->ergebnisse_spalten_.size() > 1) {
    cerr << "Datenbank: Genau eine Ergebnisspalte erwartet, erhalte aber:\n";
    this->schreibe_ergebnisse(cerr);
    return false;
  }
#endif
  return true;
} // bool Datenbank::pruefe_auf_eine_ergebnisspalte() const

/** Schreibt alle Ergebnisse
 **
 ** @param    ostr    Ausgabestrom
 **/
void
Datenbank::schreibe_ergebnisse(ostream& ostr) const
{
  if (this->ergebnisse_.empty()) {
    ostr << "--\n";
    return ;
  }
  // Maximal Anzahl an Zeichen für die Spalte
  vector<size_t> zeichen(this->ergebnisse_spalten_.size(), 0);
  for (unsigned i = 0; i < this->ergebnisse_spalten_.size(); ++i) {
    zeichen[i] = this->ergebnisse_spalten_[i].size();
    for (auto e : this->ergebnisse())
      zeichen[i] = std::max(zeichen[i], e[i].size());
  } // for (i < this->ergebnisse_.sizen())

  // Spaltentitel ausgeben
  ostr << std::setw(zeichen[0]) << this->ergebnisse_spalten_[0];
  for (unsigned i = 1; i < this->ergebnisse_spalten_.size(); ++i)
    ostr << " | " << std::setw(zeichen[i]) << this->ergebnisse_spalten_[i];
  ostr << '\n';
  // Trenner ausgeben
  for (unsigned j = 0; j < zeichen[0]; ++j)
    ostr << '-';
  for (unsigned i = 1; i < this->ergebnisse_spalten_.size(); ++i) {
    ostr << "-+-";
    for (unsigned j = 0; j < zeichen[i]; ++j)
      ostr << '-';
  }
  ostr << '\n';
  // Einträge ausgeben
  for (unsigned j = 0; j < this->ergebnisse_.size(); ++j) {
    ostr << std::setw(zeichen[0]) << this->ergebnisse_[j][0];
    for (unsigned i = 1; i < this->ergebnisse_spalten_.size(); ++i)
      ostr << " | " << std::setw(zeichen[i]) << this->ergebnisse_[j][i];
    ostr << '\n';
  } // for (j)

  return ;
} // void Datenbank::schreibe_ergebnisse(ostream& ostr) const

} // namespace Datenbank
