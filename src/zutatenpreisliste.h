#pragma once

#include "zutatmengepreis.h"

using ZutatenPreisListeBase = vector<ZutatMengePreis>;
/** Liste von Zutaten mit Menge und Preis
 ** Beispiel:
 ** 1 kg Zucker
 ** Salz
 **/
class ZutatenPreisListe : private ZutatenPreisListeBase {
  public:
    // Konstruktor
    ZutatenPreisListe();

    // Übernommene Funktionen
    using ZutatenPreisListeBase::begin;
    using ZutatenPreisListeBase::end;
    using ZutatenPreisListeBase::empty;

    // gibt zurück, ob die Zutat enthalten ist
    bool enthaelt(Zutat const& zutat) const;
    // gibt die Menge von der Zutat zurück (muss nicht eindeutig sein)
    Menge menge(Zutat const& zutat) const;
    Preis preis(Zutat const& zutat) const;

    // Fügt eine Zutat hinzu
    void hinzufuege(ZutatMengePreis const& zutat);

    // Entfernt eine Zutat
    bool entferne(Zutat const& zutat);
}; // class ZutatenPreisListe : private vector<ZutatMengePreis>

ostream& operator<<(ostream& ostr, ZutatenPreisListe const& liste);
