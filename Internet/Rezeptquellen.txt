Quellen für Rezepte
===================

== Chefkoch

Die Seite http://www.chefkoch.de/ ist wohl die bekannteste.
Allerdings ist es nach Abschnitt 4.1 der link:http://www.chefkoch.de/terms-of-use.php[AGB] folgendes verboten: „…der Aufbau eigener … Verzeichnisse unter Zuhilfenahme der auf chefkoch.de abrufbaren Inhalte“. Daher dürfen nach meinem Verständnis die Rezepte von Chefkoch nicht in selbst erstellte (digitale oder analoge) Kochbücher übernommen werden.


== Das Kochrezept
http://www.das-kochrezept.de

== Essen und Trinken
http://www.essen-und-trinken.de

== Rezeptschachtel
http://rezeptschachtel.de/



// vim: filetype=asciidoc
