#!/bin/zsh

make -C ../src sophie

for eingabe in *.eingabe; do
  local ausgabe="$( basename $eingabe .eingabe).ausgabe"
  ../src/sophie <${eingabe} ":memory:" | sed "1,/^' Beginn Referenz/d" > ${ausgabe}
  sed "1,/^# Ausgabe$/d" < "${eingabe}" | cmp --silent ${ausgabe}

  if [ $? -eq 0 ]; then
    echo -e "$eingabe: \033[1m\033[32min Ordnung\033[0m"
  else
    echo -e "$eingabe: \033[1m\033[31mUnterschiede\033[0m"
  fi
done
