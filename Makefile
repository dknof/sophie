
.PHONY: all
all : Konzept Internet src

# Ausgabe der Hilfe
.PHONY: help
help : hilfe
.PHONY: hilfe
hilfe :
	@echo "all:      Alles erstellen"
	@echo "Konzept:  html-Version vom Konzept erstellen (benötigt asciidoc)"
	@echo "Internet: Internetseite (html) generieren (benötigt asciidoc)"
	@echo "src:      Programm kompilieren"

# html-Version vom Konzept erstellen (benötigt asciidoc)
.PHONY: Konzept
Konzept :
	test -f "$@"/Makefile && $(MAKE) -C "$@"

# Internetseite (html) generieren (benötigt asciidoc)
.PHONY: Internet
Internet :
	test -f "$@"/Makefile && $(MAKE) -C "$@"

# Programm kompilieren
.PHONY: src
src :
	test -f "$@"/Makefile && $(MAKE) -C "$@"

